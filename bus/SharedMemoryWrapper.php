<?php

class SharedMemoryWrapper {

    static $logger;

    static function init() {
        self::$logger = Logger::getLogger("sharedMemoryWrapper");
    }

    static function stringToKey($string) {
        return sprintf("%u", crc32($string));

        // return hexdec($string); // assumes that id is hex string (uniqid, UUID, GUID, etc)
    }

    static function set($name, $value) {
        self::$logger->debug("SET $name = $value");

        $key = self::stringToKey($name);
        self::$logger->debug("SET: $name -> $key");

        $size = strlen($value);
        self::$logger->debug("SET: size($value) -> $size");

        @$shmid = shmop_open($key, "c", 0666, $size);
        self::$logger->debug("SET: shmid is $shmid");

        if (!empty($shmid)) {
            // shared memory exists
            self::$logger->info("SET: shmem exists");

            $shm_bytes_written = shmop_write($shmid, $value, 0);
            self::$logger->debug("SET: write $value at $shmid -> $shm_bytes_written bytes");

            if ($shm_bytes_written === false) {
                throw new Exception("Shared memory block wasn't written");
            }

            self::$logger->debug("SET: close $shmid");
            shmop_close($shmid);

            return $key;
        } else {
            self::$logger->error("SET: shmem not created or openned");
            // shared memory doesn't exist
            throw new Exception("Shared memory block wasn't created as rw");
        }
    }

    static function get($name) {
        self::$logger->debug("GET $name");

        $key = self::stringToKey($name);
        self::$logger->debug("GET: $name -> $key");

        @$shmid = shmop_open($key, "a", 0666, 0);
        self::$logger->debug("GET: shmid is $shmid");

        if (!empty($shmid)) {
            // shared memory exists
            self::$logger->info("GET: shmem exists");

            $size = shmop_size($shmid);
            self::$logger->debug("GET: size($shmid) -> $size");

            $value = shmop_read($shmid, 0, $size);
            self::$logger->debug("GET: value = $value");

            self::$logger->debug("GET: close $shmid");
            shmop_close($shmid);

            return $value;
        } else {
            self::$logger->error("GET: shmem not exists");

            // shared memory doesn't exist
            // throw new Exception("Shared memory block doesn't exists");
            return null;
        }
    }

    static function delete($name) {
        self::$logger->debug("DELETE $name");

        $key = self::stringToKey($name);
        self::$logger->debug("DELETE: $name -> $key");

        @$shmid = shmop_open($key, "w", 0666, 0);
        self::$logger->debug("DELETE: shmid is $shmid");

        if (!empty($shmid)) {
            // shared memory exists
            self::$logger->info("DELETE: shmem exists");

            $deleted = shmop_delete($shmid);
            self::$logger->debug("DELETE: delete($shmid): $deleted");

            self::$logger->debug("DELETE: close $shmid");
            shmop_close($shmid);

            return $deleted;
        } else {
            self::$logger->error("DELETE: shmem not exists");

            // shared memory doesn't exist
            // throw new Exception("Shared memory block doesn't exists");
            return null;
        }
    }
}

SharedMemoryWrapper::init();