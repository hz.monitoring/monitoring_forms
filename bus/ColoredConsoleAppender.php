<?php

require_once("Colors.php");


class ColoredConsoleAppender extends LoggerAppenderEcho {
    private $fg_color = null;
    private $bg_color = null;
    private $colors;

    public function __construct($name, $fg_color = null, $bg_color = null)
    {
        parent::__construct($name);

        $this->colors = new Colors();
        $this->fg_color = $fg_color;
        $this->bg_color = $bg_color;

        // $layout = new LoggerLayoutPattern();
        // $layout->setConversionPattern('%d [%t] %p %c %x - %message%n');

        $layout = new LoggerLayoutSimple();

        $this->setLayout($layout);
    }

    public function append(LoggerLoggingEvent $event)
    {
        if($this->layout !== null) {
            if($this->firstAppend) {
                echo $this->layout->getHeader();
                $this->firstAppend = false;
            }
            $text = $this->layout->format($event);

            $text = trim($text, "\n");
            $text = $this->colors->getColoredString($text, $this->fg_color, $this->bg_color);
            $text .= "\n";

            if ($this->htmlLineBreaks) {
                $text = nl2br($text);
            }
            echo $text;
        }
    }
}