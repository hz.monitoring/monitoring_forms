<?php

define('ENTRY_POINT', 'admin-service');
require('../common.inc.php');

require_once("../pagecontrol/Load.php");

use PFBC\Form;
use PFBC\Element;
use PFBC\Validation;

use pagecontrol\PageController;
use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;

$result = array();

$user = User::getFromSession($_SESSION);


if (!$user->isLogged()) {
    $result["success"] = false;
    $result["errorMessage"] = "Пользователь не авторизован";
}

if (empty($_GET["subject"]) || empty($_GET["lang"])) {
    $result["success"] = false;
    $result["errorMessage"] = "Некорректный запрос";
}

$subject = isset($_GET["subject"]) ? $_GET["subject"] : null;
$language = isset($_GET["lang"]) ? $_GET["lang"] : null;



if (!empty($_POST["form"])) {
    $formName = $_POST["form"];
    // if(!Form::isValid($formName, false)) {
    //     Form::renderAjaxErrorResponse($formName);
    // }
}





$data = $mn->selectDB($prefix . "data");
$pages = $data->documents;

if (empty($_GET["page_type"])) {
    die('no way');
}

$metadata = $mn->selectDB($prefix . "model");

$metaTypesCursor = $metadata->metatypes->find();

$metaTypes = array();
foreach($metaTypesCursor as $metaType) {
    $metaTypeName = $metaType["name"];
    $metaTypes[$metaTypeName] = $metaType;
}

$metaTypesLabels = array_reduce(array_keys($metaTypes), function(&$acc, $metaTypeName) use ($language, $metaTypes) {
        $metaType = $metaTypes[$metaTypeName];

        $acc[$metaTypeName] = isset($metaType["label_$language"]) ? $metaType["label_$language"] : "$metaTypeName [$language]";

        return $acc;
    }, array());



$pageType = $_GET["page_type"];
$pageTypes = $metadata->types;
$pageDesc = $pageTypes->findOne(array("name" => $pageType));





if (!empty($result)) {
    header("Content-type: text/json");
    echo json_encode($result);
    die;
}

switch ($subject) {

    case 'user':
        $action = $_GET["action"];
        switch ($action) {
            case 'search':
                $fio = isset($_POST["fio"]) ? trim($_POST["fio"]) : null;

                if ($fio) {
                    $fio_parts = explode(" ", $fio);
                    // var_dump($fio_parts);

                    $users_query = "(|" . implode("", array_map(function($part) { return "(sn=$part)(givenname=$part)"; }, $fio_parts)) . ")";
                    $users = \User::findAllByQuery($users_query);

                    $users_data = array_map(function($user) {
                        if (isset($user->uuid)) {
                            return array(
                                "givenname" => $user->givenname
                                , "lastname" => $user->lastname
                                , "login" => $user->login
                                , "uuid" => $user->uuid
                            );
                        }
                    }, $users);

                    $result["success"] = true;
                    $result["users"] = $users_data;
                } else {
                    $result["success"] = false;
                    $result["errorMessage"] = "Не указан поисковый запрос";
                }
                break;

            default:
                $result["success"] = false;
                $result["errorMessage"] = "Unknown action '$action' for subject '$subject'";
        }
        break;

    case 'department':
        $action = $_GET["action"];
        switch ($action) {
            case 'search':
                $description = isset($_POST["description"]) ? $_POST["description"] : null;

                if ($description) {
                    $department = \Department::findByPath($description);

                    if (!$department) {
                        $department_query = "(description=*$description*)";
                        $departments = \Department::findAllByQuery($department_query);
                    } else {
                        $departments = array($department);
                    }

                    $departments_data = array_map(function($dep) {
                        if (isset($dep->description)) {
                            return array(
                                "description" => $dep->description
                                , "uuid" => $dep->uuid
                            );
                        }
                    }, $departments);

                    $result["success"] = true;
                    $result["departments"] = $departments_data;
                } else {
                    $result["success"] = false;
                    $result["errorMessage"] = "Не указан поисковый запрос";
                }
                break;

            default:
                $result["success"] = false;
                $result["errorMessage"] = "Unknown action '$action' for subject '$subject'";
        }
        break;


    case 'rule':
        $action = $_GET["action"];
        switch ($action) {

            // case 'list':
            //     $actor = isset($_POST["actor"]) ? $_POST["actor"] : null;
            //     $object = isset($_POST["object"]) ? $_POST["object"] : null;
            //     if ($actor && $object) {

            //     }

            //     break;

            case 'create':

                $preset = isset($_POST["preset"]) ? $_POST["preset"] : null;

                if ($preset) {
                    $actor_uuid = isset($_POST["actor"]) ? $_POST["actor"] : null;
                    $object_uuid = isset($_POST["object"]) ? $_POST["object"] : null;

                    if ($actor_uuid && $object_uuid) {

                        if ($preset == 'chair') {
                            $typeObject = DocumentType::findByType($mn, $prefix, "chair");

                            $department = \Department::findByUuid($object_uuid);

                            $object = Document::findByUuid($mn, $prefix, $object_uuid);
                            if (is_null($object)) {
                                $object = Document::create($mn, $prefix, $typeObject, $object_uuid);
                                $object->setField("title_ru", $department->description);
                                $object->save();
                            }

                            $allRules = \Rule::find($mn, $prefix, $actor_uuid, $object_uuid);

                            if (empty($allRules)) {

                                $rule_a = \Rule::create($mn, $prefix);
                                $rule_a->addActors(array($actor_uuid));
                                $rule_a->addActions(array("+browse", "+read", "+display", "+update"));
                                $rule_a->addObjects(array($object_uuid));
                                $rule_a_id = $rule_a->save();


                                $rule_d = \Rule::create($mn, $prefix);
                                $rule_d->addActors(array($actor_uuid));
                                $rule_d->addActions(array('-update', "-display"));
                                $rule_d->addObjects(array("$object_uuid/uuid", "$object_uuid/title"));
                                $rule_d_id = $rule_d->save();

                                $allRules = \Rule::find($mn, $prefix, $actor_uuid, $object_uuid);

                                $result["success"] = true;
                                $result["rules"] = $allRules;
                            } else {
                                $result["success"] = false;
                                $result["rules"] = $allRules;
                                $result["errorMessage"] = "Rules exist";
                            }


                        } else {
                            $result["success"] = false;
                            $result["errorMessage"] = "No preset";
                        }

                    }

                }


                break;

            case 'delete':
                $ruleId = isset($_POST["id"]) ? $_POST["id"] : null;

                break;

            case 'list':
                $actors = isset($_POST["actors"]) ? $_POST["actors"] : null;
                $objects = isset($_POST["objects"]) ? $_POST["objects"] : null;

                break;

            default:
                $result["success"] = false;
                $result["errorMessage"] = "Unknown action '$action' for subject '$subject'";
        }

        break;


    case 'item':


        $currentPageType = DocumentType::findByType($mn, $prefix, $pageType);
        if (!$currentPageType) {
            $result["success"] = false;
            $result["errorMessage"] = "Page type '$pageType'";
            break;
        }

        $editorSchemaObject = DocumentType::findByType($mn, $prefix, "field");
        $editorSchema = $editorSchemaObject->items;

        $action = $_GET["action"];
        switch ($action) {
            case 'update':
                if (!isset($_POST["index"])) {
                    die('no way');
                }
                $index = (int)$_POST["index"];

                foreach ($editorSchema as $item) {
                    $isItemRequired = isset($item["required"]) ? $item["required"] : false;

                    $editorSchemaType = $item["type"];
                    $editorSchemaName = $item["name"];
                    $itemNoI18n = isset($item["no_i18n"]) ? $item["no_i18n"] : false;



                    // $itemTypeLabel = isset($metaTypesLabels[$itemType]) ? $metaTypesLabels[$itemType] : "[$itemType]";
                    // $itemLabel = isset($item["label_".$language]) ? $item["label_".$language] : "(без названия)";
                    // $itemNoVersioning = isset($item["no_versioning"]) ? $item["no_versioning"] : false;

                    $editorSchemaNameLang = $itemNoI18n ? $editorSchemaName : ($editorSchemaName . "_" . $language);

                    if (isset($_POST[$editorSchemaNameLang])) {
                        $editorSchemaValue = $_POST[$editorSchemaNameLang];
                        if ($editorSchemaType == "boolean") {
                            $editorSchemaValue = isset($editorSchemaValue[0]);
                        } elseif ($editorSchemaType != "wysiwyg") {
                            $editorSchemaValue = strip_tags($editorSchemaValue);
                        }
                    } else {
                        $editorSchemaValue = false;
                    }

                    $pageDesc["items"][$index][$editorSchemaNameLang] = $editorSchemaValue;
                }

                $status = $pageTypes->update(array("name"=>$pageType), $pageDesc);

                $result = array(
                    "success" => $status["updatedExisting"]
                    , "item" => $pageDesc["items"][$index]
                );
                break;

            case 'sort':
                if (!isset($_POST["index"]) || !isset($_POST["new_index"])) {
                    die('no way');
                }

                $index = (int)$_POST["index"];
                $newIndex = (int)$_POST["new_index"];

                $item = array_splice($pageDesc["items"], $index, 1);

                array_splice($pageDesc["items"], $newIndex, 0, $item);

                $status = $pageTypes->update(array("name"=>$pageType), $pageDesc);

                $result = array(
                    "success" => $status["updatedExisting"]
                    , "item" => $pageDesc["items"][$newIndex]
                );
                break;

            case 'add':
                if (!isset($_POST["index"])) {
                    die('no way');
                }
                $index = (int)$_POST["index"];

                $newItemType = isset($_POST["type"]) ? $_POST["type"] : null;

                if (isset($metaTypes[$newItemType])) {

                    $newItem = array();

                    foreach ($editorSchema as $item) {
                        $editorSchemaType = $item["type"];
                        $editorSchemaName = $item["name"];
                        $itemNoI18n = isset($item["no_i18n"]) ? $item["no_i18n"] : false;

                        $editorSchemaNameLang = $itemNoI18n ? $editorSchemaName : ($editorSchemaName . "_" . $language);

                        $newItem[$editorSchemaNameLang] = "";

                    }

                    $newItem["type"] = $newItemType;

                    array_splice($pageDesc["items"], $index, 0, array($newItem));

                    $status = $pageTypes->update(array("name"=>$pageType), $pageDesc);

                    $result = array(
                        "success" => $status["updatedExisting"]
                        , "item" => $newItem
                    );
                }
                break;

            case 'delete':
                if (!isset($_POST["index"])) {
                    die('no way');
                }
                $index = (int)$_POST["index"];

                array_splice($pageDesc["items"], $index, 1);

                // var_dump($pageDesc, $_POST);die;

                $status = $pageTypes->update(array("name"=>$pageType), $pageDesc);

                $result = array(
                    "success" => $status["updatedExisting"]
                    , "item" => null
                );
                break;
        }
        break; // case 'item'


    default:
        $result["success"] = false;
        $result["errorMessage"] = "Unknown subject '$subject'";
}

header("Content-type: text/json");
echo json_encode($result);