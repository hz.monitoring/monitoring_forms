<?php
require_once('../../LdapObject.php');
require_once('../../bus/Colors.php');
require_once("../../documongo/Load.php");
use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;
	require_once("lib/fconnect.php");
	$db = DBConnect::getInstance();
	$collectionUnits =  $db->colUnits;

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

$colors = new Colors();

echo "\n";

if (($handle = fopen("divisions.csv", "r")) !== FALSE) {
	$i=1;							
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$division 	= trim($data[1]);
		$parent 	= trim($data[0]);
		$document = array("name" => $data[1], "id" => $i*1.0,"pid" => "", "get" => "yes");
		$thisDivision = $collectionUnits->findOne(array("name" => $division));
		if (!$thisDivision){
			$collectionUnits->insert($document);
		}
		$i++;
	}  
	fclose($handle);
}

if (($handle = fopen("divisions.csv", "r")) !== FALSE) {
								
	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$division 	= trim($data[1]);
		$parent 	= trim($data[0]);
		if($parent != ""){
			$thisIsParent = $collectionUnits->findOne(array("name" => $parent));
			$thisIsChild  = $collectionUnits->findOne(array("name" => $division));
			$newdata = array('$set' => array('pid' => $thisIsParent['id']));
			$collectionUnits->update(array("name" => $thisIsChild['name']), $newdata);
		}
	}  

}

