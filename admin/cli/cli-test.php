<?php
   require_once(__DIR__ . "/../../vendor/autoload.php");

use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;

require_once("../../web/lib/fconnect.php");
$db = DBConnect::getInstance();
$dataCollection = $db->colDocs;
$chairs = $dataCollection->find(array('type' => 'ItemsForChair'));
foreach($chairs as $chair){
	
	//$chair['type'] = 'chair';
	//var_dump($chair['uuid']);
	$newdata = array('$set' => array('type'=>'chair'));
	$dataCollection->update(array("uuid" => $chair['uuid']), $newdata);
}

