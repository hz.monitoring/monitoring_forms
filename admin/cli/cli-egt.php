<?php
require_once("../../web/lib/fconnect.php");
$db = DBConnect::getInstance();
$errors = 0;
$success = 0;
echo "\n \t Выполняю обновление профилей...\n";	
if (($handle = fopen("EGT.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		if($data[2]=='chair'){
			$what = array('type'=>'chair','title_ru'=>$data[0]);
			$doc = $db->colDocs->findOne($what);
			if($doc==NULL){
				$errors++;
				var_dump($data[0]);
			}else{
				$success++;
				$newdata = array('$set'=>array('chairtype'=>$data[1],'rates'=>(float)str_replace(",",".",$data[3])));
				$db->colDocs->update($what, $newdata);
			}
		}
    }
    fclose($handle);
}
if($errors>0){
	echo "\n \t Всего не найдено кафедр в базе: $errors \n";
	$errors = 0;	
}
if($success>0){
	echo "\n \t Всего обновлено кафедр: $success \n";
	$success = 0;
}
echo "\n \t Обновление профилей кафедр успешно завершено! \n";
echo "\n \t Выполняю проверку наличия профилей... \n";	

//Проверяем все ли кафедры базы содержат профили и ставки
$db->colDocs->remove(array('title_ru'=>'Кафедра менеджмента  Волховского филиала'));
$chairs = $db->colDocs->find(array('type'=>'chair'));
foreach($chairs as $chair){
	if(!isset($chair['chairtype'])){
		$errors++;
		echo "НЕТ ПРОФИЛЯ: ";
		var_dump($chair['title_ru']);
	}else{
		$success++;
	}
	if(!isset($chair['rates'])){
		$errors++;
		echo "НЕТ СТАВОК: ";
		var_dump($chair['title_ru']);
	}
}
if($errors>0){
	echo "\n \t Всего ошибок в базе: $errors \n \n";
	$errors = 0;	
}
if($success>0){
	echo "\n \t Всего найдено кафедр с профилями: $success \n";
	$success = 0;
}
echo "\n \t Профили проверены! \n";
echo "\n \t Завершаю работу! \n";
