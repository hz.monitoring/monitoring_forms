<?php
   require_once(__DIR__ . "/../../vendor/autoload.php");

use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;

require_once("../../web/lib/fconnect.php");
$db = DBConnect::getInstance();
$rulesCollection = $db->colRules;
$dataCollection = $db->colDocs;
$ldap_host = "10.0.16.236";
$ldap_rootdn = "dc=herzen,dc=spb,dc=ru";

$ldap_handle = ldap_connect($ldap_host);
ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);

if (!$ldap_handle) {
    throw new \Exception("Ldap connection problem");
}

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

require_once('../../bus/Colors.php');
$colors = new Colors();
$objects = array();
$rules = array();

$a                  = require(__DIR__ .'/../../conf/conf.php');
$host				= $a['mongo_host'];
$port				= $a['mongo_port'];
$prefix 			= $a['db_prefix'];
$server				= 'mongodb://'.$host.':'.$port;


$mn = new MongoClient($server);

$err_string = $colors->getColoredString('[ERR]', 'red');

$typeObject = DocumentType::findByType($mn, $prefix, "chair");
if (!$typeObject) {
    die("Not found typeObject\n");
}
$typeObjectParent = DocumentType::findByType($mn, $prefix, "faculty");
if (!$typeObjectParent) {
    var_dump($typeObjectParent);
    die("Not found typeObjectParent\n");
}
$opt = getopt("n::");
/*				$rulesCollection->remove();
				$rule_a = array (
							'_id' => new \MongoId(),
							  'actions' => array('-display'),
							  'actors'  => array ('*'),
							  'objects' => array ("chair/confirmed",
												  "chair/visited",
												  "chair/report",
												  "chair/url",
												  "chair/url2",
												  "chair/uploaded",
												  "chair/uploaded2")
							);
				$rulesCollection->insert($rule_a);
				$rule_a = array (
							'_id' => new \MongoId(),
							  'actions' => array('-display','-update'),
							  'actors'  => array ('*'),
							  'objects' => array (  "chair/uuid",
													"chair/title",
													"chair/parent",
													"chair/chairtype",
													"chair/rates")
							);
				$rulesCollection->insert($rule_a);
*/
echo "\n";
$row = 1;
if (($handle = fopen(__DIR__ . "/people.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if (strpos($data[0], '№') === 0) {
            continue;
        }
        if($data[3]==''){
			continue;
		}else{
			$uid = trim($data[3]);
			$actors = array($uid);
			$roles = array($data[4]);
		}
        if($data[1]=='chair'){
			$objects = array(trim($data[1]));
		}else{
			$deps = explode('/',$data[1]);
			$deps = array_reverse(array_diff($deps, array('')));
			$deps[0] = trim($deps[0]);

			$deparray = $dataCollection->findOne(array('title_ru'=>$deps[0]));
			if(isset($deparray)){
				$objects = array($deparray['uuid']);
//				echo $colors->getColoredString("$actors[0] --- $objects[0] \n", "red");
				unset($deparray);
			}
		}
		switch($data[4]){
			case 'admin':
				$action = array("+browse", "+create", "+read", "+display", "+update", "+delete", "+form", "+overview", "+approve");
			break;
			case 'prorector':
				$action = array("+browse", "+read");
			break;
			case 'rector':
				$action = array("+browse", "+read");
			break;
			case 'amenable':
				$action = array("+browse", "+read", "+display", "+update");
			break;
			case 'dean':
				$action = array();
			break;
			case 'moderator':
				$action = array();
			break;
			case 'head':
				$action = array();
			break;
			case 'employee':
				$action = array();
			break;
			case 'student':
				$action = array();
			break;
			case 'herzen':
				$action = array();
			break;
		}
		$rule_a = array (
		  '_id' => new \MongoId(),
		  'actions' => $action,
		  'actors'  => $actors,
		  'objects' => $objects,
		  'roles' => $roles
		);
		$rulesCollection->insert($rule_a);
		echo $colors->getColoredString("[+++] $data[4] -- $data[1] -- $data[2] -- \n", "yellow");
    }
    fclose($handle);
}
