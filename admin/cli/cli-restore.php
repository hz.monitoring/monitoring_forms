<?php

require_once('../../LdapObject.php');
require_once('../../Department.php');
require_once('../../bus/Colors.php');
require_once("../../documongo/Load.php");
use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;

require_once("lib/fconnect.php");
$db = DBConnect::getInstance();
$rulesCollection = $db->colRules;
$collectionData = $db->colDocs;

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

$colors = new Colors();
echo "\n";
if (($handle = fopen("restore.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
        if ($data[0] == 'title_ru') {
			$massdata =  array();
			$row = array();
			foreach($data as $index){
				$indexes[] = $index;  
			}
			continue;
        }
 		for($i=1;$i<=113;$i++){
			$data_to_send[$indexes[$i]] = (float)$data[$i]; 	
		}       
       
		$title = substr($data[0],0,strlen($data[0])-33);
		$nomer = substr($data[0],strlen($data[0])-21,strlen($data[0]));
		$n = (int)$nomer{0}-1; 
		switch ($n){
			case 0:
			$nomer = 0;
			break;
			case 1:
			$nomer = 2;
			break;
			case 2:
			$nomer = 1;
			break;
		}
		$db_data = $collectionData->findOne(array('title_ru' => $title));		
		foreach($data_to_send as $key => $value){
			if($key != 'title_ru' or $key != 'verno'){
				if($db_data['versions'][$nomer]['content'][$key]!= $data_to_send[$key]){
					$version = array('$set' => array('versions.'.$nomer.'.content.'.$key => $value));
					$collectionData->update(array('title_ru' => $title),$version);
				}
			}
		}	
	
	}
    fclose($handle);
}
