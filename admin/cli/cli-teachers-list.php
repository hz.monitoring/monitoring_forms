<?php
require_once('../../bus/Colors.php');
use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;
require_once("lib/fconnect.php");
$db = DBConnect::getInstance();
$collectionData =  $db->colDocs;
$i=0;
$colors = new Colors();
$chairs = $collectionData->find(array('type'=>'chair'));
foreach($chairs as $chair){
$newdata = array('$set'=>array("teachers"=>array()));
$collectionData->update(array("title_ru"=>$chair['title_ru']),$newdata );
	}
	
if (($handle = fopen("teachers.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		if ($data[4]=="осн") {
			$fio 	= trim($data[0]);
			$chair 	= trim($data[7]);
			$chair	= preg_replace('/\s+/',' ',$chair);
			$rate	= (float)trim(str_replace(",",".",$data[5]));
			$current = $collectionData->findOne(array("title_ru"=>$chair));	
			if (($chair === $current["title_ru"]) AND $current["type"] === "chair"){
				$teachers = array(
					'teachers' => array('name' => $fio,'rate' => $rate)			
				);
				$newdata = array('$push'=> $teachers );
				$collectionData->update(array("title_ru"=>$chair),$newdata );
			}else{
			$res = ++$i . " \t" . $fio . " \t" . $chair . " \t" . $rate . "\n"; 
			echo $colors->getColoredString($res, "red"); 
			}
		}		
}	}
$osn = 0;
foreach($chairs as $chair){
	$osn += count($chair['teachers']);
echo count($chair['teachers'])."\tосн. в ".$chair['title_ru']." \n";
	}
	echo 'Всего в базу добавлено '.$osn.' человек!'." \n";
