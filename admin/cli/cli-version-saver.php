<?php
date_default_timezone_set('Europe/Moscow');
mb_internal_encoding("UTF-8");

require_once('../../bus/Colors.php');
require_once(__DIR__ . "/../../vendor/autoload.php");
use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;
$colors = new \Colors();

use pagecontrol\PageController\Configurator;
$mn = new \MongoClient(Configurator::getServerString());
$prefix = Configurator::getDbPrefix();

$realData = $mn->selectDB($prefix . "data");
$documentType = "chair";
$language = "ru";
echo "\n";
$documentTypeObject = DocumentType::findByType($mn, $prefix, $documentType);
// Даты для поиска существующих сохранений
// Если сохранения данных последнего этапа уже проводились, то повторно сохранять данные не нужно
// (иначе будет дублирование этапов).
// Для этого укажите даты, когда планируется проводить сохранение данных.
// Если вы случайно запустите сохранение второй раз, оно не будет проведено, дубль не создастся
// Укажите даты вокруг предполагаемой даты сохранения данных
$pastAvailablePeriodSince = DateTime::createFromFormat('Y-m-d|', '2018-03-18'); // дата окончания предыдущего этапа
$pastAvailablePeriodTill = DateTime::createFromFormat('Y-m-d|', '2018-03-20');  // дата начала этого этапа

$versioningLabel = "Этап 2"; // Названия среза сохранения (предыдущий этап)
$versioningDescription = "Значение показателей за этап 2 (20 сентября 2017 - 19 декабря 2017)"; // Описание среза сохранения(предыдущий этап)
$versioningLabel = "Окончание этапа 2";
$versioningDescription = "Значение показателей за этап 2";

$versioningDocuments = Document::find($mn, $prefix, $documentType);

if (!is_array($versioningDocuments)) {
    $versioningDocuments = array($versioningDocuments);
}


$bad = array();

foreach ($versioningDocuments as $versioningDocument) {
    $versioningDocumentTitle = $versioningDocument->getFieldI18nValue("title", $language);
    echo "Process " . $colors->getColoredString($versioningDocumentTitle, "yellow") . " (" . $versioningDocument->uuid . "):\n";

    if ($pastAvailablePeriodSince && $pastAvailablePeriodTill) {
        echo 'Search versions ['
            . $colors->getColoredString($pastAvailablePeriodSince->format("Y-m-d"), "purple")
            . ' — '
            . $colors->getColoredString($pastAvailablePeriodTill->format("Y-m-d"), "purple")
            . ']: ';
        $searchVersions = $versioningDocument->findVersions($pastAvailablePeriodSince, $pastAvailablePeriodTill);

        if (!empty($searchVersions)) {
            echo "Found versions: " . count($searchVersions) . ", skip\n";
            $vcount = 0;
            $vbads = array();
            $vgoods = array();
            foreach ($searchVersions as $searchVersion) {
                $searchVersionId = (string)$searchVersion["_id"];
                $currentDate = date("Y-m-d", $searchVersion["datetime"]->sec);
                $isCurrent = $currentDate >= $pastAvailablePeriodSince->format("Y-m-d") && $currentDate < $pastAvailablePeriodTill->format("Y-m-d");
                $searchVersionString = $searchVersionId . ': ' . $searchVersion["label_ru"] . ' (at ' . date('Y-M-d h:i:s', $searchVersion["datetime"]->sec) . ')';
                echo ''
                    . ($isCurrent
                        ? $colors->getColoredString($searchVersionString, "light_blue") . ' <-- выбранный этап'
                        : $searchVersionString)
                    . "\n";
                $vcount++;
            }
            if (!empty($vbads)) {
                file_put_contents("out.bads." . $versioningDocument->id . "." . $versioningDocumentTitle . ".txt", implode("\n", $vbads));
            }
            if (!empty($vgoods)) {
                file_put_contents("out.goods." . $versioningDocument->id . "." . $versioningDocumentTitle . ".txt", implode("\n", $vgoods));
            }
            continue;
        } else {
           echo "No versions found: create new version\n";
        }
    }
    $versioningElement = $versioningDocument->saveVersion($versioningLabel, $versioningDescription, $language);
    var_dump($versioningElement);
    echo 'Created';
            $reseted = $versioningDocument->resetFields($language);
            echo "Reset result: ";
}

if (!empty($bad)) {
    file_put_contents("out.bads.txt", implode("\n", $bad));
}


