<?php
   require_once(__DIR__ . "/../../vendor/autoload.php");

use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;

require_once("../../web/lib/fconnect.php");
$db = DBConnect::getInstance();
$rulesCollection = $db->colRules;

$ldap_host = "10.0.16.236";
$ldap_rootdn = "dc=herzen,dc=spb,dc=ru";

$ldap_handle = ldap_connect($ldap_host);
ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);

if (!$ldap_handle) {
    throw new \Exception("Ldap connection problem");
}

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

require_once('../../bus/Colors.php');
$colors = new Colors();

//@unlink("out.department.parent.created.txt");
//@unlink("out.department.parent.found.txt");
//@unlink("out.department.parent.fails.txt");
//@unlink("out.department.parent.skip.txt");

//@unlink("out.department.created.txt");
//@unlink("out.department.found.txt");
//@unlink("out.department.fails.txt");
//@unlink("out.department.skip.txt");

//@unlink("out.rules.found.txt");
//@unlink("out.rules.created.txt");

$objects = array();
$rules = array();

$a                  = require(__DIR__ .'/../../conf/conf.php');
$host				= $a['mongo_host'];
$port				= $a['mongo_port'];
$prefix 			= $a['db_prefix'];
$server				= 'mongodb://'.$host.':'.$port;


$mn = new MongoClient($server);

$err_string = $colors->getColoredString('[ERR]', 'red');

$typeObject = DocumentType::findByType($mn, $prefix, "chair");
if (!$typeObject) {
    die("Not found typeObject\n");
}
$typeObjectParent = DocumentType::findByType($mn, $prefix, "faculty");
if (!$typeObjectParent) {
    var_dump($typeObjectParent);
    die("Not found typeObjectParent\n");
}
$opt = getopt("n::");
				$rulesCollection->remove();
				$rule_a = array (
							'_id' => new \MongoId(),
							  'actions' => array('-display'),
							  'actors'  => array ('*'),
							  'objects' => array ("chair/confirmed",
												  "chair/visited",
												  "chair/report",
												  "chair/url",
												  "chair/url2",
												  "chair/uploaded",
												  "chair/uploaded2")
							);
				$rulesCollection->insert($rule_a);
				$rule_a = array (
							'_id' => new \MongoId(),
							  'actions' => array('-display','-update'),
							  'actors'  => array ('*'),
							  'objects' => array (  "chair/uuid",
													"chair/title",
													"chair/parent",
													"chairtype",
													"rates")
							);
				$rulesCollection->insert($rule_a);
echo "\n";
$row = 1;
if (($handle = fopen("people.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if (strpos($data[0], '№') === 0) {
            continue;
        }
        $num = count($data);
        $row++;
        $rowNum = isset($data[0]) ? $data[0] : null;
        $dep = isset($data[1]) ? trim($data[1]) : null;
        $fio = isset($data[2]) ? trim($data[2]) : null;
        $user_uuid = isset($data[3]) ? trim($data[3]) : null;
        $role = isset($data[4]) ? trim($data[4]) : null;
        echo "Read line $dep $fio: ";
        echo "\n\t";
        if ($dep && $fio && $role) {
            echo "[bind] \t $role";
            if (!isUuid($user_uuid)) {
                $fio_explode = explode(" ", $fio);
                $sn = array_shift($fio_explode);
                $givenname = implode(" ", $fio_explode);
                $user_query = "(&(sn=$sn)(givenname=$givenname))";
                $users_search = ldap_search($ldap_handle, $ldap_rootdn, $user_query);
                $users = ldap_get_entries($ldap_handle, $users_search);
                if (is_array($users) && isset($users["count"]) && $users["count"] == 1 && isset($users[0]) && isset($users[0]["cn"]) && isset($users[0]["cn"][0])) {
                    $user_uuid = $users[0]["cn"][0];
                } else {
                    $user_err_string = $colors->getColoredString("$user_query", "light_cyan");
                    echo "\n\t- $err_string user search: $user_err_string found " . $colors->getColoredString((int)$users["count"], "light_red") . " persons; \n";
                    foreach ($users as $extra_user) {
                        if (!empty($extra_user["cn"])) {
                            echo "\n\t\t- " . $extra_user["cn"][0] . " - " . $extra_user["sn"][0] . ' ' . $extra_user["givenname"][0] . "\n";
                        }
                    }

                    echo "Input user uuid: ";
                    $user_uuid = readline();
                }
            } else {
                echo $colors->getColoredString(" [user:byuuid:$user_uuid]", "brown");
            }
            $dep_uuid = null;
            $dep_parent_uuid = null;
            $retry = true;
            while (is_null($dep_uuid) && $retry) {
                $dep_object = \Department::findByPath($dep);
                if ($dep_object && $dep_object->uuid) {
                    $dep_uuid = $dep_object->uuid;
                    $dep_parent = $dep_object->getParent();
                    if ($dep_parent && $dep_parent->uuid) {
                        $dep_parent_uuid = $dep_parent->uuid;
                        echo $colors->getColoredString("\n\tFound dep parent for $dep: {$dep_parent->uuid}, {$dep_parent->description}", "blue");
                        $dep_parent_doc = Document::findByUuid($mn, $prefix, $dep_parent->uuid);
                        if (is_null($dep_parent_doc)) {
                            $dep_parent_doc = Document::create($mn, $prefix, $typeObjectParent, $dep_parent->uuid);
                            $dep_parent_doc->setField("title_ru", $dep_parent->description);
                            $dep_parent_doc->save();
                            echo "\n\t" . $colors->getColoredString("[dep parent created as $dep_parent->description]", "yellow");
                        } else {
                            echo "\n\t" . $colors->getColoredString("[dep found]", "light_green");
                        }
                    } else {
                        echo "\n\t" . $colors->getColoredString("[dep parent not found, looking for a chairs]", "light_red");
                    }
                } else {
                    $dep_err_string = $colors->getColoredString("$dep", "light_blue");
                    echo "\n\t- $err_string department $dep_err_string not found ; \n";
                    echo "What to do next? [Enter]skip, [r]etry: ";
                    $action = readline();
                    if ($action != "r") {
                        $retry = false;
                    }
                }
            }

            echo "\n\t";
			if(isUuid($user_uuid)){
				if (isUuid($dep_uuid) || $dep ==='chair') {
					if(isUuid($dep_uuid)){
						$dep_object = \Department::findByUuid($dep_uuid);
						echo $colors->getColoredString("[UUID - OK]", "green") . " user:$user_uuid, dep:$dep_uuid: ";
						$object = Document::findByUuid($mn, $prefix, $dep_uuid);

						if (is_null($object)) {
							$object = Document::create($mn, $prefix, $typeObject, $dep_uuid);
							$object->setField("title_ru", $dep_object->description);
							$x = $object->save();
							echo $colors->getColoredString("[dep created as $dep_object->description at '{$object->id}']", "yellow");
						} else {
							echo $colors->getColoredString("[dep found]", "light_green");
							
						}

						if (!$object->getFieldI18nValue("parent")) {
							$object->setField("parent", $dep_parent_uuid);
							$object->save();
							echo $colors->getColoredString("[dep parent info saved]", "yellow");
						}
					}
					if($dep ==='chair'){
						$dep_uuid = 'chair';
					}
					switch($role){
						case 'admin':
							$action = array("+browse", "+create", "+read", "+display", "+update", "+delete", "+form", "+overview", "+approve");
						break;
						case 'prorector':
							$action = array("+browse", "+read");
						break;
						case 'rector':
							$action = array("+browse", "+read");
						break;
						case 'amenable':
							$action = array("+browse", "+read", "+display", "+update");
						break;
						case 'dean':
							$action = array("+browse", "+read");
						break;
						case 'moderator':
							$action = array();
						break;
						case 'head':
							$action = array();
						break;
						case 'employee':
							$action = array();
						break;
						case 'student':
							$action = array();
						break;
						case 'herzen':
							$action = array();
						break;
					}
						$rule_a = array (
							  '_id' => new \MongoId(),
							  'actions' => $action,
							  'actors'  => array ( $user_uuid ),
							  'objects' => array ( $dep_uuid ),
							  'roles' => array($role)
							);
						$rulesCollection->insert($rule_a);

						echo $colors->getColoredString("[rules added]", "yellow");
				} else {
					echo $colors->getColoredString("[uuids fails: $user_uuid or $dep_uuid]", "light_red");
				}
			}

        } else {
            echo $colors->getColoredString("[empty value - skip]", "light_purple") . "\n";
        }

        echo "\n\n";
    }
    fclose($handle);
}
