<?php
date_default_timezone_set('Europe/Moscow');
mb_internal_encoding("UTF-8");

die(PHP_EOL . "Закомментируй вывод этой строчки, чтобы запустить этот скрипт! Завершение..." . PHP_EOL . PHP_EOL);

echo "\n";
require_once('../../bus/Colors.php');
require_once("../../documongo/Load.php");

use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;
$colors = new \Colors();
require_once("lib/fconnect.php");
$db = DBConnect::getInstance();
$realData = $db->colDocs;
$documentType = "chair";
$language = "ru";

$resetLogFile = "out.reset." . date("Ymd-His") . ".txt";
$documentTypeObject = DocumentType::findByType($mn, $prefix, $documentType);
$versioningDocuments = Document::find($mn, $prefix, $documentType);

if (!is_array($versioningDocuments)) {
    $versioningDocuments = array($versioningDocuments);
}

foreach ($versioningDocuments as $versioningDocument) {
    $versioningDocumentTitle = $versioningDocument->getFieldI18nValue("title", $language);
    echo "Process (to reset) " . $colors->getColoredString($versioningDocumentTitle, "yellow") . " (" . $versioningDocument->uuid . "):\n";

    $reseted = $versioningDocument->resetFields($language);
    echo "Reset result: ";
    var_dump($reseted);
    file_put_contents($resetLogFile, $versioningDocument->uuid . PHP_EOL, FILE_APPEND);
    file_put_contents($resetLogFile, json_encode($reseted)     . PHP_EOL . PHP_EOL, FILE_APPEND);
}
