<?php
    require_once(__DIR__ . "/../../vendor/autoload.php");
    require_once("../../web/lib/fconnect.php");

require_once('../../bus/Colors.php');
$colors = new Colors();

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

$db = DBConnect::getInstance();
$rulesCollection = $db->colRules;

echo "Rules: " . $rulesCollection->count() . "\n";


$sameRules = [];

foreach ($rulesCollection->find() as $rule) {
    // var_dump($rule);

    $ruleId = $rule["_id"]->{'$id'};
    // var_dump($ruleId);
    // var_dump($rule);
    $ruleContent = $rule;
    unset($ruleContent["_id"]);

    $ruleString = json_encode($ruleContent);
    // var_dump($ruleString);

    $sameRules[$ruleString][] = $rule;
}

$dupRules = array_filter($sameRules, function($rulesWithSameContent) {
    return count($rulesWithSameContent) > 1;
});

uasort($dupRules, function($a, $b) {
    if (count($a) == count($b)) {
        return 0;
    }
    return (count($a) < count($b)) ? -1 : 1;
});

// var_dump($dupRules);

$removeDupRules = array_reduce(array_map(function($rulesWithSameContent) {
        $originalRule = array_shift($rulesWithSameContent);
        return $rulesWithSameContent;
    }, $dupRules), function($carry, $dupRulesWithSameContent) {
        return array_merge($carry, $dupRulesWithSameContent);
    }, []);
$removeDupRuleIds = array_map(function($removeDupRule) { return $removeDupRule["_id"]->{'$id'}; }, $removeDupRules);
// var_dump($removeDupRules);
// var_dump($removeDupRuleIds);
// die;

// echo implode("\n", array_map(function($ruleString, $rulesWithSameContent) use ($removeDupRuleIds, $colors) {
//         return count($rulesWithSameContent) . " - " . $ruleString . "\n" . implode("", array_map(function($rule, $ruleKey) use ($removeDupRuleIds, $colors) {
//                 $ruleContent = $rule;
//                 unset($ruleContent["_id"]);

//                 $ruleString = json_encode($ruleContent);
//                 $outstring = $rule["_id"]->{'$id'} . ' ' . $ruleString;

//                 if (in_array($rule["_id"]->{'$id'}, $removeDupRuleIds) != ($ruleKey > 0)) {
//                     var_dump($rule, $ruleKey, $ruleString);die;
//                 }
//                 return ''
//                     . "\t"
//                     . (in_array($rule["_id"]->{'$id'}, $removeDupRuleIds)
//                         ? $colors->getColoredString("DEL", 'white', 'red') . ' '
//                         : '')
//                     . ($ruleKey > 0
//                         ? $colors->getColoredString($outstring, 'red')
//                         : $outstring
//                         )
//                     . "\n"
//                     . '';
//             }, $rulesWithSameContent, array_keys($rulesWithSameContent)));
//     }, array_keys($dupRules), $dupRules));

// file_put_contents('rule-duplication-removed.txt', '');
array_walk($removeDupRules, function($removeDupRule) use ($colors, $rulesCollection) {
    echo "Deleting rule " . ($removeDupRule["_id"]->{'$id'}) . "\n\t" . json_encode($removeDupRule) . "\n";

    $query = ["_id" => new MongoId($removeDupRule["_id"]->{'$id'})];
    $foundRule = $rulesCollection->findOne($query);
    echo "\t\tQuery: " . json_encode($query) . "\n";
    echo "\t\tResult: " . json_encode($foundRule) . "\n";
    $deleted = false;
    $deleted = $rulesCollection->remove($query, array("justOne" => true));

    // var_dump($deleted);

    if ($deleted || isset($deleted["ok"]) && $deleted["ok"]) {
        echo $colors->getColoredString("\tDeleted", "red") . "\n";
    } else {
        echo $colors->getColoredString("\tNot deleted", "white", "red") . "\n";
        echo "\t" . json_encode($deleted) . "\n";
    }
    file_put_contents('rule-duplication-removed.txt', ($deleted ? "Deleted" : "Not_deleted") . " " . json_encode($removeDupRule) . "\n", FILE_APPEND);

    // die;

});

//@unlink("out.department.parent.created.txt");
//@unlink("out.department.parent.found.txt");
//@unlink("out.department.parent.fails.txt");
//@unlink("out.department.parent.skip.txt");

//@unlink("out.department.created.txt");
//@unlink("out.department.found.txt");
//@unlink("out.department.fails.txt");
//@unlink("out.department.skip.txt");

//@unlink("out.rules.found.txt");
//@unlink("out.rules.created.txt");

// $objects = array();
// $rules = array();

// $a                  = require(__DIR__ .'/../../conf/conf.php');
// $host				= $a['mongo_host'];
// $port				= $a['mongo_port'];
// $prefix 			= $a['db_prefix'];
// $server				= 'mongodb://'.$host.':'.$port;


// $mn = new MongoClient($server);

