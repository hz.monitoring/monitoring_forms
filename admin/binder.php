<?php
    die('Access denied');

    define('ENTRY_POINT', 'binder');

    require('../common.inc.php');

    use PFBC\Form;
    use PFBC\Element;
    use PFBC\View;
	$a = require(__DIR__ .'/../conf/conf.php');
    $prefix = $a['db_prefix'];
    $mn = new MongoClient();

    $user = User::getFromSession($_SESSION);


    $metaData = $mn->selectDB($prefix . "model");

    $pageType = isset($_GET["page_type"]) ? $_GET["page_type"] : null;
    $language = isset($_GET["lang"]) ? $_GET["lang"] : "ru";


    $metaTypesCursor = $metaData->metatypes->find();
    $metaTypesArray = array();
    $metaTypesText = array();

    foreach($metaTypesCursor as $metaType) {
        $metaTypeName = $metaType["name"];
        $metaTypesArray[$metaTypeName] = $metaType;
        $metaTypesText[$metaTypeName] = isset($metaType["label_$language"]) ? $metaType["label_$language"] : "$metaTypeName [$language]";
    }

    $imageTypes = array('image/jpeg', 'image/png', 'image/gif', 'image/bmp');

    // AUTHBOX
    if (!$user->isLogged()) {
        require_once '../inc/login.inc.php';
        die;
    }
?>
<!doctype html>

<html lang="ru">
<head>
  <meta charset="utf-8" />
  <title>Form Filler</title>
  <link rel="stylesheet" href="/css/jquery-ui.css" />

  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>
  <script src="/js/jquery.form.js"></script>

  <link rel="stylesheet" href="/css/page.css" />
  <link rel="stylesheet" href="/css/admin.css" />
  <link rel="stylesheet" href="/css/navigation__horizontal.css" />

  <script type="text/javascript">

    var language = "<?=$language?>";
    var pageType = "<?=$pageType?>";

    var metatypes = <?=json_encode($metaTypesText);?>;

    var userUuid = "<?=$user->isLogged() ? $user->uuid : ""?>";

  </script>

  <script src="/js/auth.js"></script>

  <script>


    $(document).ready(function() {

    });

    function userSearchResponse(data){
        if (data) {
            var $output = $("#user__output");
            if ($output.is(":visible")) {
                $output
                .hide()
                .empty();
            }

            if (data.success) {
                if (data.users) {
                    console.log(data.users);
                    $.each(data.users, function(i, user) {
                        $output
                        .append(function(){
                            return $("<div>")
                            .addClass("bind-element user-element")
                            .append(function(){
                                return $("<span>")
                                .addClass("bind-element__description")
                                .attr("data-uuid", user.uuid)
                                .text(user.lastname + ' ' + user.givenname + ' (' + user.login + ')')
                                .click(function(){
                                    if ($("#binder__actors [data-uuid=" + user.uuid + "]").length > 0) {

                                    } else {
                                        $("#binder__actors")
                                        .append(function(){
                                            return $("<label>")
                                            .append(function(){
                                                return $("<input>")
                                                .prop("name", "actor")
                                                .prop("type", "input")
                                                .attr("data-uuid", user.uuid)
                                                .val(user.uuid)
                                            })
                                            .append(" " + user.lastname + ' ' + user.givenname + ' (' + user.login + ')')
                                            // .click(function(){
                                            //     $(this).remove()
                                            // })
                                        })
                                    }
                                })
                            })
                        })
                    })
                }
                $output
                .fadeIn("slow")
            }
        }
    }


    function departmentSearchResponse(data) {
        if (data) {
            var $output = $("#department__output");
            if ($output.is(":visible")) {
                $output
                .hide()
                .empty();
            }

            if (data.success) {
                if (data.departments) {
                    console.log(data.departments);
                    $.each(data.departments, function(i, department) {
                        $output
                        .append(function(){
                            return $("<div>")
                            .addClass("bind-element department-element")
                            .append(function(){
                                return $("<span>")
                                .addClass("bind-element__description")
                                .attr("data-uuid", department.uuid)
                                .text(department.description)
                                .click(function(){
                                    if ($("#binder__objects [data-uuid=" + department.uuid + "]").length > 0) {

                                    } else {
                                        $("#binder__objects")
                                        .append(function(){
                                            return $("<label>")
                                            .append(function(){
                                                return $("<input>")
                                                .prop("name", "object")
                                                .prop("type", "input")
                                                .attr("data-uuid", department.uuid)
                                                .val(department.uuid)
                                            })
                                            .append(" " + department.description)
                                            // .click(function(){
                                            //     $(this).remove()
                                            // })
                                        })
                                    }
                                })
                            })
                        })
                    })
                }
                $output
                .fadeIn("slow")
            }
        }
    }


  </script>
</head>






<body>

<header class="header">

<div class="userbox userbox_parent_header">
<?php
    $formName = "userbox";
    $form = new Form($formName);
    $form->configure(array(
        "prevent" => array("jQuery","jQueryUI")
        , "method" => "post"
        , "ajax" => 1
        , "ajaxCallback" => "parseAuthResponse"
        , "novalidate" => "1"
        , "action" => "/auth.php"
        , "class" => "userbox__logout-wrapper"
        , "view" => new View\Inline()
    ));

    $form->addElement(new Element\Hidden("form", $formName));
    $form->addElement(new Element\Hidden("act", "logout"));
    $form->addElement(new Element\HTML('<div class="userbox__logout">'));
    $form->addElement(new Element\Button('Выйти', "submit"));
    $form->addElement(new Element\HTML('</div>'));
    $form->render();
?>

    <div class="userbox__photo userbox__photo_source_loading">&nbsp;</div>
    <div class="userbox__username">
        <span class="userbox__username__displayname"></span>
        <span class="userbox__username__login" title="<?=$user->uuid?>">
            <?=$user->login?>
        </span>
    </div>
    <div class="userbox__helper"></div>
</div>

</header>


<!--
<div id="forbidden" class="forbidden">
  <h1>Доступ не разрешен</h1>
  <p>Отсутствует разрешение на управления связями</p>

  <p><a href="?">Главная страница</a> | <a href="javascript:history.go(-1);">Вернуться назад</a></p>
</div>
 -->

<h1>Привязка</h1>

<?php
    $formName = "user_search";
    $form = new Form($formName);
    $form->configure(array(
        "prevent" => array("jQuery","jQueryUI")
        , "method" => "post"
        , "ajax" => 1
        , "ajaxCallback" => "userSearchResponse"
        , "novalidate" => ""
        , "action" => "/admin/admin-service.php?lang=$language&page_type=$pageType&subject=user&action=search"
        , "class" => ""
        , "view" => new View\Search()
    ));

    $form->addElement(new Element\Hidden("form", $formName));
    $form->addElement(new Element\HTML('<legend>Поиск человека</legend>'));
    $form->addElement(new Element\Search("", "fio", array(
        "placeholder" => "Search",
        "append" => '<button class="btn btn-primary">Найти человека</button>'
    )));
    $form->addElement(new Element\HTML('<div id="user__output"></div>'));
    $form->render();
?>


<?php
    $formName = "department_search";
    $form = new Form($formName);
    $form->configure(array(
        "prevent" => array("jQuery","jQueryUI")
        , "method" => "post"
        , "ajax" => 1
        , "ajaxCallback" => "departmentSearchResponse"
        , "novalidate" => ""
        , "action" => "/admin/admin-service.php?lang=$language&page_type=$pageType&subject=department&action=search"
        , "class" => ""
        , "view" => new View\Search()
    ));

    $form->addElement(new Element\Hidden("form", $formName));
    $form->addElement(new Element\HTML('<legend>Поиск подразделения</legend>'));
    $form->addElement(new Element\Search("", "description", array(
        "placeholder" => "Search",
        "append" => '<button class="btn btn-primary">Найти подразделение</button>'
    )));
    $form->addElement(new Element\HTML('<div id="department__output"></div>'));
    $form->render();
?>


<?php
    $formName = "binder";
    $form = new Form($formName);
    $form->configure(array(
        "prevent" => array("jQuery","jQueryUI")
        , "method" => "post"
        , "ajax" => 1
        , "ajaxCallback" => "bindResponse"
        , "novalidate" => "1"
        , "action" => "/admin/admin-service.php?lang=$language&page_type=$pageType&subject=rule&action=create"
        , "class" => ""
    ));

    $form->addElement(new Element\Hidden("form", $formName));
    $form->addElement(new Element\Hidden("preset", "chair"));
    $form->addElement(new Element\HTML('<div id="binder" class="binder__actors">
  <div class="binder__actors" id="binder__actors"></div>
  <div class="binder__objects" id="binder__objects"></div>
</div>
'));
    $form->addElement(new Element\Button('Связать', "submit"));
    $form->render();
?>


</body>
</html>
