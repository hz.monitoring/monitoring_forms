<?php return array(

# fill setting with values and symlink to ./conf.php

"mongo_host"        => "<your mongo server host, e.g. localhost>",
"mongo_port"        => "<your mongo server port, e.g., 27017>",

"db_prefix"         => "<your db prefix>",
"db_user"           => "<your db user>",
"db_pass"           => "<your db password>",

"default_lang"      => "<language code, e.g. ru>",

);
