<?php
return array(
    'email-smtp-host' => "<host>",
    'email-smtp-username' => "<user>",
    'email-smtp-password' => "<pass>",

    'email-from' => '<from>',
    'email-from-name' => '<from-name>',
);
