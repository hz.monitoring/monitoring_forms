<?php

namespace pagecontrol\PageController\Herzen;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

class ComplexRatingResult extends \pagecontrol\PageController\Herzen
{
    protected static $filename = __FILE__;

    protected $memberTemplate = "ComplexRatingResult.member.html";

    protected $title = "Результаты комплексного рейтинга кафедр";

    protected $subtitle = "Данные представлены на 26 ноября 2015 года";

}
