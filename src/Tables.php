<?php

namespace pagecontrol\PageController\Herzen;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

class Tables extends \pagecontrol\PageController\Herzen
{
	const PAGETYPE = 'chair';
    protected static $filename = __FILE__;

    protected $memberTemplate = "Tables.member.html";
    protected $guestTemplate = "Herzen.guest.html";

    function __construct()
    {
        $_GET["page_type"] = self::PAGETYPE;
        parent::__construct();

    }

    function renderMember() {

        if ($this->mode == self::MODE_OBJECT) {
            if ($this->pageType != self::PAGETYPE) {
                die;
            }
        } elseif ($this->mode == self::MODE_CLASS) {
            if ($this->pageType != self::PAGETYPE) {
                die;
            }
        }

        parent::renderMember();
    }

    function renderNavigation() {
    	parent::renderNavigation();
    }

}
