<?php

namespace pagecontrol\PageController\Herzen;

class HerzenMailer extends \PHPMailer {

    protected $settings_file = __DIR__ . '/../conf/email.conf.php';
    protected $settings;

    public function __construct() {
        $this->settings = require $this->settings_file;

        $this->CharSet = 'UTF-8';
        $this->isSMTP();
        $this->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $this->Host = $this->settings["email-smtp-host"];
        $this->SMTPAuth = true;
        $this->Username = $this->settings["email-smtp-username"];
        $this->Password = $this->settings["email-smtp-password"];
        $this->From = $this->settings["email-from"];
        $this->FromName = $this->settings["email-from-name"];
        $this->XMailer = ' ';
    }
}
