<?php

namespace pagecontrol\PageController\Herzen\Admin;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

class RolesAndMenus extends \pagecontrol\PageController\Herzen
{
    protected static $filename = __FILE__;

    protected $memberTemplate = "RolesAndMenus.member.html";
    protected $guestTemplate = "../Herzen.guest.html";

    function __construct()
    {
        $mn = static::getMongoClient();

        $this->fetchMetadata();


        $this->mode = self::MODE_NONE;
        $pageType = isset($_GET["page_type"]) ? $_GET["page_type"] : null;
        if (!is_null($pageType)) {
            $this->mode = self::MODE_CLASS;
            $this->currentPageType = DocumentType::findByType($mn, $this->prefix, $pageType);
            $pageType = $this->currentPageType ? $this->currentPageType->type : null;
        }

        $this->language = isset($_GET["lang"]) ? $_GET["lang"] : "ru";
        $this->pageType = $pageType;

        $this->schemaType = DocumentType::findByType($mn, $this->prefix, "field");
    }

    function renderNavigation() {
        $mn = $this->getMongoClient();
        $prefix = $this->prefix;
        $language = $this->language;
        $user = $this->user;
        $mode = $this->mode;
        $currentPage = $this->currentPage;
        $currentPageType = $this->currentPageType;
		//	var_dump($this->currentPageType);

if(isset($_GET['action'])){ $getlink = '&action='.$_GET['action']; }else{ $getlink = ''; }


        $nav = array();

        foreach (DocumentType::find($mn, $prefix) as $iPageType) {
            $nav_line = array();
            $nav_line["type"] = $iPageType;
            $nav_line["content"] = array();

            $isOverviewPermitted = $iPageType->isPermitted($this->user->uuid, "overview");
            $nav_line["is_overview"] = $isOverviewPermitted;

            if ($iPageType->isPermitted($this->user->uuid, "browse") || true) {
                $iPages = Document::find($mn, $prefix, $iPageType->type);

                foreach ($iPages as $iPage) {
                    $iPageId = $iPage->id;

                    if ($iPage->exists()) {
                        if ($iPage->isPermitted($this->user->uuid, "browse")) {
                            $nav_line["content"][] = $iPage;
                        } else {
                            continue;
                        }
                    }
                }
            }

            $nav[] = $nav_line;
        }


        $nav_rendered = '';
        $nav_rendered .= '<nav class="navigation navigation--horizontal">';

        foreach ($nav as $nav_line) {
            $iPageType = $nav_line["type"];
            $iPages = $nav_line["content"];
            $isOverviewPermitted = isset($nav_line["is_overview"]) ? $nav_line["is_overview"] : false;


            $iPageLabelVar = "label_$language";
            $iPageLabel = $iPageType->$iPageLabelVar ? $iPageType->$iPageLabelVar : "[" . $iPageType->type . "]";


            if (count($iPages) > 0 || $isOverviewPermitted) {
                $nav_rendered .= ''
                    // . '<div class="navbar">'
                    // . '<div class="navbar-inner">'
                    . '<ul class="pages pages--level1">';
            }

            if (count($iPages) > 5 || $isOverviewPermitted) {
                $nav_rendered .= ''
                    . '<li class="pages__page pages__page--title">'
                    . " $iPageLabel "
                    . '</li>';
            }

            if (count($iPages) > 0) {
                if (count($iPages) > 5) {
                    $nav_rendered .= $this->iteratePagesDropdowns($iPages, $iPageType->type, $iPageLabel);
                } else {
                    $nav_rendered .= $this->iteratePagesPlain($iPages, $iPageType->type, $iPageLabel);
                }
            }


            if ($isOverviewPermitted) {
                $nav_rendered .= ''
                    . '<li class="pages__page pages__page--overview">'
                        . '<a href="/overview.php?page_type=' . $iPageType->type . '&amp;lang=' . $language . '">'
                            . ' <button class="btn btn-info"><span class="icon-th"></span> Обзор</button> '
                        . '</a>'
                    . '</li>';
            }

            if (count($iPages) > 0 || $isOverviewPermitted) {
                $nav_rendered .= '</ul>';
                // $nav_rendered .= '</div>';
                // $nav_rendered .= '</div>';
            }
        }

        $nav_rendered .= '</nav>';

        echo $nav_rendered;
    }

function renderVersionNavigation() {
$mn = $this->getMongoClient();
$prefix = $this->prefix;
$language = $this->language;
$user = $this->user;
$mode = $this->mode;
$currentPage = $this->currentPage;
$currentPageType = $this->currentPageType;
$currentVersionId = false;
?>

<nav class="navigation">
  <h2 class="navigation__heading">Версии:</h2>
  <div class="navigation__content page-versions">
    <a class="page-version <?=!$currentVersionId ? 'page-type--active' : '' ?>"
       href="?page_type=<?=$currentPageType -> type ?>&amp;lang=<?=$language ?>">Текущая</a>

<?php foreach ($currentPageType->getAvailablePeriods(null) as $availablePeriod): ?>
<?php
$iPeriodDescription = isset($availablePeriod["description"]) ? $availablePeriod["description"] : null;
$iPeriodSince = isset($availablePeriod["since"]) ? $availablePeriod["since"] -> format("d.m.Y") : null;
$iPeriodTill = isset($availablePeriod["till"]) ? $availablePeriod["till"] -> format("d.m.Y") : null;
?>

<?php if ($iPeriodDescription): ?>
    <a class="page-version <?=$currentVersionId && $currentVersionId == $iVersionId ? 'page-type--active' : '' ?>"
       href="?page_type=<?=$currentPageType -> type ?>&amp;lang=<?=$language ?>&amp;since=<?=$iPeriodSince ?>&amp;till=<?=$iPeriodTill ?>"><?=$availablePeriod["description"] ?></a>
<?php endif; ?>

<?php endforeach; ?>
  </div>
  <div class="navigation__helper"></div>

</nav>

<?php
}

}
