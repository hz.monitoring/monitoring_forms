<?php

namespace pagecontrol\PageController\Herzen;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

class Bulletin extends \pagecontrol\PageController\Herzen
{
    protected static $filename = __FILE__;

    protected $memberTemplate = "Bulletin.member.html";
    protected $guestTemplate = "Herzen.guest.html";

    function __construct()
    {
        $mn = static::getMongoClient();

        $this->fetchMetadata();


        $this->mode = self::MODE_NONE;
        $pageType = isset($_GET["page_type"]) ? $_GET["page_type"] : null;
        if (!is_null($pageType)) {
            $this->mode = self::MODE_CLASS;
            $this->currentPageType = DocumentType::findByType($mn, $this->prefix, $pageType);
            $pageType = $this->currentPageType ? $this->currentPageType->type : null;
        }

        $this->language = isset($_GET["lang"]) ? $_GET["lang"] : "ru";
        $this->pageType = $pageType;

        $this->schemaType = DocumentType::findByType($mn, $this->prefix, "field");
    }
}
