<?php

namespace pagecontrol\PageController\Herzen;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

class Admin extends \pagecontrol\PageController\Herzen
{
    protected static $filename = __FILE__;

    protected $memberTemplate = "Admin.member.html";
    protected $guestTemplate = "Herzen.guest.html";

    function __construct()
    {
        $mn = static::getMongoClient();

        $this->fetchMetadata();


        $this->mode = self::MODE_NONE;
        $pageType = isset($_GET["page_type"]) ? $_GET["page_type"] : null;
        if (!is_null($pageType)) {
            $this->mode = self::MODE_CLASS;
            $this->currentPageType = DocumentType::findByType($mn, $this->prefix, $pageType);
            $pageType = $this->currentPageType ? $this->currentPageType->type : null;
        }

        $this->language = isset($_GET["lang"]) ? $_GET["lang"] : "ru";
        $this->pageType = $pageType;

        $this->schemaType = DocumentType::findByType($mn, $this->prefix, "field");
    }

    function renderNavigation() {
        $mn = $this->getMongoClient();
        $prefix = $this->prefix;
        $language = $this->language;
        $user = $this->user;
        $mode = $this->mode;
        $currentPage = $this->currentPage;
        $currentPageType = $this->currentPageType;

        echo '<nav class="navigation navigation--horizontal pages">';
        echo '<ul>';

        foreach (DocumentType::find($mn, $prefix) as $iPageType) {
            $iPageTypeName = $iPageType->type;
            $iPageLabelVar = "label_$language";
            $iPageLabel = $iPageType->$iPageLabelVar;
            if (!$iPageLabel) {
                $iPageLabel = "[$iPageTypeName]";
            }

            $isOverviewPermitted = $iPageType->isPermitted($user->uuid, "overview");

            if (true || $iPageType->isPermitted($user->uuid, "browse")) {

                $iPages = Document::find($mn, $prefix, $iPageTypeName);

                if (count($iPages) > 0) {

                    $twoLevels = false && count($iPages) > 2;

                    if ($twoLevels) {
                        echo ""
                            . '<li class="ropdown">'
                                . '<button class="btn dropdown-toggle sr-only" type="button" id="dropdownMenu1" data-toggle="dropdown">'
                                    . $iPageLabel
                                    . '<span class="caret"></span>'
                                . '</button>'
                                . '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">'
                                . '';

                    }

                    foreach ($iPages as $iPage) {
                        $iPageId = $iPage->id;

                        if ($iPage->exists()) {
                            if ($iPage->isPermitted($user->uuid, "browse")) {

                            } else {
                                continue;
                            }
                            $iPagePermitted = $iPage->isPermitted($user->uuid, "read");

                            $iPageActive = $mode == self::MODE_OBJECT && $currentPage && $currentPage->id == $iPageId;

                            $iPageLabel = (!empty($iPage->fields["title_$language"])
                                ? ($iPage->fields["title_$language"])
                                : "без названия, #$iPageId");

                            echo '<li class="pages__page ' . ($iPageActive ? 'pages__page--active' : '') . '" role="presentation">'
                                . ($iPagePermitted ? ('<a role="menuitem" href="?id=' . $iPageId . '">') : '')
                                . $iPageLabel
                                . ($iPagePermitted ? ('</a>') : '')
                                . '</li>'
                                . '';
                        } else {
                            echo "<li>—</li>";
                        }
                    }

                    if ($twoLevels) {
                        echo ''
                                . '</ul>'
                            . "</li>";
                    }
                }

                if ($isOverviewPermitted) {
                    echo '<li class="pages__page pages__overview">'
                        . '<a href="/overview.php?page_type=' . $iPageTypeName . '&amp;lang=' . $language . '">Отчет по всем данным</a>'
                        . '</li>'
                        . '';
                }
            }
        }

        echo '</ul>';
        echo '</nav>';
    }


    function renderPalette() {
        ?>

        <div id="palette" class="palette">
          <h2 class="palette__heading">Типы полей</h2>
          <div class="palette__content">
        <?php foreach($this->getMetaTypesLabels() as $metaTypeName => $metaTypeLabel): ?>
            <div class="palette__item" data-item-type="<?=$metaTypeName?>"><?=$metaTypeLabel?></div>
        <?php endforeach; ?>
          </div>
        </div>

        <?php
    }

    function renderConstructor() {
        $language = $this->language;
        $pageType = $this->pageType;
        $currentPageType = $this->currentPageType;


        ?>

        <div id="schema" class="schema">
        <?php
            $schema_label_var = "label_$language";
            $schema_label = $currentPageType->$schema_label_var
        ?>
          <h1><?=$schema_label?></h1>

          <div id="schema__sortable" class="schema__sortable">

        <?php

            $metaTypesLabels = $this->getMetaTypesLabels();

            foreach ($currentPageType->items as $item) {
                $itemType = $item["type"];
                $itemTypeLabel = isset($metaTypesLabels[$itemType]) ? $metaTypesLabels[$itemType] : "[$itemType]";
                $itemLabel = isset($item["label_".$language]) ? $item["label_".$language] : "(без названия)";
                $itemRequired = isset($item["required"]) ? (boolean)$item["required"] : false;
                $itemNoI18n = isset($item["no_i18n"]) ? (boolean)$item["no_i18n"] : false;
                $itemNoVersioning = isset($item["no_versioning"]) ? (boolean)$item["no_versioning"] : false;

                $itemClasses = array();
                $itemClasses[] = "palettePlaced";

                $editorItemsNames = array();

                $attrsString = implode(" ", array_reduce($this->schemaType->items, function(&$acc, $editorItem) use ($item, &$editorItemsNames) {
                        $editorItemName = $editorItem["name"];
                        $editorItemNoI18n = isset($editorItem["no_i18n"]) ? (boolean)$editorItem["no_i18n"] : false;;
                        $editorItemNameLang = $editorItemNoI18n ? $editorItemName : ($editorItemName . "_" . $this->language);

                        $itemValue = isset($item[$editorItemNameLang]) ? $item[$editorItemNameLang] : null;

                        if (!is_array($itemValue)) {
                            $acc[] = "data-property-" . $editorItemNameLang . '="' . ($itemValue ? htmlentities($itemValue) : '') . '"';
                            $editorItemsNames[] = $editorItemNameLang;
                        } else {
                        }

                        return $acc;
                    }, array()));

                $itemString = '<div class="' . implode(" ", $itemClasses) . '"'
                                . ' onclick="editItem(this)"'
                                . $attrsString
                                . ' data-properties-list="' . implode(" ", $editorItemsNames) . '"'
                                . '>'
                                . '<div class="palettePlaced__name">' . $itemLabel . '</div>'
                                . '<div class="palettePlaced__type">' . $itemTypeLabel . '</div>'
                            . '</div>';
                echo $itemString;
            }

        ?>
          </div>
        </div>

        <?php
    }


    function renderEditorForm() {


        $name = $this->schemaType->type;
        $formItems = $this->schemaType->items;


        $language = $this->language;
        $pageType = $this->pageType;

        $nf = new Form($name);
        $nf->configure(array(
            "prevent" => array("jQuery","jQueryUI","bootstrap")
            , "action" => '/admin/admin-service.php?subject=item&action=update&lang=' . $language . '&page_type=' . $pageType
            , "ajax" => 1
            , "ajaxCallback" => "saveResponseCallback"
            , "novalidate" => "1"
        ));

        foreach ($formItems as $formItem) {
            $formElement = $this->getFormElement($formItem);
            if ($formElement) {
                $nf->addElement($formElement);
            }
        }

        $nf->addElement(new Element\Button("Сохранить", 'submit'));
        $nf->addElement(new Element\Button("Удалить", 'button', array('onclick' => 'removeItem();')));

        echo '<div id="dialog-form">';
        $nf->render();
        echo '</div>';
    }

    function renderPreviewForm() {
        $language = $this->language;
        $pageType = $this->pageType;
        $currentPageType = $this->currentPageType;

        ?>

        <div class="form-preview">
            <div class="form-preview__toggle">&nbsp;</div>

            <div class="form-preview__content">
            <?php

            $form = new Form($pageType);
            $form->configure(array(
                "prevent" => array("jQuery","jQueryUI","bootstrap")
                , "method" => "post"
                , "novalidate" => ""
                ));
            $form->addElement(new Element\HTML("<div>"));
            foreach ($currentPageType->items as $item) {
                $itemType = $item["type"];
                $itemLabel = isset($item["label_".$language]) ? $item["label_".$language] : ($item["name"] . " [$language]");
                $itemName = $item["name"];


                $form->addElement(new Element\HTML("<div>"));
                $form->addElement(new Element\HTML("<span class=\"ui-icon\ ui-icon-carat-2-n-s\"></span>"));


                $formElement = $this->getFormElement($item);
                if ($formElement) {
                    $form->addElement($formElement);
                }

                $form->addElement(new Element\HTML("</div>"));
            }

            $form->addElement(new Element\Button('Сохранить', "submit"));
            $form->addElement(new Element\Button('Удалить', 'button'));

            $form->addElement(new Element\HTML('</div>'));


            $form->render();
            ?>
            </div>
        </div>


        <?php
    }


    function getFormElement($formItem) {
        $element = null;

        $itemType = isset($formItem["type"]) ? $formItem["type"] : null;
        $itemName = isset($formItem["name"]) ? $formItem["name"] : null;

        $language = $this->language;

        if ($itemType && $itemName) {

            $props = array();

            $itemRequired = isset($formItem["required"]) ? (boolean)$formItem["required"] : false;
            if ($itemRequired) {
                $props["required"] = "required";
            }
            $itemNoI18n = isset($formItem["no_i18n"]) ? (boolean)$formItem["no_i18n"] : false;
            if (!$itemNoI18n) {
                $itemName = $itemName . "_" . $language;
                $formItem["name"] = $itemName;
            }

            $itemLabel = isset($formItem["label_$language"]) ? $formItem["label_$language"] : "$itemName ($language)";

            switch ($itemType) {
                case "section":
                    $element = new Element\HTML('<hr/><h2 title="' . $itemName . '">' . $itemLabel . '</h2>');
                    break;

                case "select":
                    $selectOptions = $this->getMetaTypesLabels();
                    $element = new Element\Select($itemLabel, $itemName, $selectOptions, $props);
                    break;


                default:
                    $element = parent::getFormElement($formItem);
                    break;
            }
        } else {
            echo "<div>Wrong type (Admin)</div>";
            var_dump($formItem);
        }

        return $element;
    }
}
