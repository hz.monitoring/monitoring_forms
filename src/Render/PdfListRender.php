<?php

namespace pagecontrol\PageController\Herzen\Render;

class PdfListRender {

    function render($roles, $db, $user, $docs){

        $mainrole = $out = '';
        foreach($roles as $one){
            if($one == 'dean' && $mainrole !== 'prorector'){$mainrole = 'dean';}
            if($one == 'prorector'){$mainrole = 'prorector';}
            if($one == 'admin'){$mainrole = 'admin'; break;}
            if($one == 'moderator'){$mainrole = 'moderator'; break;}
            if($one == 'customer'){$mainrole = 'customer'; break;}

        }

        $linksTable = array();
        $formTypes = array("институт", "факультет", "филиал");

        switch ($mainrole) {
            case 'dean':
                // $objects = array();
                // $rules = $db->colRules->find(array('actors'=>$user->uuid));
                // foreach($rules as $rule){
                //  foreach($rule['objects'] as $object){
                //      if(strlen($object)==36){
                //          $objects[]=$object;
                //      }
                //  }
                // }
                // $objects = array_unique($objects);
                foreach($docs as $doc){
          $linkHtml = '';
                    $filename = 'pdf/'.md5($doc['id']).'.pdf';
                    if(file_exists($filename)){
                        $linkHtml .= '<li><a target="_blank" href="'.$filename.'">'.$doc['title_ru'].'</a></li>';
                    }else{
                        // $linkHtml .= '<li><a class="muted">'.$doc['title_ru'].' <br/><small class="muted">pdf не загружен</small></a></li>';
                    }
          $linksTable[] = $linkHtml;
                }
                break;

            case 'customer':
            case 'prorector':
                // $what = array('type'=>'faculty');
                // $documents = $db->colDocs->find($what);
                foreach($docs as $doc){
            $linkHtml = '';
                        $filename = 'pdf/'.md5($doc['id']).'.pdf';
                        if(file_exists($filename)){
                            $linkHtml .= '<li><a target="_blank" href="'.$filename.'">'.$doc['title_ru'].'</a></li>';
                        }else{
                            // $linkHtml .= '<li><a class="muted">'.$doc['title_ru'].' <br/><small class="muted">pdf не загружен</small></a></li>';
                        }
            $linksTable[] = $linkHtml;
                }
                break;

            case 'moderator':
            case 'admin':
                // $what = array('type'=>'faculty');
                // $documents = $db->colDocs->find($what);
                // $documents->sort(array("prorector" => -1));
                foreach($docs as $doc){
                        $linkHtml = '';
                        $filename = 'pdf/'.md5($doc['id']).'.pdf';
                        if(file_exists($filename)){
                            $linkHtml .= '<li style="background: #efefef; margin-bottom: 20px; padding: 5px;"><a target="_blank" href="'.$filename.'">'.$doc['title_ru'].'</a>';
                        }else{
                            $linkHtml .= '<li style="background: #efefef; margin-bottom: 20px; padding: 5px;"><a class="muted">'.$doc['title_ru'].' <br/><small class="muted">pdf не загружен</small></a>';
                        }
                        $linkHtml .=''
                        . '<div class="row">'
                            . '<div class="span3">'
                                . '<form action="upload.php" method="post" target="res-' . md5($doc['id']) . '" enctype="multipart/form-data" onsubmit="hideBtn(\'status-' . md5($doc['id']) . '\');">'
                                    . '<div class="row">'
                                        . '<input type="file" id="userfile" name="userfile" style="margin-left: 30px;"/>'
                                        . '<input type="hidden" name="filename1" id="filename1" value="'.md5($doc['id']).'.pdf">'
                                        . '<input type="hidden" name="target" id="target" value="' . md5($doc['id']) . '" />'
                                        . '<iframe id="res-' . md5($doc['id']) . '" name="res-' . md5($doc['id']) . '" style="width:0px; height:0px; border:0px"></iframe>'
                                    . '</div>'
                                    . '<div class="row">'
                                        . '<div class="span1">'
                                            . '<input type="submit" name="upload" id="upload-'.md5($doc['id']).'" value="Загрузить файл" class="btn btn-primary" />'
                                        . '</div>'
                                    . '</div>'
                                    . '<div class="row">'
                                        . '<div class="span4">'
                                            . '<div id="status-' . md5($doc['id']) . '" style="display: none; margin-top: 10px;" class="alert"></div>'
                                        . '</div>'
                                    . '</div>'
                                . '</form>'
                            . '</div>'
                            . '<div class="span1">'
                                . '<form action="upload.php" method="post" target="res-' . md5($doc['id']) . '" enctype="multipart/form-data" onsubmit="hideBtn(\'status-' . md5($doc['id']) . '\');">'
                                    . '<div class="row">'
                                        . '<div class="span1">'
                                            . '<input type="file" style="visibility: hidden;"/>'
                                        . '</div>'
                                    . '</div>'
                                    . '<div class="row">'
                                        . '<div class="span1">'
                                            . '<input type="hidden" name="filename1" id="filename1" value="'.md5($doc['id']).'.pdf">'
                                            . '<input type="submit" name="delete-upload" id="upload-'.md5($doc['id']).'" value="Удалить файл" class="btn  btn-small btn-danger" />'
                                            . '<input type="hidden" name="target" id="target" value="' . md5($doc['id']) . '" />'
                                        . '</div>'
                                    . '</div>'
                                . '</form>'
                            . '<div style="clear: both;"></div>'
                        . '</div>'
                      . '</div>'
                    . '</li>';

                        $linksTable[] = $linkHtml;
                }
                break;
        }

        $out .= implode("", $linksTable);

        if($out !== ''){
            $out = ''.$out.'';
        }else{
            $out='<p class="text-error">Нет данных для отображения</p>';
        }
        return $out;
    }

}
