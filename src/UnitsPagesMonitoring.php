<?php

namespace pagecontrol\PageController\Herzen;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

class UnitsPagesMonitoring extends \pagecontrol\PageController\Herzen
{
    protected static $filename = __FILE__;

    protected $memberTemplate = "UnitsPagesMonitoring.member.html";

    protected $title = "Мониторинг страниц АУП";

    protected $subtitle = "Данные представлены на 5 февраля 2015 года";

}
