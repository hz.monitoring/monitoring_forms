<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ExternalMonitoring\Monitoring;

class Queue extends Monitoring
{
    const PAGETYPE = "queue:queue";

    protected static $filename = __FILE__;

    protected function getNav() {
        return parent::getNav() + array(
        );
    }

    protected function canViewMonitor() {
        return $this->canView("queue:monitor");
    }

}
