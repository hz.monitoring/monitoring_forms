<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring\Hosting;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ExternalMonitoring\Hosting;

class Monitor extends Hosting
{
    const PAGETYPE = "hosting:monitor";

    protected static $filename = __FILE__;

    protected $memberTemplate = "Monitor.member.html";

}
