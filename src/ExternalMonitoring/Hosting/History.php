<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring\Hosting;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ExternalMonitoring\Hosting;

class History extends Hosting
{
    const PAGETYPE = "hosting:history";

    protected static $filename = __FILE__;

    protected $memberTemplate = "History.member.html";

}
