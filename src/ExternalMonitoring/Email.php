<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ExternalMonitoring\Monitoring;

class Email extends Monitoring
{
    const PAGETYPE = "email:email";

    protected static $filename = __FILE__;

    protected function getNav() {
        return parent::getNav() + array(
        );
    }

    protected function canViewMonitor() {
        return $this->canView("email:monitor");
    }

}
