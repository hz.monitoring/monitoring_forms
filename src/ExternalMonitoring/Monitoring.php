<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ComplexMonitoring;

class Monitoring extends ComplexMonitoring
{
    const PAGETYPE = "monitoring:monitoring";

    protected static $filename = __FILE__;
    protected $guestTemplate = "../../Herzen.guest.html";

    protected function getNav() {
        return array(
            "monitoring:up" => array(
                "link" => "/",
                "title" => "Комплексный мониторинг кафедр",
                "permissions" => array(
                    "*" => array(
                        "view",
                        "browse",
                    )
                ),
            ),

            "hosting:monitor" => array(
                "link" => "/hosting/",
                "title" => "Мониторинг хостинга",
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        "browse",
                    ),
                    "4ba6401f-39d7-4311-9d54-78730c8c33d4" => array( // puchkovm
                        "view",
                        "browse",
                    ),
                ),
            ),

            "sarco:monitor" => array(
                // "link" => "/sarco/",
                "title" => "Мониторинг проекторов" . ' <small><span class="label">в разработке</span></small>',
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        "browse",
                    ),
                    "4ba6401f-39d7-4311-9d54-78730c8c33d4" => array( // puchkovm
                        "browse",
                    ),
                ),
            ),

            "email:monitor" => array(
                "link" => "/email/",
                "title" => "Мониторинг почты" . ' <small><span class="label label-warning">бета</span></small>',
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        "browse",
                    ),
                    "4ba6401f-39d7-4311-9d54-78730c8c33d4" => array( // puchkovm
                        "browse", "view"
                    ),
                ),
            ),

            "ldap:monitor" => array(
                // "link" => "/ldap/",
                "title" => "Мониторинг учетных записей" . ' <small><span class="label">в разработке</span></small>',
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        "browse",
                    ),
                    "4ba6401f-39d7-4311-9d54-78730c8c33d4" => array( // puchkovm
                        "browse",
                    ),
                ),
            ),

            "queue:monitor" => array(
                "link" => "/queue/",
                "title" => "Мониторинг сервера очередей" . ' <small><span class="label">в разработке</span></small>',
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        "browse",
                    ),
                    "4ba6401f-39d7-4311-9d54-78730c8c33d4" => array( // puchkovm
                        "browse",
                    ),
                ),
            ),
        );
    }

    protected static function insertAtNav($parentNav, $offset, $insertNav) {
        $parentNav = array_slice($parentNav, 0, $offset, true) +
                    $insertNav +
                    array_slice($parentNav, $offset, NULL, true);

        return $parentNav;
    }

    protected static function insertAfterNav($parentNav, $key, $insertNav) {
        $offset = array_search($key, array_keys($parentNav))+1;

        return static::insertAtNav($parentNav, $offset, $insertNav);
    }

    protected static function insertBeforeNav($insertNav, $key) {
        $offset = array_search($key, array_keys($insertNav));

        return static::insertAtNav($parentNav, $offset, $insertNav);
    }


    protected function getFullNavigation() {
        $parentNav = is_callable('parent::getFullNavigation') ? parent::getFullNavigation() : array();
        $fullNav = array_merge($parentNav, $this->getNav());

        foreach ($fullNav as $itemKey => $item) {
            $item["id"] = $itemKey;
            $fullNav[$itemKey] = $item;
        }
        return $fullNav;
    }

    protected function can($type, $action) {
        $nav = $this->getFullNavigation();
        $allPermissions = (isset($nav[$type]) && isset($nav[$type]["permissions"]))
            ? $nav[$type]["permissions"]
            : array();

        $userPermissions = array();
        if (isset($allPermissions["*"])) {
            $userPermissions = array_merge($userPermissions, $allPermissions["*"]);
        }
        if (isset($allPermissions[$this->user->uuid])) {
            $userPermissions = array_merge($userPermissions, $allPermissions[$this->user->uuid]);
        }

        return in_array($action, $userPermissions);
    }

    protected function canView($view = "monitoring:monitoring") {
        return $this->can($view, "view");
    }

    protected function canBrowse($view = "monitoring:monitoring") {
        return $this->can($view, "browse");
    }

    public function renderNavigation() {

        $navRender = '';
        $navRender .= '<nav class="navigation navigation--horizontal">';
        $navRender .= ''
            // . '<div class="navbar">'
            // . '<div class="navbar-inner">'
            . '<ul class="pages pages--level1">'
            . '';

        foreach ($this->getFullNavigation() as $navItem) {
            $navView = $navItem["id"];
            $navUrl = isset($navItem["link"]) ? $navItem["link"] : null;
            $navLabel = $navItem["title"];

            $pageIsActive = $navView == static::PAGETYPE;
            $pageIsPermitted = $this->canView($navView);

            $pageIsCreated = !empty($navUrl);
            $pageIsDisplayed = $pageIsActive || $this->canBrowse($navView);

            $navRender .= ''
                . ($pageIsDisplayed
                    ? (''
                        . '<li class="pages__page ' . ($pageIsActive ? 'pages__page--active active' : '') . '" role="presentation">'
                            . ($pageIsPermitted ? '<a role="menuitem" tabindex="-1" href="' . $navUrl . '">' : '')
                                . (($pageIsPermitted && $pageIsCreated) ? '<button class="btn btn-link">' : '<button class="btn btn-link" disabled="disabled">')
                                    . $navLabel
                                . '</button>'
                            . ($pageIsPermitted ? '</a>' : '')
                        . '</li>'
                        . '')
                    : '')
                . '';

        }
        $navRender .= '</ul>';
        // $navRender .= '</div>';
        // $navRender .= '</div>';

        $navRender .= '</nav>';

        return $navRender;
    }

}
