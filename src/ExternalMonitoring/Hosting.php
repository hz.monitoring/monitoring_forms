<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ExternalMonitoring\Monitoring;

class Hosting extends Monitoring
{
    const PAGETYPE = "hosting:hosting";

    protected static $filename = __FILE__;

    protected function getNav() {
        $localNav = array(
            "hosting:history" => array(
                "link" => "/hosting/?view=history",
                "title" => "История мониторинга хостинга",
                "parent" => "hosting:monitor",
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        // "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        // "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        // "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        // "browse",
                    ),
                ),
            ),
            "hosting:status" => array(
                "link" => "/hosting/?view=status",
                "title" => "Текущий статус мониторинга хостинга",
                "parent" => "hosting:monitor",
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        // "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        // "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        // "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        // "browse",
                    ),
                ),
            ),
            "hosting:export" => array(
                "link" => "/hosting/?view=export",
                "title" => "Экспорт данных мониторинга хостинга",
                "parent" => "hosting:monitor",
                "permissions" => array(
                    "009466f6-e180-4bab-b761-b360073cf281" => array( // tikhontagunov
                        "view",
                        // "browse",
                    ),
                    "447f1371-6f8a-4c6c-81b8-18e43e7bc456" => array( // olegpokalo
                        "view",
                        // "browse",
                    ),
                    "abcd519c-c900-4be8-a9c9-5d9ad1232b9a" => array( // tikhomirov
                        "view",
                        // "browse",
                    ),
                    "b34a0855-5cd7-43ac-8299-2fb88cb32290" => array( // adevyatkov
                        "view",
                        // "browse",
                    ),
                ),
            ),
        );

        return static::insertAfterNav(parent::getNav(), "hosting:monitor", $localNav);
    }

    protected function canViewMonitor() {
        return $this->canView("hosting:monitor");
    }
    protected function canViewHistory() {
        return $this->canView("hosting:history");
    }
    protected function canViewStatus() {
        return $this->canView("hosting:status");
    }
    protected function canViewExport() {
        return $this->canView("hosting:export");
    }

}
