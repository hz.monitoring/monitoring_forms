<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring\Queue;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ExternalMonitoring\Queue;

class Monitor extends Queue
{
    const PAGETYPE = "queue:monitor";

    protected static $filename = __FILE__;

    protected $memberTemplate = "Monitor.member.html";

}
