<?php

namespace pagecontrol\PageController\Herzen\ExternalMonitoring\Email;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

use pagecontrol\PageController\Herzen\ExternalMonitoring\Email;

class Monitor extends Email
{
    const PAGETYPE = "email:monitor";

    protected static $filename = __FILE__;

    protected $memberTemplate = "Monitor.member.html";

}
