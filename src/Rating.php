<?php

namespace pagecontrol\PageController\Herzen;

use PFBC\Form;
use PFBC\Element;
use PFBC\View;
use PFBC\Validation;

use documongo\MongoObject\Document;
use documongo\MongoObject\DocumentType;

class Rating extends \pagecontrol\PageController\Herzen
{
    protected static $filename = __FILE__;

    protected $memberTemplate = "Rating.member.html";
    protected $guestTemplate = "Herzen.guest.html";

    function __construct()
    {
        $mn = static::getMongoClient();

        $this->fetchMetadata();


        $this->mode = self::MODE_NONE;
        $pageType = isset($_GET["page_type"]) ? $_GET["page_type"] : null;
        if (!is_null($pageType)) {
            $this->mode = self::MODE_CLASS;
            $this->currentPageType = DocumentType::findByType($mn, $this->prefix, $pageType);
            $pageType = $this->currentPageType ? $this->currentPageType->type : null;
        }

        $this->language = isset($_GET["lang"]) ? $_GET["lang"] : "ru";
        $this->pageType = $pageType;

        $this->schemaType = DocumentType::findByType($mn, $this->prefix, "field");
    }

    function renderNavigation() {
        $mn = $this->getMongoClient();
        $prefix = $this->prefix;
        $language = $this->language;
        $user = $this->user;
        $mode = $this->mode;
        $currentPage = $this->currentPage;
        $currentPageType = $this->currentPageType;
?>

<nav class="navigation"> 
  <h2 class="navigation__heading">Категории страниц</h2>
  <div class="navigation__content page-types">

<?php foreach (DocumentType::find($mn, $prefix) as $iPageType): ?>
<?php
$iPageTypeName = $iPageType -> type;
$iPageLabelVar = "label_$language";
$iPageLabel = $iPageType -> $iPageLabelVar;
if (!$iPageLabel) {
	$iPageLabel = "[$iPageTypeName]";
}
?>
    <a class="page-type <?=$currentPageType && $iPageTypeName == $currentPageType -> type ? 'page-type--active' : '' ?>"
       href="?page_type=<?=$iPageTypeName ?>&amp;lang=<?=$language ?>"><?=$iPageLabel ?></a>
<?php endforeach;	?>

  </div>
  <div class="navigation__helper"></div>

</nav>

<?php

}

function renderVersionNavigation() {
$mn = $this->getMongoClient();
$prefix = $this->prefix;
$language = $this->language;
$user = $this->user;
$mode = $this->mode;
$currentPage = $this->currentPage;
$currentPageType = $this->currentPageType;
$currentVersionId = false;
?>

<nav class="navigation">
  <h2 class="navigation__heading">Версии:</h2>
  <div class="navigation__content page-versions">
    <a class="page-version <?=!$currentVersionId ? 'page-type--active' : '' ?>"
       href="?page_type=<?=$currentPageType -> type ?>&amp;lang=<?=$language ?>">Текущая</a>

<?php foreach ($currentPageType->getAvailablePeriods(null) as $availablePeriod): ?>
<?php
$iPeriodDescription = isset($availablePeriod["description"]) ? $availablePeriod["description"] : null;
$iPeriodSince = isset($availablePeriod["since"]) ? $availablePeriod["since"] -> format("d.m.Y") : null;
$iPeriodTill = isset($availablePeriod["till"]) ? $availablePeriod["till"] -> format("d.m.Y") : null;
?>

<?php if ($iPeriodDescription): ?>
    <a class="page-version <?=$currentVersionId && $currentVersionId == $iVersionId ? 'page-type--active' : '' ?>"
       href="?page_type=<?=$currentPageType -> type ?>&amp;lang=<?=$language ?>&amp;since=<?=$iPeriodSince ?>&amp;till=<?=$iPeriodTill ?>"><?=$availablePeriod["description"] ?></a>
<?php endif; ?>

<?php endforeach; ?>
  </div>
  <div class="navigation__helper"></div>

</nav>

<?php
}

}
