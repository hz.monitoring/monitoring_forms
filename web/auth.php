<?php
    define('ENTRY_POINT', 'auth');
    require('common.inc.php');

    use PFBC\Form;
    use PFBC\Element;
    use PFBC\View;
    use PFBC\Validation;

    header("Content-type: text/json");

    $result = array();

    $formName = isset($_POST["form"]) ? $_POST["form"] : null;

    if($formName == "auth") {
        if (!Form::isValid($formName)) {
            $result["success"] = false;
            $result["errorMessage"] = "Ошибка при заполнении формы";

            echo json_encode($result);
            die;
        }
    }

    $user = User::getFromSession($_SESSION);
    $act = isset($_POST["act"]) ? $_POST["act"] : null;

    function sendEmail($message, $extraMessage, $user = null) {
        try {
            $mailer = new pagecontrol\PageController\Herzen\HerzenMailer();

            $userDepartmentName = '-';
            $userLogin = '-';
            $userType = '-';
            $userFio = '-';

            if ($user) {
                $userEntry = $user->getEntry();
                $userDepartment = $user->getDepartment();

                $userDepartmentName = (isset($userDepartment) && isset($userDepartment->description)) ? $userDepartment->description : '?';
                $userLogin = (isset($user) && isset($user->login)) ? $user->login : '?';
                $userType = (isset($userEntry['employeetype'][0]) ? $userEntry['employeetype'][0] : '?');
                $userFio = isset($user)
                    ? (''
                        . (isset($user->lastname) ? $user->lastname : '?')
                        . ' '
                        . (isset($user->givenname) ? $user->givenname : '?')
                        . '')
                    : '?';
            }

            if ($message) {
                $message = "$message - ";
            }

            $mailer->Subject = $message . $userDepartmentName . ' (' . $userLogin . ', как ' . $userType . ')';
            $mailer->Body = $userFio . ' ' . $extraMessage . ' в Forms.herzen.spb.ru';
            $mailer->addAddress('niptuckker@gmail.com');
            $mailer->addAddress('tikhontagunov@herzen.spb.ru');
            $mailer->addAddress('complexrating@herzen.spb.ru');
            if(!$mailer->send()) {
                file_put_contents(__DIR__ . '/../../login.log.txt', "Ошибка при отправке письма о входе " + $mailer->ErrorInfo, FILE_APPEND);
            } else {

            }
        } catch (\Exception $e) {
            file_put_contents(__DIR__ . '/../../login.log.txt', "Ошибка при попытке отправить письмо о входе " + $e->getMessage(), FILE_APPEND);
        }
    }

    switch ($act) {
        case 'login':
            if ($user->isLogged()) {
                $result["success"] = false;
                $result["errorMessage"] = "Пользователь уже авторизован";
            } else {
                if (isset($_POST["username"]) && isset($_POST["password"])) {
                    $username = $_POST["username"];
                    $userpass = $_POST["password"];
                    if (!$username || !$userpass) {
                        $result["success"] = false;
                        if (!$username) {
                            $result["errorMessage"] = "Имя пользователя не указано";

                            sendEmail("Кто-то не указал логин", 'не смог зайти');
                        } elseif (!$userpass) {
                            $result["errorMessage"] = "Пароль не указан";

                            sendEmail("$username не указал пароль", 'не смог зайти');
                        }
                    } else {
                        try {
                            $user = User::bind($username, $userpass);
                            if ($user->isLogged()) {
                                $result["success"] = true;
                                $result["useruuid"] = $user->uuid;
                                $result["username"] = $user->login;
                                $result["resultMessage"] = "Пользователь найден";

                                sendEmail("", 'зашел', $user);

                            } else {
                                $result["success"] = false;
                                $result["errorMessage"] = "Пользователь не найден";

                                sendEmail("Непр. пароль ($username)", 'не смог зайти', $user);
                            }
                        } catch (Exception $e) {
                            $result["success"] = false;
                            $result["errorMessage"] = "Сервер авторизации временно недоступен";

                            sendEmail("Ldap не доступен ($username)", 'не смог зайти');
                        }
                    }
                } else {
                    $result["success"] = false;
                    $result["errorMessage"] = "Пустой запрос";

                    sendEmail("Пустой запрос ($username)", 'не смог зайти');
                }
            }
            break;

        case 'logout':
            if ($user->isLogged()) {
                $isLoggedOut = $user->logout();
                $result["success"] = $isLoggedOut;
                $result["resultMessage"] = "Пользователь вышел";
            } else {
                $result["success"] = true;
                $result["resultMessage"] = "Пользователь уже вышел";
            }
            break;

        default:
            $result["success"] = false;
            $result["errorMessage"] = "Неизвестное действие: $act";
            break;
    }


echo json_encode($result);
