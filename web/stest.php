<?php
die(".");
var_dump("HELLO");
define('ENTRY_POINT', 'service');
require('common.inc.php');
require('../vendor/autoload.php');
use PFBC\Form;
use PFBC\Element;
use PFBC\Validation;

use documongo\MongoObject\DocumentType;
use documongo\MongoObject\Document;

$result = array();

$user = User::getFromSession($_SESSION);

use pagecontrol\PageController\Configurator;
$mn = new \MongoClient(Configurator::getServerString());
$prefix = Configurator::getDbPrefix();
if (!$user->isLogged()) {
    $result["success"] = false;
    $result["errorMessage"] = "Пользователь не авторизован";
}

if (empty($_GET["subject"]) || empty($_GET["lang"])) {
    $result["success"] = false;
    $result["errorMessage"] = "Некорректный запрос";
}

$subject = $_GET["subject"];
$language = $_GET["lang"];



if (!empty($_POST["form"])) {
    $formName = $_POST["form"];
    // if(!Form::isValid($formName, false)) {
    //     Form::renderAjaxErrorResponse($formName);
    // }
}



$imageTypes = array('image/jpeg', 'image/png', 'image/gif', 'image/bmp');


$metadata = $mn->selectDB($prefix . "model");
$metaTypesCursor = $metadata->metatypes->find();
$metaTypesData = array();
$metaTypesText = array();
foreach($metaTypesCursor as $metaType) {
    $metaTypeName = $metaType["name"];
    $metaTypesData[$metaTypeName] = $metaType;
    $metaTypesText[$metaTypeName] = isset($metaType["label_$language"]) ? $metaType["label_$language"] : ($metaType["name"] . ' [' . $language . ']');
}




$itemSchema = array();
$itemSchema["+text:name"] = "Идентификатор поля";
$itemSchema["+select:type"] = array("label" => "Тип поля", "options" => $metaTypesText);
$itemSchema["+text:label_$language"] = "Подпись к полю";
$itemSchema["text:placeholder_$language"] = "Подпись-заглушка";
$itemSchema["+boolean:required"] = "Обязательное поле";
$itemSchema["+boolean:no_i18n"] = "Единое поле для всех языков";
$itemSchema["+boolean:no_versioning"] = "Не включено в версионинг";
$itemSchema["+hidden:index"] = "Номер поля по порядку";
$itemSchema["textarea:short_description_$language"] = "Краткое описание";
$itemSchema["wysiwyg:long_description_$language"] = "Полное описание";


$data = $mn->selectDB($prefix . "data");
$pages = $data->documents;




if (empty($_GET["page_type"])) {
    die('no way');
}

$pageType = $_GET["page_type"];
$pageTypes = $metadata->types;
$pageDesc = $pageTypes->findOne(array("name" => $pageType));


if (!empty($result)) {
    header("Content-type: text/json");
    echo json_encode($result);
    die;
}

switch ($subject) {

    case 'metatypes':
        $action = $_GET["action"];
        switch ($action) {
            case 'list':
                $result = array("success" => true, "metatypes" => $metaTypesText);
                break;
        }
        break; // case 'metatypes'

    case 'section':
        $action = $_GET["action"];
        switch ($action) {
            case 'list':
                $result = $pageDesc["sections"];
                break;
        }
        break; // case 'metatypes'

    case 'page':
        $action = $_GET["action"];
        switch ($action) {
			case 'edithistory':

              $pageId = isset($_POST["id"]) ? $_POST["id"] : null;

                $pageTypeObject = DocumentType::findByType($mn, $prefix, $pageType);


                $page = Document::findById($mn, $prefix, $pageId);

                if ($page) {
                    if (!$page->isPermitted($user->uuid, "update")) {
                        $result["success"] = false;
                        $result["errorMessage"] = "Редактирование не разрешено";
                        break;
                    }

              //      if (!$pageTypeObject->isAvailable()) {
              //          $result["success"] = false;
              //          $result["errorMessage"] = "Ввод данных прекращен";
              //          break;
              //      }
                } else {
                    if (!$pageTypeObject->isPermitted($user->uuid, "create")) {
                        $result["success"] = false;
                        $result["errorMessage"] = "Создание не разрешено";
                        break;
                    }
                    $page = Document::create($mn, $prefix, $pageTypeObject);
                }

                $updatedFields = array();
                $ignoredFields = array();
                $badFields = array();

                $successFields = array();
                $mistakeFields = array();
                $warningFields = array();

                $validResults = array();

                foreach ($pageTypeObject->items as $pageItem) {
                    $valid = null;
                    $validResult = '';

                    $pageItemName = isset($pageItem["name"]) ? $pageItem["name"] : null;
                    $pageItemType = isset($pageItem["type"]) ? $pageItem["type"] : null;

                    if (!$pageItemName || !$pageItemType) {
                        continue;
                    }

                    $pageItemMetaType = $metaTypesData[$pageItem["type"]];
                    $pageFieldName = (isset($pageItem["no_i18n"]) && $pageItem["no_i18n"]) ? ($pageItemName) : ($pageItemName . "_" . $language);
                    $pageItemMultiple = isset($pageItem["multiple"]) ? (boolean)$pageItem["multiple"] : false;


                    if (!$page->isPermitted($user->uuid, "update", $pageItemName, $language)) {
                        if (isset($_POST[$pageItemName]) || isset($_FILES[$pageItemName])) {
                            $ignoredFields[] = $pageItemName;
                        }
                        continue;
                    }

                    if (isset($_POST[$pageItemName])) {

                        $pageFieldValue = (float)trim($_POST[$pageItemName]);

                        if (isset($pageItemMetaType["is_binary"]) && $pageItemMetaType["is_binary"]) {
                            continue;
                        }

                        if (in_array($pageItemType, array("integer", "integer_gt_zero", "float", "float_gt_zero"))) {
 
                            $valid = true;
                        } else {
                            $valid = true;
                        }
						//	$pageFieldName=array('versions' => array( $_GET['period']=>array('content'=>array($pageFieldName))));
                        $changed = $page->setVerField($_GET['period'], $pageFieldName, $pageFieldValue);
                        
                        
                        if ($changed) {
                            $updatedFields[] = $pageItemName;
                        } else {

                        }

                        if (is_null($valid)) {
                            $warningFields[] = $pageItemName;

                        } elseif ($valid) {
                            $successFields[] = $pageItemName;

                        } else {
                            $mistakeFields[] = $pageItemName;
                        }

                        if ($validResult) {
                            $validResults[$pageFieldName] = $validResult;
                        }

                    } elseif (isset($_FILES[$pageItemName])) {
                        $fileObject = $_FILES[$pageItemName];

                        $imgbinary = fread(fopen($fileObject["tmp_name"], "r"), filesize($fileObject["tmp_name"]));


                        $changed = $page->setVerField($_GET['period'],$pageFieldName, array(
                                "name" => $fileObject["name"]
                                , "type" => $fileObject["type"]
                               , "size" => $fileObject["size"]
                                , "content" => base64_encode($imgbinary)
                            ));
                        if ($changed) {
                            $updatedFields[] = $pageItemName;
                        }

                    } else {

                    }

                }
   $status = $page->save();

                if ($status) {

                    $result["success"] = true;
                    $result["id"] = (string)$page->id;
                    $result["resultMessage"] = "Данные сохранены";
                    $result["updatedFields"] = $updatedFields;
                    $result["ignoredFields"] = $ignoredFields;

                    $result["successFields"] = $successFields;
                    $result["warningFields"] = $warningFields;
                    $result["mistakeFields"] = $mistakeFields;

                    $result["validResults"] = $validResults;
                } else {

                    $result["success"] = false;
                    $result["errorMessage"] = "Проблемы при сохранении: ошибка хранилища";
                }
                break;

            case 'update':
                $pageId = isset($_POST["id"]) ? $_POST["id"] : null;

                $pageTypeObject = DocumentType::findByType($mn, $prefix, $pageType);


                $page = Document::findById($mn, $prefix, $pageId);

                if ($page) {
                    if (!$page->isPermitted($user->uuid, "update")) {
                        $result["success"] = false;
                        $result["errorMessage"] = "Редактирование не разрешено";
                        break;
                    }

                    if (!$pageTypeObject->isAvailable()) {
                        $result["success"] = false;
                        $result["errorMessage"] = "Ввод данных прекращен";
                        break;
                    }
                } else {
                    if (!$pageTypeObject->isPermitted($user->uuid, "create")) {
                        $result["success"] = false;
                        $result["errorMessage"] = "Создание не разрешено";
                        break;
                    }
                    $page = Document::create($mn, $prefix, $pageTypeObject);
                }

                $updatedFields = array();
                $ignoredFields = array();
                $badFields = array();

                $successFields = array();
                $mistakeFields = array();
                $warningFields = array();

                $validResults = array();

                foreach ($pageTypeObject->items as $pageItem) {
                    $valid = null;
                    $validResult = '';

                    $pageItemName = isset($pageItem["name"]) ? $pageItem["name"] : null;
                    $pageItemType = isset($pageItem["type"]) ? $pageItem["type"] : null;

                    if (!$pageItemName || !$pageItemType) {
                        continue;
                    }

                    $pageItemMetaType = $metaTypesData[$pageItem["type"]];

                    $pageFieldName = (isset($pageItem["no_i18n"]) && $pageItem["no_i18n"]) ? ($pageItemName) : ($pageItemName . "_" . $language);

                    $pageItemMultiple = isset($pageItem["multiple"]) ? (boolean)$pageItem["multiple"] : false;


                    if (!$page->isPermitted($user->uuid, "update", $pageItemName, $language)) {
                        if (isset($_POST[$pageItemName]) || isset($_FILES[$pageItemName])) {
                            $ignoredFields[] = $pageItemName;
                        }
                        continue;
                    }

                    if (isset($_POST[$pageItemName])) {

                        $pageFieldValue = (float)trim($_POST[$pageItemName]);

                        if (isset($pageItemMetaType["is_binary"]) && $pageItemMetaType["is_binary"]) {
                            //if ($_POST[$pageItemName] == "empty") {
                            //unset($docObject[$pageFieldName]);
                            //} else {
                            continue;
                            //}
                        }

                        if (in_array($pageItemType, array("integer", "integer_gt_zero", "float", "float_gt_zero"))) {
                            // list($valid, $validResult) = validateNumber($pageFieldValue);
                            $valid = true;
                        } else {
                            $valid = true;
                        }

                        $changed = $page->setField($pageFieldName, $pageFieldValue);
                        if ($changed) {
                            $updatedFields[] = $pageItemName;
                        } else {

                        }

                        if (is_null($valid)) {
                            $warningFields[] = $pageItemName;

                        } elseif ($valid) {
                            $successFields[] = $pageItemName;

                        } else {
                            $mistakeFields[] = $pageItemName;
                        }

                        if ($validResult) {
                            $validResults[$pageFieldName] = $validResult;
                        }

                    } elseif (isset($_FILES[$pageItemName])) {
                        $fileObject = $_FILES[$pageItemName];

                        $imgbinary = fread(fopen($fileObject["tmp_name"], "r"), filesize($fileObject["tmp_name"]));


                        $changed = $page->setField($pageFieldName, array(
                                "name" => $fileObject["name"]
                                , "type" => $fileObject["type"]
                                , "size" => $fileObject["size"]
                                , "content" => base64_encode($imgbinary)
                            ));
                        if ($changed) {
                            $updatedFields[] = $pageItemName;
                        } else {
                            // $ignoredFields[] = $pageItemName;
                        }
                        // $docObject[$pageFieldName] = array(
                        //     "name" => $fileObject["name"]
                        //     , "type" => $fileObject["type"]
                        //     , "size" => $fileObject["size"]
                        //     , "content" => base64_encode($imgbinary)
                        //     , "_id" => new MongoId()
                        // );
                    } else {

                    }

                }
                $status = $page->save();
                // $status = $pages->update(array("_id" => $docId), $docObject);
                // $pageId = $docObject["_id"];

                if ($status) {

                    $result["success"] = true;
                    $result["id"] = (string)$page->id;
                    $result["resultMessage"] = "Данные сохранены";
                    $result["updatedFields"] = $updatedFields;
                    $result["ignoredFields"] = $ignoredFields;

                    $result["successFields"] = $successFields;
                    $result["warningFields"] = $warningFields;
                    $result["mistakeFields"] = $mistakeFields;

                    $result["validResults"] = $validResults;
                } else {

                    $result["success"] = false;
                    $result["errorMessage"] = "Проблемы при сохранении: ошибка хранилища";
                }
			break;

            case 'delete':
                $pageId = isset($_POST["id"]) ? $_POST["id"] : null;

                $page = Document::findById($mn, $prefix, $pageId);

                if (!$page) {
                    $result["success"] = false;
                    $result["errorMessage"] = "Страница не найдена";
                } else {
                    if (!$page->isPermitted($user->uuid, "delete")) {
                        $result["success"] = false;
                        $result["errorMessage"] = "Удаление не разрешено";
                        break;
                    }

                    $status = $page->delete();

                    if ($status) {
                        $result["success"] = true;
                        $result["resultMessage"] = "Страница удалена";
                    } else {
                        $result["success"] = false;
                        $result["errorMessage"] = "Проблемы при удалении: ошибка хранилища";
                    }
                }
                break;

            default:
                $result["success"] = false;
                $result["errorMessage"] = "Unknown action for '$subject': '$action'";
        }
        break; // case 'page':

    case 'attachment':
        $action = $_GET["action"];
        switch($action) {
            case "download":
                if (isset($_GET["id"]) && isset($_GET["type"])) {
                    $pageId = $_GET["id"];
                    $attachmentField = $_GET["type"];
                    $pageFields = array_filter($pageDesc["items"], function($item) use ($attachmentField) { return (isset($item["name"]) && $item["name"] == $attachmentField); });
                    $pageField = array_shift($pageFields);
                    $pageFieldName = (isset($pageField["no_i18n"]) && $pageField["no_i18n"]) ? $pageField["name"] : ($pageField["name"] . "_" . $language);

                    $page = $pages->findOne(array("_id" => new MongoId($pageId)));

                    if (!is_null($page)) {
                        $attachment = $page[$pageFieldName];
                        $attachmentType = $attachment["type"];
                        header('Content-Description: File Transfer');
                        header('Content-Type: application/octet-stream');
                        header('Content-Disposition: attachment; filename=' . $attachment["name"]);
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . $attachment["size"]);
                        echo base64_decode($attachment["content"]);

                        die;
                    }
                }
                echo "Download break";
                die;
                break;

            case "preview":
                if (isset($_GET["id"]) && isset($_GET["type"])) {
                    $pageId = $_GET["id"];
                    $attachmentField = $_GET["type"];
                    $pageFields = array_filter($pageDesc["items"], function($item) use ($attachmentField) { return (isset($item["name"]) && $item["name"] == $attachmentField); });
                    $pageField = array_shift($pageFields);
                    $pageFieldName = (isset($pageField["no_i18n"]) && $pageField["no_i18n"]) ? $pageField["name"] : ($pageField["name"] . "_" . $language);

                    $page = $pages->findOne(array("_id" => new MongoId($pageId)));

                    if (!is_null($page)) {
                        $attachment = $page[$pageFieldName];
                        $attachmentType = $attachment["type"];
                        header('Content-Type: ' . $attachmentType);
                        header('Content-Disposition: inline; filename=' . $attachment["name"]);
                        header('Content-Transfer-Encoding: binary');
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . $attachment["size"]);
                        echo base64_decode($attachment["content"]);
                        die;
                    }
                }
                echo "Preview error";
                die;
                break;

        case "delete":
            if (isset($_GET["id"]) && isset($_GET["type"])) {
                $pageId = $_GET["id"];
                $attachmentField = $_GET["type"];
                $pageFields = array_filter($pageDesc["items"], function($item) use ($attachmentField) { return (isset($item["name"]) && $item["name"] == $attachmentField); });
                $pageField = array_shift($pageFields);
                $pageFieldName = (isset($pageField["no_i18n"]) && $pageField["no_i18n"]) ? $pageField["name"] : ($pageField["name"] . "_" . $language);

                $page = $pages->findOne(array("_id" => new MongoId($pageId)));

                if (!is_null($page)) {
                    unset($page[$attachmentField]);
                    $status = $pages->update(array("_id" => new MongoId($pageId)), $page);

                    $result["success"] = $status["ok"];
                    if ($result["success"]) {
                        $result["resultMessage"] = "Данные сохранены";
                    } else {
                        $result["errorMessage"] = "Ошибка сохранения: ошибка хранилища данных";
                    }
                }
            }
            break;

        default:
            $result["success"] = false;
            $result["errorMessage"] = "Unknown action for '$subject': '$action'";
    }
    break; // case 'attachment'

    default:
        $result["success"] = false;
        $result["errorMessage"] = "Unknown subject '$subject'";
}

header("Content-type: text/json");
echo json_encode($result);
