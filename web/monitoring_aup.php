<?php
    define('ENTRY_POINT', 'monitoring');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");
    session_start();

require_once(__DIR__ . "/../vendor/autoload.php");
require_once("lib/fconnect.php");

    $db = DBConnect::getInstance();
    use PFBC\Form;
    use PFBC\Element;
    use PFBC\View;
    use PFBC\Validation;
    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;
    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();
    $language = Configurator::getDefaultLang();
    $pageType = "faculty";
    $currentPageType = DocumentType::findByType($mn, $prefix, $pageType);

?><!doctype html>

<html lang="ru">
<head>
  <meta charset="utf-8" />
  <title>Комплексный мониторинг кафедр</title>
  <link type="text/css" rel="stylesheet" href="/css/bootstrap-combined.min.css">
  <script src="/js/jquery-1.9.1.js"></script>

  <link rel="stylesheet" href="/css/overview.table.css" />
  <link rel="stylesheet" href="/css/monitoring.css" />
  <script src="/js/overview.table.js"></script>
  <style>
    td {
        white-space: nowrap;
    }
  </style>

</head>

<body>

<div class="overview">

<div id="overview-table-top-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--top" id="overview-table-top"></table>
</div>

<div id="overview-table-left-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--left" id="overview-table-left"></table>
</div>

<div id="overview-table-top-left-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--top-left" id="overview-table-top-left"></table>
</div>
<table class="overview__table overview__table--horizontal" id="overview-table">
  <thead>
    <tr>
        <td>
            <b>Подразделение</b>
        </td>
        <td>
            <b>Количество оценок</b>
        </td>
        <td>
            <b>Количество студентов</b>
        </td>
        <td>
            <b>Процент</b>
        </td>
    </tr>
  </thead>
  <tbody>
<?php

    $facultyContingent = array(
        'аспирантура' => 709,
        'безопасности жизнедеятельности' => 445,
        'биологии' => 512,
        'волховский филиал' => 363,
        'выборгский филиал' => 234,
        'дагестанский филиал' => 234,
        'географии' => 518,
        'изобразительного искусства' => 734,
        'институт детства' => 1476,
        'институт дефектологического образования и реабилитации' => 1536,
        'институт иностранных языков' => 1175,
        'институт компьютерных наук и технологического образования' => 526,
        'институт музыки, театра и хореографии' => 976,
        'институт народов севера' => 210,
        'институт педагогики' => 985,
        'институт психологии' => 894,
        'институт физической культуры и спорта' => 489,
        'институт философии человека' => 492,
        'институт экономики и управления' => 1802,
        'истории и социальных наук' => 844,
        'математики' => 354,
        'институт международных связей' => 366,
        'русского языка как иностранного' => 366,
        'социальных наук' => 2,
        'физики' => 215,
        'филологический' => 622,
        'философии человека' => 1,
        'химии' => 217,
        'юридический' => 878,
    );

    $markStatus = 'student';

    $allMarksArray = [];
    $allDepartments = [];
    $allStatusMarks = $db->colUnitRate->find(array('status'=>$markStatus));
    foreach($allStatusMarks as $oneStatusMarks){
        $user_uuid = $oneStatusMarks['uid'];
        $user = User::findByUuid($user_uuid);
        // $ou_uuid = isset($user->department_uuid) ? $user->department_uuid : null;

        $userParent = $user->getDepartment();
        // var_dump($user->login, $user->getDepartment());
        $userParentUuid = $userParent ? $userParent->description : null;

        $allMarksArray[$userParentUuid][] = $oneStatusMarks;
        $allDepartments[$userParentUuid] = $userParent;
    }

    // var_dump(array_keys($allDepartments));die;


    $ovDocuments = Document::find($mn, $prefix, $currentPageType->type);
    $facultyRows = array();
    foreach ($ovDocuments as $ovDocument) {

        $faculty_uuid = isset($ovDocument->fields['uuid']) ? $ovDocument->fields['uuid'] : null;
        $faculty_title = $ovDocument->getFieldI18nValue('title', $language);

        $faculty_key = trim(str_ireplace('факультет', '', mb_strtolower($faculty_title)));

        $facultyRows[$faculty_title] = [
            'title' => $faculty_title,
            'mark_count' => '',
            'student_count' => isset($facultyContingent[$faculty_key]) ? $facultyContingent[$faculty_key] : '?',
        ];
    }


    foreach ($allMarksArray as $faculty_uuid => $marks) {
        $faculty = $allDepartments[$faculty_uuid];
        $faculty_key = trim(str_ireplace('факультет', '', mb_strtolower($faculty_uuid)));

        if (!isset($facultyRows[$faculty_uuid])) {
            $facultyRows[$faculty_uuid]['title'] = (($faculty && $faculty->description) ? $faculty->description : '<span title="' . $faculty_uuid . '">Неизвестно</span>');
            $facultyRows[$faculty_uuid]['student_count'] = isset($facultyContingent[$faculty_key]) ? $facultyContingent[$faculty_key] : '?';
        }
        $facultyRows[$faculty_uuid]['mark_count'] = count($marks);
    }

    // var_dump($allMarksArray);die;

    $facultyTrs = array_map(function($cells, $faculty_uuid) {
        return '<tr>'
                . '<td class="overview-table-cell-title" data-item="title">' . $cells['title'] . '</td>'
                . '<td class="overview-table-cell-count" data-item="count">' . $cells['mark_count'] . '</td>'
                . '<td class="overview-table-cell-count" data-item="count">' . $cells['student_count'] . '</td>'
                . '<td class="overview-table-cell-count" data-item="count" uuid="' . $faculty_uuid . '">' . ($cells['student_count'] > 0 ? (round($cells['mark_count']/$cells['student_count']*100, 2) . "%") : '') . '</td>'
            . '</tr>';
    }, $facultyRows, array_keys($facultyRows));

    sort($facultyTrs);

    $total_marks = array_sum(array_map(function($row) { return $row["mark_count"]; }, $facultyRows));
    $total_students = array_sum(array_map(function($row) { return $row["student_count"]; }, $facultyRows));

    $facultyTrs[] = '<tr><td class="overview-table-cell-title"><b>Всего</b></td><td><b>' . $total_marks . '</b></td><td><b>' . $total_students . '</b></td><td><b>' . round($total_marks/$total_students*100, 2) . '%</b></td></tr>';

    echo implode("\n", array_map(function($_it) {
            return $_it;
        }, $facultyTrs));

?>
  </tbody>
</table>

</div>

</body>
</html>
