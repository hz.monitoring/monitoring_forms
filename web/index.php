<?php
    define('ENTRY_POINT', 'view');
	header('Content-Type: text/html; charset=utf-8');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");
    session_start();
    require_once(__DIR__ . "/../vendor/autoload.php");
    use pagecontrol\PageController;
    $user = User::getFromSession($_SESSION);

    require_once('lib/frole.php');
    $role = new Role();
    if(isset($user->uuid)){
		$index = $role->indexPage($user->uuid);
		if($index){
			header("Location: $index");
		}
	}
    $formCtrl = new PageController\Herzen\ComplexMonitoring();
    $formCtrl->attachUser($user);
    $formCtrl->render();
