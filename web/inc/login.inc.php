<?php

    use \PFBC\Form;
    use \PFBC\Element;
    use \PFBC\View;
    use \PFBC\Validation;

    $currentDatetime = new \DateTime();
    $stopDatetime = new \DateTime("2013-12-28");


?>
<!doctype html>
<html lang="ru">
<head>
  <meta charset="utf-8" />
  <title>Комплексный мониторинг кафедр</title>
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <!-- <link rel="stylesheet" href="/css/bootstrap.min.css" /> -->
  <!-- <link rel="stylesheet" href="/css/bootstrap-theme.min.css" /> -->
  <!-- <link type="text/css" rel="stylesheet" href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.1/css/bootstrap-combined.min.css"> -->


  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>
  <script src="/js/jquery.form.js"></script>

  <script src="/js/bootstrap.min.js"></script>

  <script type="text/javascript">

    var language = "<?=$language?>";
    var pageType = "<?=$pageType?>";

    var metatypes = <?=json_encode($metaTypesText);?>;

    var pageId = "<?=$mode == MODE_OBJECT && $currentPage ? $currentPage->id : '';?>";

    var userUuid = "<?=$user->isLogged() ? $user->uuid : ""?>";

  </script>

  <style type="text/css">
    body {
      text-align: center;

    }
    .authbox {
      margin: auto;
    }

  .main-page {
      margin: 60px 100px;
      padding: 20px;
      border: 15px solid rgb(253, 193, 115);

      /*display: none;*/
      background-color: rgb(245, 221, 172);
  }

  .main-page__heading {

  }
  .main-page__content {
      margin-top: 30px;
      margin-bottom: 30px;
      font-size: 2em;
      line-height: 1.2em;
  }
  .main-page__content__text {
      font-size: .6em;
  }
  </style>

  <script src="/js/auth.js?rand=<?=rand();?>"></script>

</head>

<body>

<?php if ($currentDatetime >= $stopDatetime): ?>
<div class="main-page">
    <h1 class="main-page__heading">Сбор данных за период 20.11 — 20.12 завершен</h1>
    <div class="main-page__content">Спасибо за участие!</div>
    <div class="main-page__content">
      <div class="main-page__content__text">Мы ответим на ваши вопросы по адресу <a href="complexRating@herzen.spb.ru">complexRating@herzen.spb.ru</a> (Александр Владимирович Тихомиров).</div>
      <div class="main-page__content__text">Работа с формой в январе будет определяться <a href="http://www.herzen.spb.ru/main/innovation/1378796720/">регламентом</a>.</div>
    </div>
</div>

<?php endif; ?>

<?php
    $formName = "auth";
    $form = new Form($formName);
    $form->configure(array(
        "prevent" => array("jQuery","jQueryUI")
        , "method" => "post"
        , "ajax" => 1
        , "ajaxCallback" => "parseAuthResponse"
        , "novalidate" => ""
        , "action" => "/auth.php"
        , "class" => "authbox"
        , "view" => new View\Vertical()
    ));

    $form->addElement(new Element\Hidden("act", "login"));
    $formLabel = "Комплексный мониторинг кафедр";
    $form->addElement(new Element\Hidden("form", $formName));
    $form->addElement(new Element\HTML('<legend>Представьтесь</legend>'));
    $form->addElement(new Element\HTML('<div class="authbox__username">'));
    $form->addElement(new Element\Textbox("Имя пользователя", "username", array("required" => true)));
    $form->addElement(new Element\HTML('</div>'));
    $form->addElement(new Element\HTML('<div class="authbox__password">', array("required" => true)));
    $form->addElement(new Element\Password("Пароль", "password"));
    $form->addElement(new Element\HTML('</div>'));
    $form->addElement(new Element\HTML('<legend class="authbox__output" id="authbox__output"></legend>'));
    $form->addElement(new Element\HTML('<div class="authbox__submit">'));
    $form->addElement(new Element\Button('Войти', "submit"));
    $form->addElement(new Element\HTML('</div>'));

    $form->render();
?>

</body>
</html>
