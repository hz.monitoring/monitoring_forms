<?php
    define('ENTRY_POINT', 'admin');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");
    session_start();
    require_once(__DIR__ . "/../vendor/autoload.php");
    use pagecontrol\PageController;
    $user = User::getFromSession($_SESSION);
    $formCtrl = new PageController\Herzen\Admin\Pro();
    $formCtrl->attachUser($user);
    $formCtrl->render();



