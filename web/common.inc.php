<?php
    if (!defined('ENTRY_POINT')) die('Access denied');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");
    session_start();
    require_once(__DIR__ . "/../vendor/autoload.php");
