<?php
    define('ENTRY_POINT', 'monitoring');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");
    session_start();

require_once(__DIR__ . "/../vendor/autoload.php");
require_once("lib/fconnect.php");

    $hiddenTitles = [
        "Тагунов Тихон Валерьевич",
        "Безответная Ответта Ответична",
        "Преподанный Преподаватель Препович",
        "Факультет тестовый",
        "Кафедра тестовая"
    ];

    $effectiveContractFaculties = [
//        '215747d4-c41f-47db-a175-0fbedb77c614' => 'Институт педагогики',
//        'e2be7e01-ac0f-408e-9da9-ec58fb5e93bb' => 'Институт психологии',
//        '47de3176-bbc3-44e0-8063-8920ac56fdc8' => 'Факультет химии',
    ];



    $db = DBConnect::getInstance();
    use PFBC\Form;
    use PFBC\Element;
    use PFBC\View;
    use PFBC\Validation;
    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;
    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();
    $language = Configurator::getDefaultLang();
    $availableTypes = ["chair", "teacher"];
    $pageType = (isset($_GET["type"]) && in_array($_GET["type"], $availableTypes)) ? (string)$_GET["type"] : "chair";
    $currentPageType = DocumentType::findByType($mn, $prefix, $pageType);

    $showHidden = isset($_GET["hidden"]) ? (boolean)$_GET["hidden"] : false;
?><!doctype html>

<html lang="ru">
<head>
  <meta charset="utf-8" />
  <title>Комплексный мониторинг кафедр</title>
<link rel="SHORTCUT ICON" href="/favicon.ico" />
<link rel="icon" href="/favicon.ico" type="image/ico" />

  <link type="text/css" rel="stylesheet" href="/css/bootstrap-combined.min.css">
  <script src="/js/jquery-1.9.1.js"></script>

  <link rel="stylesheet" href="/css/overview.table.css" />
  <link rel="stylesheet" href="/css/monitoring.css" />
  <script src="/js/overview.table.js"></script>
  <style>
    td {
        white-space: nowrap;
    }
  </style>

</head>

<body>

<div class="overview">

<div id="overview-table-top-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--top" id="overview-table-top"></table>
</div>

<div id="overview-table-left-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--left" id="overview-table-left"></table>
</div>

<div id="overview-table-top-left-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--top-left" id="overview-table-top-left"></table>
</div>
<table class="overview__table overview__table--horizontal" id="overview-table">
  <thead>
    <tr>
<?php
//var_dump($currentPageType);
    $fields = array_filter($currentPageType->items, function($item) {

          return in_array($item["name"], array("title", "uuid", "visited", "confirmed", "uploaded","uploaded2"));
        });
    $cells = "";

    $title_cell = "";
    $confirmed_cell = "";
    $visited_cell = "";
	$uploaded_cell = "";
	$uploaded2_cell = "";
	$pdf_cell = "";

    foreach ($fields as $ovItem) {
        $labelVar = "label_" . $language;
        $ovItemLabel = $ovItem[$labelVar];
        $ovItemName = $ovItem["name"];

        $cell = '<td class="overview-table-cell-' . $ovItemName . '" data-item="' . $ovItemName . '">' . $ovItemLabel . "</td>";

        if ($ovItemName == "title") {
            $title_cell = $cell;
            continue;
        }
        if ($ovItemName == "confirmed") {
            $confirmed_cell = $cell;
            continue;
        }
        if ($ovItemName == "visited") {
            $visited_cell = $cell;
            continue;
        }
        if ($ovItemName == "uploaded") {
		    $uploaded_cell = $cell;
            continue;
        }
        if ($ovItemName == "uploaded2") {
		    $uploaded2_cell = $cell;
            continue;
        }
        if ($ovItemName == "uuid") {
           // $pdf_cell = '<td>Отчет</td>';           //for PDF
            continue;
        }

        $cells .= $cell;
    }


  echo  $cells = ''
           . $title_cell
           . $visited_cell
           . $confirmed_cell
           . $pdf_cell
        //   . $uploaded_cell
        //   . $uploaded2_cell
        //  .$unitconfirm_cell
           . $cells
           . '';
?>
    </tr>
  </thead>
  <tbody>
<?php
    $ovDocuments = Document::find($mn, $prefix, $currentPageType->type);
    $rowsByParent = array();

    foreach ($ovDocuments as $ovDocument) {
        $docTitle = $ovDocument->getFieldI18nValue("title", $language);
        if (in_array($docTitle, $hiddenTitles)) {
            if (!$showHidden) {
                continue;
            }
        }

        $docConfirmed = $ovDocument->getFieldI18nValue("confirmed");
        $docVisited = $ovDocument->getFieldI18nValue("visited");

        $docParent = $ovDocument->getFieldI18nValue("parent");

        $row = "";
        $row .= '<tr>';
        $cells = "";
        $title_cell = "";
        $confirmed_cell = "";
        $pdf_cell = "";
        $visited_cell = "";
		$uploaded_cell = "";
		$uploaded2_cell = "";

        foreach ($fields as $ovItem) {
            $labelVar = "label_" . $language;
            $ovItemLabel = $ovItem[$labelVar];
            $ovItemName = $ovItem["name"];
            $ovItemI18nName = (isset($ovItem["no_i18n"]) && $ovItem["no_i18n"]) ? ($ovItemName) : ($ovItemName . "_" . $language);
            $ovItemValue = isset($ovDocument->fields[$ovItemI18nName]) ? $ovDocument->fields[$ovItemI18nName] : "";

            if (!is_scalar($ovItemValue)) {
                $ovItemValue = "...";
            }
            $ovItemValueOld = null;

            $valueContent = ''
                    . '<div>'
                      . $ovItemValue
                    . '</div>';

            $cell = '<td class="overview-table-cell-' . $ovItemName . '" data-item="' . $ovItemName . '">'
                        . $valueContent
                    . "</td>";
           if ($ovItemName == "title") {
                $title_cell = $cell;
                continue;
            }





            if($ovItemName == "uploaded"){
            	continue;
                $uploaded_cell = '<td class="overview-table-cell-' . $ovItemName
                    . ' overview-table-cell-' . $ovItemName . '-' . ($ovItemValue ? 'yes' : 'no')
                    . '" data-item="' . $ovItemName . '">'
                    . ($ovItemValue
                        ? '<i class="glyphicon glyphicon-thumbs-up icon-thumbs-up"></i> '
                        : '<i class="glyphicon glyphicon-thumbs-down icon-thumbs-down"></i>'
                        ) . '</td>';

				continue;
			}

            if($ovItemName == "uploaded"){
            	continue;
                $uploaded_cell = '<td class="overview-table-cell-' . $ovItemName
                    . ' overview-table-cell-' . $ovItemName . '-' . ($ovItemValue ? 'yes' : 'no')
                    . '" data-item="' . $ovItemName . '">'
                    . ($ovItemValue
                        ? '<i class="glyphicon glyphicon-thumbs-up icon-thumbs-up"></i> '
                        : '<i class="glyphicon glyphicon-thumbs-down icon-thumbs-down"></i>'
                        ) . '</td>';

				continue;
			}
			            if($ovItemName == "uploaded2"){
			            	continue;
                $uploaded2_cell = '<td class="overview-table-cell-' . $ovItemName
                    . ' overview-table-cell-' . $ovItemName . '-' . ($ovItemValue ? 'yes' : 'no')
                    . '" data-item="' . $ovItemName . '">'
                    . ($ovItemValue
                        ? '<i class="glyphicon glyphicon-thumbs-up icon-thumbs-up"></i> '
                        : '<i class="glyphicon glyphicon-thumbs-down icon-thumbs-down"></i>'
                        ) . '</td>';

				continue;
			}
            if ($ovItemName == "confirmed") {
                $confirmed_cell = '<td class="overview-table-cell-' . $ovItemName
                    . ' overview-table-cell-' . $ovItemName . '-' . ($ovItemValue ? 'yes' : 'no')
                    . '" data-item="' . $ovItemName . '">'
                    . ($ovItemValue
                        ? '<i class="glyphicon glyphicon-ok-sign icon-ok-sign"></i> '
                        : '<i class="glyphicon glyphicon-remove-sign icon-remove-sign"></i>'
                        )
                    . (isset($effectiveContractFaculties[$docParent])
                        ? '<div style="position: absolute;"><div style="position: relative; top: -20px; right: -180px;">участвует в пилотном мониторинге преподавателей</div></div>'
                        : '')
                    . '</td>'
                    . '';
                continue;
            }
            if ($ovItemName == "visited") {
                $visited_cell = '<td class="overview-table-cell-' . $ovItemName
                    . ' overview-table-cell-' . $ovItemName . '-' . ($ovItemValue ? 'yes' : 'no')
                    . '" data-item="' . $ovItemName . '">'
                    . ($ovItemValue
                        ? '<i class="glyphicon glyphicon-eye-open icon-eye-open"></i> '
                        : '<i class="glyphicon glyphicon-eye-close icon-eye-close"></i>'
                        ) . '</td>';
                continue;
            }
            if ($ovItemName == "uuid") {
				$chair = $db->colDocs->findOne(array('uuid'=>$ovItemValue)); // Требует доработки
				if(@$chair['report']){
					$pdf_cell = '<td class="report_given"> Отчет сдан</td>';
				}else{
					if(!file_exists('pdf/'.md5($ovItemValue).'.pdf')){
						$pdf_cell = '<td class="no_pdf">PDF не создан</td>';
					}else{
						$pdf_cell = '<td class="pdf">PDF создан</td>';
					}
				}

    $unitconfirm_cell = '<td style="background-color:#FFB198;"><i class="glyphicon icon-pencil"></i></td>';
    $collection = $db->colRules;
    $query = array('objects'=>$ovItemValue);
    $cursor = $collection->find($query);
    $i=0;


                continue;
            }

            $cells .= $cell;
        }

        $cells = ''
               . $title_cell
               . $visited_cell
               . $confirmed_cell
             //  . $pdf_cell
               //. $uploaded_cell
               // . $uploaded2_cell
              // .$unitconfirm_cell
               . $cells;

        $row .= $cells;

        $row .= '</tr>';

        $rowsByParent[$docParent][] = $row;
    }

     $rowsByParentSorted = array_reduce(array_keys($rowsByParent), function($full, $parentUuid) use ($rowsByParent, $effectiveContractFaculties, $mn, $prefix, $language) {
            $parentDoc = Document::findByUuid($mn, $prefix, $parentUuid);

            if ($parentDoc) {
                $parentTitle = $parentDoc->getFieldI18nValue("title", $language);

                $grandParentUuid = $parentDoc->getFieldI18nValue("parent");

                if ($grandParentUuid) {
                    $grandParentDoc = Document::findByUuid($mn, $prefix, $grandParentUuid);
                    if ($grandParentDoc) {
                        $grandParentTitle = $grandParentDoc->getFieldI18nValue("title", $language);
                        if ($grandParentTitle) {
                            $parentTitle = $grandParentTitle . ' → ' . $parentTitle;
                        }
                    }
                }

                // if (in_array($parentTitle, $effectiveContractFaculties)) {
                //     $parentTitle .= '<br><i>(участвует в пилотном мониторинге преподавателей</i>';
                // }

                $parentRows = $rowsByParent[$parentUuid];
                sort($parentRows);
//ниже необходим авто подсчет colspan
                $full[$parentTitle][] = '<tr>'
                    . '<td colspan="4">'
                    . '<b>' . $parentTitle . '</b>'

                    . (in_array($parentTitle, $effectiveContractFaculties)
                        ? '<div style="position: absolute;"><div style="position: relative; top: -20px; right: -882px;">участвует в пилотном мониторинге преподавателей</div></div>'
                        : '')
                    . '</td>'
                    . '</tr>'
                            . implode("\n", $parentRows);
            }

            return $full;
        }, array());

    echo implode("\n", array_map(function($_it) {
            return implode("", $_it);
        }, $rowsByParentSorted));

?>
  </tbody>
</table>

</div>

</body>
</html>
