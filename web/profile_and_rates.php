<?php
    define('ENTRY_POINT', 'monitoring');
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");
    session_start();

    require_once(__DIR__ . "/../vendor/autoload.php");
    require_once("lib/fconnect.php");

    $db = DBConnect::getInstance();
    use PFBC\Form;
    use PFBC\Element;
    use PFBC\View;
    use PFBC\Validation;
    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;
    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();
    $language = Configurator::getDefaultLang();
    $pageType = "chair";
    $currentPageType = DocumentType::findByType($mn, $prefix, $pageType);

?><!doctype html>

<html lang="ru">
<head>
  <meta charset="utf-8" />
  <title>Комплексный мониторинг кафедр</title>
  <link type="text/css" rel="stylesheet" href="/css/bootstrap-combined.min.css">
  <script src="/js/jquery-1.9.1.js"></script>

  <link rel="stylesheet" href="/css/overview.table.css" />
  <link rel="stylesheet" href="/css/monitoring.css" />
  <script src="/js/overview.table.js"></script>
  <style>
    td {
        white-space: nowrap;
    }
  </style>

</head>

<body>

<div class="overview">

<div id="overview-table-top-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--top" id="overview-table-top"></table>
</div>

<div id="overview-table-left-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--left" id="overview-table-left"></table>
</div>

<div id="overview-table-top-left-wrapper">
  <table class="overview__table overview__table--horizontal overview__table--top-left" id="overview-table-top-left"></table>
</div>
<table class="overview__table overview__table--horizontal" id="overview-table">
  <thead>
    <tr>
<?php

    $fields = array_filter($currentPageType->items, function($item) {

          return in_array($item["name"], array("title", "uuid", "parent", "chairtype", "rates"));
        });
    $cells = "";

    $title_cell = "";
    $parent_cell = "";

    foreach ($fields as $ovItem) {
        $labelVar = "label_" . $language;
        $ovItemLabel = $ovItem[$labelVar];
        $ovItemName = $ovItem["name"];

        $cell = '<td class="overview-table-cell-' . $ovItemName . '" data-item="' . $ovItemName . '">' . $ovItemLabel . "</td>";

        if ($ovItemName == "title") {
            $title_cell = $cell;
            continue;
        }
        if ($ovItemName == "uuid") {
            continue;
        }
        if ($ovItemName == "parent") {
            $parent_cell .= $cell;
            continue;
        }
        if ($ovItemName == "chairtype") {
            $cells .= $cell;
            continue;
        }
        if ($ovItemName == "rates") {
            $cells .= $cell;
            continue;
        }

        $cells .= $cell;
    }


  echo  $cells = ''
            . $parent_cell
            . $title_cell
            . $cells;
?>
    </tr>
  </thead>
  <tbody>
<?php
    $ovDocuments = Document::find($mn, $prefix, $currentPageType->type);
    $rowsByParent = array();

    foreach ($ovDocuments as $ovDocument) {



        $docParent = $ovDocument->getFieldI18nValue("parent");

        $row = "";
        $row .= '<tr>';
        $cells = "";

        $title_cell = "";
        $parent_cell = "";

        foreach ($fields as $ovItem) {
            $labelVar = "label_" . $language;
            $ovItemLabel = $ovItem[$labelVar];
            $ovItemName = $ovItem["name"];
            $ovItemI18nName = (isset($ovItem["no_i18n"]) && $ovItem["no_i18n"]) ? ($ovItemName) : ($ovItemName . "_" . $language);
            $ovItemValue = isset($ovDocument->fields[$ovItemI18nName]) ? $ovDocument->fields[$ovItemI18nName] : "";

            if (!is_scalar($ovItemValue)) {
                $ovItemValue = "...";
            }
            $ovItemValueOld = null;

            $valueContent = ''
                    . '<div>'
                      . $ovItemValue
                    . '</div>';

            $cell = '<td class="overview-table-cell-' . $ovItemName . '" data-item="' . $ovItemName . '">'
                        . $valueContent
                    . "</td>";
            if ($ovItemName == "title") {
                $title_cell = $cell;
                continue;
            }
            if ($ovItemName == "uuid") {
                continue;
            }
            if ($ovItemName == "parent") {
                $parentDoc = Document::findByUuid($mn, $prefix, $docParent);
                $parent_cell = '<td class="overview-table-cell-' . 'parent' . '" data-item="' . 'parent' . '">'
                        . $parentDoc->getFieldI18nValue("title", $language)
                    . "</td>";
                continue;
            }
            if ($ovItemName == "chairtype") {
                $cells .= $cell;
                continue;
            }
            if ($ovItemName == "rates") {
                $cells .= $cell;
                continue;
            }

            $cells .= $cell;
        }

        $cells = ''
                . $parent_cell
                . $title_cell
                . $cells;

        $row .= $cells;

        $row .= '</tr>';

        $rowsByParent[$docParent][] = $row;
    }

     $rowsByParentSorted = array_reduce(array_keys($rowsByParent), function($full, $parentUuid) use ($rowsByParent, $mn, $prefix, $language) {
            $parentDoc = Document::findByUuid($mn, $prefix, $parentUuid);

            if ($parentDoc) {
                $parentTitle = $parentDoc->getFieldI18nValue("title", $language);

                $parentRows = $rowsByParent[$parentUuid];
                sort($parentRows);

                $full[$parentTitle][] = implode("\n", $parentRows);
            }

            return $full;
        }, array());

    echo implode("\n", array_map(function($_it) {
            return implode("", $_it);
        }, $rowsByParentSorted));

?>
  </tbody>
</table>

</div>

</body>
</html>
