<?php
    define('ENTRY_POINT', 'view');

    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");

    session_start();

	require_once(__DIR__ . "/../vendor/autoload.php");

#    require_once("PFBC/Form.php");
#    require_once("pagecontrol/Load.php");
#    require_once("documongo/Load.php");

#    require_once('User.php');

    use pagecontrol\PageController;

    $user = User::getFromSession($_SESSION);


    $formCtrl = new PageController\Herzen\Tables();
    $formCtrl->attachUser($user);
    $formCtrl->render();



