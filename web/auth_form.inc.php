<?php
    if (!defined('ENTRY_POINT')) die('Access denied');

    use PFBC\Form;
    use PFBC\Element;
    use PFBC\View;
    use PFBC\Validation;


    if (isset($user)):
        if (!$user->isLogged()): ?>


<?php
    $formName = "auth";
    $form = new Form($formName);
    $form->configure(array(
        "prevent" => array("jQuery","jQueryUI","bootstrap")
        , "method" => "post"
        , "ajax" => 1
        , "ajaxCallback" => "parseAuthResponse"
        , "novalidate" => ""
        , "action" => "auth.php"
        , "class" => "authbox"
        ));

    $form->addElement(new Element\Hidden("act", "login"));


    $formLabel = "Заполнение форм";

    $form->addElement(new Element\Hidden("form", $formName));
    $form->addElement(new Element\HTML('<legend>Представьтесь</legend>'));

    $form->addElement(new Element\HTML('<div class="authbox__username">'));
    $form->addElement(new Element\Textbox("Имя пользователя", "username", array("required" => true)));
    $form->addElement(new Element\HTML('</div>'));

    $form->addElement(new Element\HTML('<div class="authbox__password">', array("required" => true)));
    $form->addElement(new Element\Password("Пароль", "password"));
    $form->addElement(new Element\HTML('</div>'));

    $form->addElement(new Element\HTML('<legend class="authbox__output" id="authbox__output"></legend>'));

    $form->addElement(new Element\HTML('<div class="authbox__submit">'));
    $form->addElement(new Element\Button('Войти', "submit"));
    $form->addElement(new Element\HTML('</div>'));

    $form->render();
?>

<?php die; else: /*user->isLogged*/ ?>

<div class="userbox userbox_parent_header">
<?
    $formName = "userbox";
    $form = new Form($formName);
    $form->configure(array(
        "prevent" => array("jQuery","jQueryUI","bootstrap")
        , "method" => "post"
        , "ajax" => 1
        , "ajaxCallback" => "parseAuthResponse"
        , "novalidate" => "1"
        , "action" => "auth.php"
        , "class" => "userbox__logout-wrapper"
        , "view" => new View\Inline()
    ));

    $form->addElement(new Element\Hidden("form", $formName));
    $form->addElement(new Element\Hidden("act", "logout"));
    $form->addElement(new Element\HTML('<div class="userbox__logout">'));
    $form->addElement(new Element\Button('Выйти', "submit"));
    $form->addElement(new Element\HTML('</div>'));
    $form->render();
?>

    <div class="userbox__photo userbox__photo_source_loading">&nbsp;</div>
    <div class="userbox__username">
        <!--Пользователь -->
        <span class="userbox__username__displayname"></span>
        <span class="userbox__username__login" title="<?=$user->uuid?>">
            <?=$user->login?>
        </span>
    </div>
    <div class="userbox__helper"></div>
</div>

<?php   endif; /*user->isLogged*/ ?>

<?php endif; /*user->isLogged*/ ?>
