<?php
    define('ENTRY_POINT', 'security');
    require(__DIR__ . '/../common.inc.php');

$runs = [
    "bash /web/forms/admin/cli/cli-git.sh",
    "bash /web/forms/admin/cli/cli-download-sheet.sh",
    "php /web/forms/admin/cli/cli-binder-without-ldap.php /web/forms/admin/cli/people.csv",
];

foreach ($runs as $run) {
    $run .= " 2>&1";
    $return_var = null;
    $output = [];
    $last_line = exec($run, $output, $return_var);
    ?>
    <pre>
        <?php
            echo $run;
            echo "<br/>";
            echo "<br/>";
            echo $return_var;
            echo "<br/>";
            echo "<br/>";
            echo $last_line;
            echo "<br/>";
            echo "<br/>";
            echo implode("<br/>", $output);
            echo "<br/>";
        ?>
    </pre>
    <hr/>
    <?php
}

