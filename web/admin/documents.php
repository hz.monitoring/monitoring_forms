<?php
    define('ENTRY_POINT', 'security');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();

    $security = $mn->selectDB($prefix . "security");

    $skip = isset($_GET["skip"]) ? $_GET["skip"] : 0;
    $limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;
    $type = isset($_GET["type"]) ? $_GET["type"] : "chair";
    $extra = isset($_GET["extra"]) ? $_GET["type"] : false;

    $rules = $security->rules->find();//->skip($skip)->limit($limit);

    $documents = Document::find($mn, $prefix, $type);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

.security {

}

.security td {
    border: 1px gray solid;
}
</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
<table class="security">
            <thead>
                <tr>
                    <td>#</td>
                    <td>type</td>
                    <td>id</td>
                    <td>uuid</td>
                    <td>title_ru</td>
                    <td>parent</td>
                    <td>extra</td>
                </tr>
            </thead>
<tbody>

<?php
    class AllUsers {
        public function __get($var) {
            return "Все пользователи";
        }
    }

    $users = array(
         "*" => new AllUsers(),
    );
    $departments = array();

    $i=0;
    $max = 0;

    $documents_rules = [];
    // foreach ($departments as $de) {
    //     foreach ($rule["objects"] as $rule_object_uuid) {
    //         $documents_rules[$rule_object_uuid][] = $rule;
    //     }
    // }

    foreach ($documents as $document) {
        $document_rules = isset($documents_rules[$document->uuid]) ? $documents_rules[$document->uuid] : [];

        flush();
        ob_flush();

        $i++;
        ?>

        <tr>
            <td><?=$i?></td>
            <td><?=$document->type?></td>
            <td><?=$document->id?></td>
            <td><?=$document->uuid?></td>
            <td><?=$document->fields['title_ru']?></td>
            <td><?=$document->fields['parent']?></td>

            <td><?=($extra
                ? (isset($document->fields['personal_info']) ? $document->fields['personal_info'] : '')
                : '')?></td>
        </tr>

        <?php
        /*
        if (0)
        foreach ($document_rules as $document_rule) {

            ?>
            <table class="security">
            <thead>
                <tr>
                    <td>id</td>
                    <td>title_ru</td>
                    <td>uuid</td>
                    <td>type</td>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td><?=$i . '<br/>' . $rule["_id"]?></td>
                    <td><?=implode("", array_map(function($it) use ($mn, $prefix, &$users) {
                            $it_uuid = $it;

                            if (!isset($users[$it_uuid])) {
                                $users[$it_uuid] = User::findByUuid($it_uuid);
                            }
                            $user = $users[$it_uuid];

                            return '<div class="element actor actor-' . $it_uuid . '">'
                                . $it_uuid
                                . '<br/><code>' . $user->login . '</code>'
                                . '<br/>' . $user->lastname . ' ' . $user->givenname
                                . ' (<a href="https://manage.herzen.spb.ru/user/searchfio.php?fio=' . $user->lastname . ' ' . $user->givenname . '">посмотреть на Менедже</a>)'
                                . '</div>';
                        }, $rule["actors"])); ?></td>
                    <td><?=implode("", array_map(function($it) use ($mn, $prefix) {
                            return '<div class="element action action-' . $it . '">' . $it . '</div>';
                        }, $rule["actions"])); ?></td>
                    <td><?=implode("", array_map(function($it) use ($mn, $prefix, &$departments) {
                            $slash_pos = strpos($it, "/");
                            $it_uuid = $it;
                            if ($slash_pos != false) {
                                $it_uuid = substr($it, $slash_pos);
                            }

                            if (!isset($departments[$it_uuid])) {
                                $departments[$it_uuid] = Department::findByUuid($it_uuid);
                            }
                            $department = $departments[$it_uuid];

                            $document = Document::findByUuid($mn, $prefix, $it_uuid);

                            return '<div class="element object object-' . $it . '">'
                                . $it
                                . '<br/>' . $department->description
                                . '<br/>' . (is_object($document) ? "#{$document->id}, {$document->fields['title_ru']}" : '<strong>No document!</strong>')
                                . '</div>';
                        }, $rule["objects"])); ?></td>
                </tr>

            <?php
                    if ($i == $max) {
                        break;
                    }
                }
            ?>

            </tbody>
            </table>
        <?php
        */

    }
?>
</tbody>
</table>
</body>
</html>
