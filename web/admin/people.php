<?php
    define('ENTRY_POINT', 'security');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

require_once('../../bus/Colors.php');
function getColoredString($text) {
    return $text;
}

$ldap_host = "ldap.service.herzen";
$ldap_rootdn = "dc=herzen,dc=spb,dc=ru";

$ldap_handle = ldap_connect($ldap_host);
ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);

if (!$ldap_handle) {
    throw new \Exception("Ldap connection problem");
}

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();

    $security = $mn->selectDB($prefix . "security");

    $skip = isset($_GET["skip"]) ? $_GET["skip"] : 0;
    $limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;

    $rules = $security->rules->find();//->skip($skip)->limit($limit);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

.security {

}

.security td {
    border: 1px gray solid;
}
</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
<pre>

<?php

if (($handle = fopen(__DIR__ . "/../../admin/cli/people.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        if (strpos($data[0], '№') === 0) {
            continue;
        }
        $num = count($data);
        $row++;
        $rowNum = isset($data[0]) ? $data[0] : null;
        $dep = isset($data[1]) ? trim($data[1]) : null;
        $fio = isset($data[2]) ? trim($data[2]) : null;
        $user_uuid = isset($data[3]) ? trim($data[3]) : null;
        $role = isset($data[4]) ? trim($data[4]) : null;
        echo "Read line $dep $fio: ";
        echo "\n\t";
        if ($dep && $fio && $role) {
            echo "[bind] \t $role";
            if (!isUuid($user_uuid)) {
                $fio_explode = explode(" ", $fio);
                $sn = array_shift($fio_explode);
                $givenname = implode(" ", $fio_explode);
                $user_query = "(&(sn=$sn)(givenname=$givenname))";
                $users_search = ldap_search($ldap_handle, $ldap_rootdn, $user_query);
                $users = ldap_get_entries($ldap_handle, $users_search);
                if (is_array($users) && isset($users["count"]) && $users["count"] == 1 && isset($users[0]) && isset($users[0]["cn"]) && isset($users[0]["cn"][0])) {
                    $user_uuid = $users[0]["cn"][0];
                } else {
                    $user_err_string = getColoredString("$user_query", "light_cyan");
                    echo "\n\t- $err_string user search: $user_err_string found " . getColoredString((int)$users["count"], "light_red") . " persons; \n";
                    foreach ($users as $extra_user) {
                        if (!empty($extra_user["cn"])) {
                            echo "\n\t\t- " . $extra_user["cn"][0] . " - " . $extra_user["sn"][0] . ' ' . $extra_user["givenname"][0] . "\n";
                        }
                    }

                    echo "Input user uuid: ";
                    $user_uuid = readline();
                }
            } else {
                echo getColoredString(" [user:byuuid:$user_uuid]", "brown");
            }
            $dep_uuid = null;
            $dep_parent_uuid = null;
            $retry = true;
            while (is_null($dep_uuid) && $retry) {
                $dep_object = \Department::findByPath($dep);
                if ($dep_object && $dep_object->uuid) {
                    $dep_uuid = $dep_object->uuid;
                    $dep_parent = $dep_object->getParent();
                    if ($dep_parent && $dep_parent->uuid) {
                        $dep_parent_uuid = $dep_parent->uuid;
                        echo getColoredString("\n\tFound dep parent for $dep: {$dep_parent->uuid}, {$dep_parent->description}", "blue");
                        $dep_parent_doc = Document::findByUuid($mn, $prefix, $dep_parent->uuid);
                        if (is_null($dep_parent_doc)) {
                            echo "\n\t" . getColoredString("[dep parent created as $dep_parent->description]", "yellow");
                        } else {
                            echo "\n\t" . getColoredString("[dep found]", "light_green");
                        }
                    } else {
                        echo "\n\t" . getColoredString("[dep parent not found, looking for a chairs]", "light_red");
                    }
                } else {
                    $dep_err_string = getColoredString("$dep", "light_blue");
                    echo "\n\t- $err_string department $dep_err_string not found ; \n";
                    echo "What to do next? [Enter]skip, [r]etry: ";
                    $action = readline();
                    if ($action != "r") {
                        $retry = false;
                    }
                }
            }

            echo "\n\t";
            if(isUuid($user_uuid)){
                if (isUuid($dep_uuid) || $dep ==='chair') {
                    if(isUuid($dep_uuid)){
                        $dep_object = \Department::findByUuid($dep_uuid);
                        echo getColoredString("[UUID - OK]", "green") . " user:$user_uuid, dep:$dep_uuid: ";
                        $object = Document::findByUuid($mn, $prefix, $dep_uuid);

                        if (is_null($object)) {
                            echo getColoredString("[dep created as $dep_object->description at '{$object->id}']", "yellow");
                        } else {
                            echo getColoredString("[dep found]", "light_green");

                        }

                        if ($object && !$object->getFieldI18nValue("parent")) {
                            echo getColoredString("[dep parent info saved]", "yellow");
                        }
                    }
                    if($dep ==='chair'){
                        $dep_uuid = 'chair';
                    }
                    switch($role){
                        case 'admin':
                            $action = array("+browse", "+create", "+read", "+display", "+update", "+delete", "+form", "+overview", "+approve");
                        break;
                        case 'prorector':
                            $action = array("+browse", "+read");
                        break;
                        case 'rector':
                            $action = array("+browse", "+read");
                        break;
                        case 'amenable':
                            $action = array("+browse", "+read", "+display", "+update");
                        break;
                        case 'dean':
                            $action = array();
                        break;
                        case 'moderator':
                            $action = array();
                        break;
                        case 'head':
                            $action = array();
                        break;
                        case 'employee':
                            $action = array();
                        break;
                        case 'student':
                            $action = array();
                        break;
                        case 'herzen':
                            $action = array();
                        break;
                    }
                        $rule_a = array (
                              '_id' => new \MongoId(),
                              'actions' => $action,
                              'actors'  => array ( $user_uuid ),
                              'objects' => array ( $dep_uuid ),
                              'roles' => array($role)
                            );

                        echo getColoredString("[rules added]", "yellow");
                } else {
                    echo getColoredString("[uuids fails: $user_uuid or $dep_uuid]", "light_red");
                }
            }

        } else {
            echo getColoredString("[empty value - skip]", "light_purple") . "\n";
        }

        echo "\n\n";
    }
    fclose($handle);
}


?>
</pre>
</body>
</html>
