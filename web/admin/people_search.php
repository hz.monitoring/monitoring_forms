<?php
    define('ENTRY_POINT', 'admin-standalone');
    require(__DIR__ . '/../common.inc.php');

    use pagecontrol\PageController;

    $user = User::getFromSession($_SESSION);

    if (!$user->isLogged()) {
        $formCtrl = new PageController\Herzen\Admin();
        $formCtrl->attachUser($user);
        $formCtrl->render();
        exit;
    }
    $admins = [
        'tikhontagunov'
    ];
    if (!in_array($user->login, $admins)) {
        echo 'Доступ не разрешен. <a href="/">Главная</a>';
        exit;
    }





    $ldap_host = "ldap.service.herzen";
    $ldap_rootdn = "dc=herzen,dc=spb,dc=ru";

    $ldap_handle = ldap_connect($ldap_host);
    ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);

    $ldap_default_namespace = 'ou=Users,dc=herzen,dc=spb,dc=ru';

    if (!$ldap_handle) {
        throw new \Exception("Ldap connection problem");
    }

    function isUuid($string) {
        return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
    }

    function escape($string) {
        return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
    }

    $interesting_queries = [
        "Зарегистрированы, но остались заготовками" => "(&(associateddomain=students.stub)(uid=*))",
        "Зарегистрированы, но не могут войти (нет posixAccount)" => "(&(associateddomain=students.stub)(uid=*)(!(objectClass=posixAccount)))",
    ];


    $user_query = null;
    $query = null;
    if (isset($_GET["query"])) {
        $query = $_GET["query"];

        if (strpos($query, "=") === false) {
            if (strpos($query, "@")) {
                $user_query = "mail=$query";
            } else {
                $fio = $query;
                $fio_parts = explode(" ", $fio);
                $sn = array_shift($fio_parts);
                $givenname = implode(" ", $fio_parts);
                $user_query = "(&(sn=$sn)(givenname=$givenname))";
            }
        } else {
            $user_query = $query;
        }
    } else {
    }

    // var_dump($user_query);


    $users_count = 0;
    if ($user_query) {
        $users_search = ldap_search($ldap_handle, $ldap_rootdn, $user_query);
        $users = ldap_get_entries($ldap_handle, $users_search);
        ldap_close($ldap_handle);

        $users_count = is_array($users) && isset($users["count"]) ? $users["count"] : 0;
    }

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="/css/jquery-ui.css" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/bootstrap-4.1-examples/dashboard/dashboard.css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(function(){

        })
    </script>


    <title>
        <?php if ($user_query): ?>
            <?=$users_count?> найдено по запросу <?=$query?>
        <?php else: ?>
            Введите поисковый запрос
        <?php endif?>
    </title>
  </head>
  <body class="text-monospace">


<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-1 col-md-1 mr-0" href="?">Главная</a>
      <span class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">ФИО, эл. почта или запрос</span>
      <form class="form-inline w-100">
        <input class="form-control form-control-dark w-100" type="text" id="query" name="query" placeholder="Иванов Иван Иванович" aria-label="Search" value="<?=$query ? $query : 'Тагунов Тихон Валерьевич'?>">
        <!-- <button type="submit" class="btn btn-primary">&raquo;</button> -->
      </form>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Выход</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">


            <ul class="nav flex-column list-group">

              <?=(!in_array($user_query, $interesting_queries)
                  ? (''
                      . '<li class="list-group-item list-group-flush">'
                        . '<a class="nav-link active" href="?query=' . urlencode($user_query) . '">'
                          // . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>'
                            . 'Текущий запрос'
                            . '<div>'
                              . '<code class="badge badge-primary text-left">'
                                . mb_ereg_replace("([^!])(\()", "\\1<br>\\2", $user_query)
                              . '</code>'
                            . '</div>'
                            . ' <span class="sr-only">(current)</span>'
                        . '</a>'
                        . '<a class="nav-link d-flex text-muted" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                            Сохранить запрос
                          </a>'
                      . '</li>'
                      . '')
                    : '')?>
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Интересные запросы</span>
              <a class="d-flex align-items-center text-muted" href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
              </a>
            </h6>

            <ul class="nav flex-column list-group">
              <?=implode("\n", array_map(function($int_query, $int_query_description) use ($user_query){
                  return ''
                    . '<li class="list-group-item">'
                      . '<a class="nav-link ' . ($int_query==$user_query ? 'active' : '') . '" href="?query=' . urlencode($int_query) . '">'
                        // . '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>'
                          . $int_query_description
                          . '<div>'
                            . '<code class="badge badge-primary text-left">'
                              . mb_ereg_replace("([^!])(\()", "\\1<br>\\2", $int_query)
                            . '</code>'
                          . '</div>'
                          . ' <span class="sr-only">(current)</span>'
                      . '</a>'
                    . '</li>'

                    // . '<a class="list-group-item" >'
                    // .
                    // . ' '
                    // .
                    // . '</a>'
                    . '';
                }, $interesting_queries, array_keys($interesting_queries)))?>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
              </button>
            </div>
          </div>

          <?php

            echo '<div class="px-3">';
            echo '<span>' . 'Запрос к лдапу' . '</span>';
            echo '<p class="monospace">' . $user_query . '</p></p>';
            echo '</div>';

        if (!$users_count) {
            echo '<div class="px-3">';
            echo '<h3>' . 'Ничего не найдено' . '</h3>';
            echo '</div>';

        } else {
            $max = 100;
            $start = 1;

            echo '<div class="px-3">';
            echo '<h3>'
                . (int)$users["count"] . ' найдено по запросу, '
                . 'показаны ' . $start . '..' . $max
                . '</h3>';
            echo '</div>';


            $users_table = [];
            $users_table_header_sort = ["uid", "description", "associateddomain", "objectclass", "userpassword"];
            $users_table_header = array_combine($users_table_header_sort, $users_table_header_sort);
            $users_list = [];

            foreach ($users as $user_i => $extra_user) {
                $user_i_num = $user_i+1;
                if (!isset($extra_user["dn"])) {
                    continue;
                }
                if ($user_i_num > $max) {
                    break;
                }
                if ($user_i_num < $start) {
                    continue;
                }


                $users_list[] = '<li class="list-group-item">';

                $users_list[] = ' <details>
                        <summary>';
                $users_list[] = '<h3 class="">'
                        . '<span class="badge badge-primary">' . $user_i_num . '</span>'
                        . ' '
                        . $extra_user["sn"][0]
                        . ' ' . $extra_user["givenname"][0]
                        . ' '
                        . '<small class="text-muted">'
                            . substr($extra_user["dn"], 0, stripos($extra_user["dn"], ","))
                            . '<small class="text-muted">'
                                . (substr($extra_user["dn"], stripos($extra_user["dn"], ",")) == "," . $ldap_default_namespace
                                    ? ''
                                    : substr($extra_user["dn"], stripos($extra_user["dn"], ",")))
                            . '</small>'
                        . '</small>'
                    . '</h3>'

                    . '<p class="p-2">'
                        . '<span class="d-inline p-2 bg-transparent">'
                            . (isset($extra_user["uid"][0]) ? $extra_user["uid"][0] : '(empty)') . '</span>'
                        . ' '
                        . '<span class="d-inline p-2 bg-primary text-white">'
                            . (isset($extra_user["associateddomain"][0]) ? $extra_user["associateddomain"][0] : '(empty)') . '</span>'
                        . ' '
                        . (isset($extra_user["objectclass"]) && is_array($extra_user["objectclass"]) && in_array("posixAccount", $extra_user["objectclass"])
                            ? '<span class="d-inline p-2 bg-success text-white">' . 'Есть posixAccount' . '</span>'
                            : '<span class="d-inline p-2 bg-warning text-black">' . 'Нет posixAccount' . '</span>'
                            )
                    . '</p>'
                    // . " - " . $extra_user["cn"][0]
                    // . " - " .
                    . "";

                // $users_list[] = '      Подробнее';
                $users_list[] = '  </summary>';

                $users_table_row = [];


                $users_list[] = '<div class="text-monospace">';
                $users_list[] = "User dn: <code>" . $extra_user["dn"] . "</code><br><br>";
                for ($i=0; $i < $extra_user["count"]; $i++) {
                    $extra_user_field = $extra_user[$i];
                    $extra_user_field_value = $extra_user[$extra_user_field];

                    $extra_user_field_array = is_array($extra_user_field_value) ? $extra_user_field_value : [$extra_user_field_value];

                    $users_table_header[$extra_user_field] = $extra_user_field;

                    $value_processor = function($field_value) {
                        if (!is_array($field_value)) {
                          return $field_value;
                        } else {
                          if (isset($field_value["count"])) {
                            unset($field_value["count"]);
                            sort($field_value);
                            return implode("<br>", array_values($field_value));
                          }
                        }
                      };
                    $users_table_row[$extra_user_field] = $value_processor($extra_user_field_value);

                    $users_list[] = "<b>" . $extra_user_field . "</b>"
                        . "<br>"
                        . "";

                    $users_list[] = var_export($extra_user_field_value, true);

                    $users_list[] = "<br>";
                }

                $users_table[] = $users_table_row;

                $users_list[] = '<pre>';
                $users_list[] = var_export($extra_user, true);
                $users_list[] = '</pre>';
                $users_list[] = '</div>';

                $users_list[] = ' </details>';
                $users_list[] = '</li>';
            }

            echo '<table class="table">';
            echo '<thead>'
              . '<tr>'
              . '<td>#</td>'
              . implode("\n", array_map(function($cell) { return '<td>' . $cell . '</td>'; }, $users_table_header))
              . '</tr>'
              . '</thead>'
              . '<tbody>'
              . implode("\n", array_map(function($row, $row_i) use ($users_table_header) {
                  return '<tr>'
                    . '<td>' . ($row_i+1) . '</td>'

                    . implode("\n", array_map(function($cellKey) use ($row) {
                        return '<td>' . (isset($row[$cellKey]) ? $row[$cellKey] : '') . '</td>';
                      }, $users_table_header))
                    . '</tr>';
                }, $users_table, array_keys($users_table)))
              . '</tbody>';
            echo '</table>';

            echo '<div class="px-3">';
            echo '<h3>'
                . (int)$users["count"] . ' найдено по запросу, '
                . 'показаны ' . $start . '..' . $max
                . '</h3>';
            echo '</div>';

            echo '<ul class="list-group">';
            echo implode("\n", $users_list);
            echo '</ul>';

        }
?>

          <!-- <canvas class="my-4 w-100 chartjs-render-monitor" id="myChart" width="1539" height="649" style="display: block; width: 1539px; height: 649px;"></canvas> -->
<!--
          <h2>Section title</h2>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Header</th>
                  <th>Header</th>
                  <th>Header</th>
                  <th>Header</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td>sit</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                  <td>at</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                  <td>Duis</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                  <td>sed</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                  <td>Mauris</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                  <td>arcu</td>
                </tr>
                <tr>
                  <td>1,011</td>
                  <td>eget</td>
                  <td>nulla</td>
                  <td>Class</td>
                  <td>aptent</td>
                </tr>
                <tr>
                  <td>1,012</td>
                  <td>taciti</td>
                  <td>sociosqu</td>
                  <td>ad</td>
                  <td>litora</td>
                </tr>
                <tr>
                  <td>1,013</td>
                  <td>torquent</td>
                  <td>per</td>
                  <td>conubia</td>
                  <td>nostra</td>
                </tr>
                <tr>
                  <td>1,014</td>
                  <td>per</td>
                  <td>inceptos</td>
                  <td>himenaeos</td>
                  <td>Curabitur</td>
                </tr>
                <tr>
                  <td>1,015</td>
                  <td>sodales</td>
                  <td>ligula</td>
                  <td>in</td>
                  <td>libero</td>
                </tr>
              </tbody>
            </table>
          </div>
             -->
        </main>
      </div>
    </div>


<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-sm">
      <div>
      </div>
    </div>
  </div>
</div>

    <?php
        // echo '';
        // echo '<span>' . 'Поисковый запрос' . '</span>';
        // echo '<h1>' . '<input class="form-control-lg" name="query" type="text" placeholder="Иванов Иван Иванович">' . '</h1>';
        // echo '</div>';
        // echo '</form>';




        // echo '<div class="px-3">';
        // echo '<span>' . 'Поисковый запрос' . '</span>';
        // echo '<h1>' . $query . '</h1>';
        // echo '</div>';




?>
</pre>
</body>
</html>
