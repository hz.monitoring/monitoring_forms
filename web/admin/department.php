<?php
    define('ENTRY_POINT', 'security');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();

    $security = $mn->selectDB($prefix . "security");

    $skip = isset($_GET["skip"]) ? $_GET["skip"] : 0;
    $limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;

    $dep_path = isset($_GET["path"]) ? $_GET["path"] : null;
    $department = Department::findByPath($dep_path);
    // $rules = $security->rules->find();//->skip($skip)->limit($limit);

    $document = Document::findBy($mn, $prefix, array("title_ru" => $dep_path));
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

.security {

}

.security td {
    border: 1px gray solid;
}
</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
    <table class="security">
        <thead>
            <tr>
                <td>Ldap Название</td>
                <td>Ldap UUID</td>
                <td>Mongo UUID</td>
                <td>Mongo Название</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?=$department->description?></td>
                <td><?=$department->uuid?></td>
                <td><?=$document->uuid?></td>
                <td><?=$document->fields['title_ru']?></td>
            </tr>
        </tbody>
    </table>
</body>
</html>
