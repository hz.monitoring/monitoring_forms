<?php
    define('ENTRY_POINT', 'model');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();

    $model = $mn->selectDB($prefix . "model");

    $itemType = isset($_GET["type"]) ? (string)$_GET["type"] : "chair";

    $skip = isset($_GET["skip"]) ? $_GET["skip"] : 0;
    $limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;

    $chairType = $model->types->findOne(["type" => $itemType]);//->skip($skip)->limit($limit);

    if (is_null($chairType)) {
        exit;
    }

    $items = array_reduce(array_keys($chairType["items"]), function($items, $item_key) use ($chairType) {
        $item_desription = $chairType["items"][$item_key];
        foreach ($item_desription as $item_desription_key => $item_desription_value) {
            $items[$item_desription_key][$item_key] = $item_desription_value;
        }
        return $items;
    }, []);
    // var_dump($items);
    // die;
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

.model {

}

.model td {
    border: 1px gray solid;
    white-space: nowrap;
}
</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> types</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> types</a>
</div> -->
<table class="model">
<!-- <thead>
    <tr>
        <td>name</td>
        <td>type</td>
        <td>index</td>
        <td>label_ru</td>
        <td>placeholder_ru</td>
        <td>index_code_ru</td>
        <td>required</td>
        <td>no_i18n</td>
        <td>no_versioning</td>
        <td>multiple</td>
        <td>short_description_ru</td>
        <td>long_descri</td>
        <td>Object name</td>
        <td>Object type</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Роль</td>
    </tr>
</thead> -->
<tbody>

<?php


function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}
    class AllUsers {
        public function __get($var) {
            return "Все пользователи";
        }
    }

    $users = array(
         "*" => new AllUsers(),
    );
    $departments = array();

    $i=0;
    $max = 0;

    // var_dump($chairType);die;


?>
    <thead>
        <tr>
            <?php foreach (array_keys($items) as $item_key): ?>
                <td><?=$item_key?></td>
            <?php endforeach; ?>
        </tr>
    </thead>

<?php

    foreach($chairType["items"] as $item_num => $item_desription) {
        // var_dump($item_desription, $item_num);
        // flush();
        // ob_flush();

        $i++;
?>
    <tr>
<?php
        foreach (array_keys($items) as $item_key) {
            ?>

            <td><?=isset($item_desription[$item_key]) ? $item_desription[$item_key] : ""?></td>
            <?php
        }
?>

    </tr>

<?php
        if ($i == $max) {
            break;
        }
    }
?>

</tbody>
</table>
</body>
</html>
