<?php
    define('ENTRY_POINT', 'security');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

require_once('../../bus/Colors.php');
function getColoredString($text) {
    return $text;
}

$ldap_host = "ldap.service.herzen";
$ldap_rootdn = "dc=herzen,dc=spb,dc=ru";

$ldap_handle = ldap_connect($ldap_host);
ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);

if (!$ldap_handle) {
    throw new \Exception("Ldap connection problem");
}

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

    $cn = isset($_GET["cn"]) ? $_GET["cn"] : 0;

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
<pre>

<?php
                $err_string = "";
                $user_query = "(cn=$cn)";
                $users_search = ldap_search($ldap_handle, $ldap_rootdn, $user_query);
                $users = ldap_get_entries($ldap_handle, $users_search);
                print_r($users);
                // var_dump($users);
                if (is_array($users) && isset($users["count"]) && $users["count"] == 1 && isset($users[0]) && isset($users[0]["cn"]) && isset($users[0]["cn"][0])) {
                    $user_uuid = $users[0]["cn"][0];
                } else {
                    $user_err_string = getColoredString("$user_query", "light_cyan");
                    echo "\n\t- $err_string user search: $user_err_string found " . getColoredString((int)$users["count"], "light_red") . " persons; \n";
                    foreach ($users as $extra_user) {
                            echo "\n\t\t- " . $extra_user["cn"][0] . " - " . $extra_user["sn"][0] . ' ' . $extra_user["givenname"][0] . "\n";

                            echo "User dn: <code>" . $extra_user["dn"] . "</code><br><br>";
                            for ($i=0; $i < $extra_user["count"]; $i++) {
                                $extra_user_field = $extra_user[$i];
                                $extra_user_field_value = $extra_user[$extra_user_field];

                                echo "<b>" . $extra_user_field . "</b>"
                                    . "<br>"
                                    . "";

                                print_r($extra_user_field_value);

                                echo "<br>";
                            }

                            print_r($extra_user);
                    }

                    // echo "Input user uuid: ";
                    // $user_uuid = readline();
                }


    ldap_close($ldap_handle);


?>
</pre>
</body>
</html>
