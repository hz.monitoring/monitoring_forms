<?php
    define('ENTRY_POINT', 'security');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();

    $security = $mn->selectDB($prefix . "security");

    $skip = isset($_GET["skip"]) ? $_GET["skip"] : 0;
    $limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;

    $rules = $security->rules->find();//->skip($skip)->limit($limit);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

.security {

}

.security td {
    border: 1px gray solid;
    white-space: nowrap;
}
</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
<table class="security">
<thead>
    <tr>
        <td>Rule #</td>
        <td>Rule ID</td>
        <td>Users count</td>
        <td>Users</td>
        <td>Objects count</td>
        <td>Objects</td>
        <td>Actions</td>
        <td>User UUID</td>
        <td>User login</td>
        <td>User name</td>
        <td>User link</td>
        <td>Object[/xpath]</td>
        <td>Object name</td>
        <td>Object type</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Роль</td>
    </tr>
</thead>
<tbody>

<?php


function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}
    class AllUsers {
        public function __get($var) {
            return "Все пользователи";
        }
    }

    $users = array(
         "*" => new AllUsers(),
    );
    $departments = array();

    $i=0;
    $max = 0;
    foreach ($rules as $rule) {
        flush();
        ob_flush();

        $i++;
?>
    <tr>
        <td><?=$i?></td>
        <td><?=$rule["_id"]?></td>
        <td><?=count($rule["actors"])?></td>
        <td><?=(is_array($rule["actors"])
                ? implode(",", array_map(function($action) use ($mn, $prefix) {
                        return '' . $action . '';
                    }, $rule["actors"]))
                : $rule["actors"]); ?></td>
        <td><?=count($rule["objects"])?></td>
        <td><?=(is_array($rule["objects"])
                ? implode(",", array_map(function($action) use ($mn, $prefix) {
                        return '' . $action . '';
                    }, $rule["objects"]))
                : $rule["objects"]); ?></td>
        <td>'<?=(is_array($rule["actions"])
                ? implode(",", array_map(function($action) use ($mn, $prefix) {
                        return '' . $action . '';
                    }, $rule["actions"]))
                : $rule["actions"]); ?></td>
        <?=implode("", array_map(function($actor) use ($mn, $prefix, &$users) {
                $actor_uuid = $actor;

                if (!isset($users[$actor_uuid])) {
                    $users[$actor_uuid] = User::findByUuid($actor_uuid);
                }
                $user = $users[$actor_uuid];

                return '<td>' . $actor_uuid . '</td>'
                    . '<td>' . (isset($user->login) ? $user->login : '?') . '</td>'
                    . '<td>' . $user->lastname . ' ' . $user->givenname . '</td>'
                    . '<td>https://manage.herzen.spb.ru/user/searchfio.php?fio=' . $user->lastname . ' ' . $user->givenname . '</td>'
                    . '';
            }, $rule["actors"])); ?>
        <?=implode("", array_map(function($object) use ($mn, $prefix, &$departments) {
                $slash_pos = strpos($object, "/");
                $object_uuid = $object;
                if ($slash_pos != false) {
                    $object_uuid = substr($object, $slash_pos);
                }

                if (!isset($departments[$object_uuid])) {
                    $departments[$object_uuid] = Department::findByUuid($object_uuid);
                }
                $department = $departments[$object_uuid];

                $document = Document::findByUuid($mn, $prefix, $object_uuid);
                $documentByName = Document::findBy($mn, $prefix, array("title_ru" => $department->description));

                $nameOk = true
                    && $documentByName
                    && $department
                    && isset($documentByName->fields['title_ru'])
                    && $documentByName->fields['title_ru'] == $department->description
                    && $documentByName->uuid == $object
                    && true;

                $is_document = isUuid($object);
                $markBad = $is_document && !$nameOk;
                $object_style = '';
                if ($markBad) {
                    $object_style = 'background: salmon;';
                }


                return ''
                    . '<td>' . $object . '</td>'
                    . '<td>' . ($department->description
                        ? $department->description
                        : "Подразделение не найдено в лдапе"
                        ) . '</td>'
                    . '<td>' . ($is_document
                        ? 'объект'
                        : 'класс объектов'
                        ) . '</td>'
                    . '<td>' . ($is_document && is_object($document)
                        ? $document->uuid
                        : 'не найдено') . '</td>'
                    . '<td>' . ($is_document && is_object($document)
                        ? $document->id
                        : 'не найдено') . '</td>'
                    . '<td>' . ($is_document && is_object($document)
                        ? $document->fields['title_ru']
                        : 'не найдено') . '</td>'
                    . '<td>' . ($is_document && is_object($documentByName)
                        ? $documentByName->uuid
                        : 'не найдено') . '</td>'
                    . '<td>' . ($is_document && is_object($documentByName)
                        ? $documentByName->id
                        : 'не найдено') . '</td>'
                    . '<td>' . ($is_document && is_object($documentByName)
                        ? (isset($documentByName->fields['title_ru']) ? $documentByName->fields['title_ru'] : '')
                        : 'не найдено') . '</td>'
                    . '';
            }, $rule["objects"])); ?></td>
        <td><?=(is_array($rule["roles"])
                ? implode(",", array_map(function($action) use ($mn, $prefix) {
                        return '' . $action . '';
                    }, $rule["roles"]))
                : $rule["roles"]); ?></td>
    </tr>

<?php
        if ($i == $max) {
            break;
        }
    }
?>

</tbody>
</table>
</body>
</html>
