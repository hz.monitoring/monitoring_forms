<?php
    define('ENTRY_POINT', 'security');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();

    $security = $mn->selectDB($prefix . "security");

    $skip = isset($_GET["skip"]) ? $_GET["skip"] : 0;
    $limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;

    $rules = $security->rules->find();//->skip($skip)->limit($limit);
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

.security {

}

.security td {
    border: 1px gray solid;
}
</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
<table class="security">
<thead>
    <tr>
        <td>Rule #</td>
        <td>User</td>
        <td>Action</td>
        <td>Object[/xpath]</td>
    </tr>
</thead>
<tbody>

<?php


function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}
    class AllUsers {
        public function __get($var) {
            return "Все пользователи";
        }
    }

    $users = array(
         "*" => new AllUsers(),
    );
    $departments = array();

    $i=0;
    $max = 0;
    foreach ($rules as $rule) {
        flush();
        ob_flush();

        $i++;
?>
    <tr>
        <td><?=$i . '<br/>' . $rule["_id"]?></td>
        <td><?=implode("", array_map(function($actor) use ($mn, $prefix, &$users) {
                $actor_uuid = $actor;

                if (!isset($users[$actor_uuid])) {
                    $users[$actor_uuid] = User::findByUuid($actor_uuid);
                }
                $user = $users[$actor_uuid];

                return '<div class="element actor actor-' . $actor_uuid . '">'
                    . $actor_uuid
                    . '<br/><code>' . (isset($user->login) ? $user->login : '?') . '</code>'
                    . '<br/>' . $user->lastname . ' ' . $user->givenname
                    . ' (<a href="https://manage.herzen.spb.ru/user/searchfio.php?fio=' . $user->lastname . ' ' . $user->givenname . '">посмотреть на Менедже</a>)'
                    . '</div>';
            }, $rule["actors"])); ?></td>
        <td><?=(is_array($rule["actions"])
                ? implode("", array_map(function($action) use ($mn, $prefix) {
                        return '<div class="element action action-' . $action . '">' . $action . '</div>';
                    }, $rule["actions"]))
                : $rule["actions"]); ?></td>
        <td><?=implode("", array_map(function($object) use ($mn, $prefix, &$departments) {
                $slash_pos = strpos($object, "/");
                $object_uuid = $object;
                if ($slash_pos != false) {
                    $object_uuid = substr($object, $slash_pos);
                }

                if (!isset($departments[$object_uuid])) {
                    $departments[$object_uuid] = Department::findByUuid($object_uuid);
                }
                $department = $departments[$object_uuid];

                $document = Document::findByUuid($mn, $prefix, $object_uuid);
                $documentByName = Document::findBy($mn, $prefix, array("title_ru" => $department->description));

                $nameOk = true
                    && $documentByName
                    && $department
                    && $documentByName->fields['title_ru'] == $department->description
                    && $documentByName->uuid == $object
                    && true;

                $is_document = isUuid($object);
                $markBad = $is_document && !$nameOk;
                $object_style = '';
                if ($markBad) {
                    $object_style = 'background: salmon;';
                }


                return '<div class="element object object-' . $object . '" style="' . $object_style . '">'
                    . 'Объект: <code>'
                    . $object
                    . '</code><br/>'
                    . ($department->description
                        ? '<b>' . $department->description . '</b>'
                        : '<i>Подразделение не найдено в лдапе</i>'
                        )
                    . ($is_document
                        ? (''
                            . '<br/>'
                            . '<br/>Ищем по UUID в нашей базе: '
                            . (is_object($document)
                                ? (''
                                    . '<br/>'
                                    . "<code>{$document->uuid}</code><br/>"
                                    . "#{$document->id}, {$document->fields['title_ru']}"
                                    . '')
                                : ('<strong>не найдено</strong>')
                                )
                            . '<br/>'
                            . '<br/>Ищем по названию в нашей базе: '
                            . (is_object($documentByName)
                                ? (''
                                    . '<br/>'
                                    . "<code>{$documentByName->uuid}</code><br/>"
                                    . "#{$documentByName->id}, {$documentByName->fields['title_ru']}"
                                    . '')
                                : ('<strong>не найдено</strong>')
                                )
                            . '')
                        : '<br/>Класс объектов'
                        )
                    . '</div>';
            }, $rule["objects"])); ?></td>
    </tr>

<?php
        if ($i == $max) {
            break;
        }
    }
?>

</tbody>
</table>
</body>
</html>
