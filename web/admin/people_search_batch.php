<?php
    define('ENTRY_POINT', 'admin-standalone');
    require(__DIR__ . '/../common.inc.php');

    use pagecontrol\PageController;

    $user = User::getFromSession($_SESSION);

    if (!$user->isLogged()) {
        $formCtrl = new PageController\Herzen\Admin();
        $formCtrl->attachUser($user);
        $formCtrl->render();
        exit;
    }
    $admins = [
        'tikhontagunov'
    ];
    if (!in_array($user->login, $admins)) {
        echo 'Доступ не разрешен. <a href="/">Главная</a>';
        exit;
    }



$people_batch = "
Федоренкова Валерия Степановна
Спиридонова Елена Владимировна
Богданов Олег Андреевич
Государев Илья Борисович
Дячук Татьяна Владимировна
Кипрушина Ирина Игоревна
Кудрявцева Ирина Андреевна
Куликова Светлана Сергеевна
ПАЛЬТИЕЛЬ ЛЕВ РОМАНОВИЧ
Рябчикова Зоя Степановна
Свиридкина Елена Викторовна
Унру Софья Александровна
Фруменкова Татьяна Георгиевна
Ковалевская Милослава Глебовна
Соловьева Вайда Линасовна
Астэр Ирина Валериевна
Лобень Лариса Михайловна
Ремнев Владимир Данилович
Чернышова Светлана Леонидовна
ШЕЛКОВА ЛЮДМИЛА НИКОЛАЕВНА
Воинова Виктория Владимировна
Кулькова Марианна Алексеевна
Пастушенко Павел Владимирович
Яковлева Ольга Валерьевна
Павлова Татьяна Борисовна
Атаев Геннадий Леонидович
Шамров Иван Иванович
Матасов Юрий Тимофеевич
Фомин Владимир Владимирович
Махов Сергей Иванович
Рудаков Леонид Ильич
Окрепилов Владимир Валентинович
Швецкий Михаил Владимирович
Готская Ирина Борисовна
Лукина Людмила Евгеньевна
Максимова Елена Михайловна
Скрипкина Екатерина Александровна
Гирзекорн Светлана Николаевна
ГОРБАЧЕВ ВАСИЛИЙ ИВАНОВИЧ
Русова Татьяна Геннадьевна
Савельева Светлана Сергеевна
";

$ldap_host = "ldap.service.herzen";
$ldap_rootdn = "dc=herzen,dc=spb,dc=ru";

$ldap_handle = ldap_connect($ldap_host);
ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);

if (!$ldap_handle) {
    throw new \Exception("Ldap connection problem");
}

function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}

function escape($string) {
    return str_replace(array("(", ")"), array("\\(", "\\)"), $string);
}

    $fio = isset($_GET["fio"]) ? $_GET["fio"] : 0;

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> rules</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> rules</a>
</div> -->
<pre>

<?php

$people_found = [];

foreach (explode("\n", $people_batch) as $fio) {
                $fio_parts = explode(" ", $fio);
                $sn = array_shift($fio_parts);
                $givenname = implode(" ", $fio_parts);

                $err_string = "";
                $user_query = "(&(sn=$sn)(givenname=$givenname))";
                $users_search = ldap_search($ldap_handle, $ldap_rootdn, $user_query);
                $users = ldap_get_entries($ldap_handle, $users_search);
                $people_found["$fio"] = [];
                if (is_array($users) && isset($users["count"]) && $users["count"] == 1 && isset($users[0]) && isset($users[0]["cn"]) && isset($users[0]["cn"][0])) {
                    $user_uuid = $users[0]["cn"][0];
                } else {
                    $user_err_string = $user_query;
                    echo "\n\t- $err_string user search: $user_err_string found " . (int)$users["count"] . " persons; \n";

                    foreach ($users as $extra_user) {
                            if (!isset($extra_user["dn"])) {
                                continue;
                            }
                            $people_found["$fio"][] = $extra_user["dn"] . "<br>" . $extra_user["cn"][0] . "<br>" . $extra_user["sn"][0] . ' ' . $extra_user["givenname"][0];

                            echo '<div style="background: #ccc; margin: 5px 0px 30px;">';
                            echo "\n\t\t" . $extra_user["dn"] . "- " . $extra_user["cn"][0] . " - " . $extra_user["sn"][0] . ' ' . $extra_user["givenname"][0] . "\n";
                            echo ' <details>
                                    <summary>';
                            echo '      Подробнее';
                            echo '  </summary>';



                            echo "User dn: <code>" . $extra_user["dn"] . "</code><br><br>";
                            for ($i=0; $i < $extra_user["count"]; $i++) {
                                $extra_user_field = $extra_user[$i];
                                $extra_user_field_value = $extra_user[$extra_user_field];

                                echo "<b>" . $extra_user_field . "</b>"
                                    . "<br>"
                                    . "";

                                print_r($extra_user_field_value);

                                echo "<br>";
                            }

                            print_r($extra_user);

                            echo ' </details>';
                            echo '</div>';
                    }

                    // echo "Input user uuid: ";
                    // $user_uuid = readline();
                }
}

echo "<hr>";
echo "<table><tbody>";
foreach ($people_found as $fio => $found) {
    echo "<tr><td>$fio</td>" . implode("", array_map(function($found_one) { return "<td>$found_one</td>"; }, $found)) . "</tr>";
}
echo "</tbody></table>";

    ldap_close($ldap_handle);


?>
</pre>
</body>
</html>
