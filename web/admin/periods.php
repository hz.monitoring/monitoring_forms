<?php
    define('ENTRY_POINT', 'model');
    require(__DIR__ . '/../common.inc.php');

    use documongo\MongoObject\Document;
    use documongo\MongoObject\DocumentType;

    use pagecontrol\PageController\Configurator;
    $mn = new \MongoClient(Configurator::getServerString());
    $prefix = Configurator::getDbPrefix();

    $model = $mn->selectDB($prefix . "model");

    $skip = isset($_GET["skip"]) ? $_GET["skip"] : 0;
    $limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;
    $type = isset($_GET["type"]) ? $_GET["type"] : "chair";

    $chairType = $model->types->findOne(["type" => $type]);//->skip($skip)->limit($limit);

    $available_periods = array_reduce(array_keys($chairType["available_periods"]), function($available_periods, $item_key) use ($chairType) {
        $item_desription = $chairType["available_periods"][$item_key];
        foreach ($item_desription as $item_desription_key => $item_desription_value) {
            $available_periods[$item_desription_key][$item_key] = $item_desription_value;
        }
        return $available_periods;
    }, []);
    // var_dump($available_periods);
    // die;
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="/css/jquery-ui.css" />
  <script src="/js/jquery-1.9.1.js"></script>
  <script src="/js/jquery-ui.js"></script>


<style type="text/css">

.model {

}

.model td {
    border: 1px gray solid;
    white-space: nowrap;
}
</style>

<script type="text/javascript">
$(function(){
    $(".element")
    .click(function(){

    })

})
</script>
</head>
<body>

<!-- <div>
<a href="?limit=<?=$limit?>&skip=<?=$skip-$limit?>">Prev <?=$limit?> types</a>
<a href="?limit=<?=$limit?>&skip=<?=$skip+$limit?>">Next <?=$limit?> types</a>
</div> -->
<table class="model">
<!-- <thead>
    <tr>
        <td>name</td>
        <td>type</td>
        <td>index</td>
        <td>label_ru</td>
        <td>placeholder_ru</td>
        <td>index_code_ru</td>
        <td>required</td>
        <td>no_i18n</td>
        <td>no_versioning</td>
        <td>multiple</td>
        <td>short_description_ru</td>
        <td>long_descri</td>
        <td>Object name</td>
        <td>Object type</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по UUID в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Ищем по названию в нашей базе</td>
        <td>Роль</td>
    </tr>
</thead> -->
<tbody>

<?php


function isUuid($string) {
    return preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $string);
}
    class AllUsers {
        public function __get($var) {
            return "Все пользователи";
        }
    }

    $users = array(
         "*" => new AllUsers(),
    );
    $departments = array();

    $i=0;
    $max = 0;

    // var_dump($chairType);die;

    $now = (new \DateTimeImmutable())->format("Y-m-d");

?>
    <thead>
        <tr>
            <?php foreach (array_keys($available_periods) as $item_key): ?>
                <td><?=$item_key?></td>
            <?php endforeach; ?>
        </tr>
    </thead>

<?php

    foreach($chairType["available_periods"] as $item_num => $item_desription) {
        // var_dump($item_desription, $item_num);
        // flush();
        // ob_flush();

        $isActual = false;
        // var_dump($item_desription["since"]);
        // var_dump($now);
        if (isset($item_desription["since"]) && $item_desription["till"]) {
            $isActual = $item_desription["since"] <= $now && $item_desription["till"] >= $now;
        } elseif (isset($item_desription["since"])) {
            $isActual = $item_desription["since"] <= $now;
        } elseif (isset($item_desription["till"])) {
            $isActual = $item_desription["till"] >= $now;
        }

        $availableBgColor = "#ffffee";
        $availableColor = "#000000";
        if (isset($item_desription["available"])) {
            $availableBgColor = $item_desription["available"] ? "lightgreen" : "salmon";
            $availableColor = $item_desription["available"] ? "green" : "red";
        }
        $isMain = isset($item_desription["main"]) && $item_desription["main"];

        $i++;
?>
    <tr
        style="
            <?=($isActual?"background-color: $availableBgColor;":"")?>
            <?=($isMain?"font-weight: bold;":"")?>
            color: <?=$availableColor?>;
            "
    >
<?php
        foreach (array_keys($available_periods) as $item_key) {
            ?>

            <td><?=isset($item_desription[$item_key]) ? $item_desription[$item_key] : ""?></td>
            <?php
        }
?>

    </tr>

<?php
        if ($i == $max) {
            break;
        }
    }
?>

</tbody>
</table>
</body>
</html>
