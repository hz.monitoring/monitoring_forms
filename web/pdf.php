<?php
header("Content-Type: text/html; charset=utf-8");
echo '<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>Генерация PDF... </p><p>Вы будете автоматически перенаправлены назад после завершения!</p>';
if(!isset(	 $_POST['chairID']
			,$_POST['chairName']
			,$_POST['fakNAME']
			,$_POST['srcAddr']
			,$_POST['tableDATA'])){
	die('<p style="color:red;">
	<b>Внимание! Данные не полные, генерация файла PDF невозможна! 
	Пожалуйста, сообщите об этом.</b>
	</p>');
}else{
	require_once('../vendor/autoload.php');
	
	$connection = require_once('../config/phperge.php');
	
	$doc = new \PHPerge\Document("chair_data_9", $connection);

	$tableArr=unserialize(rawurldecode($_POST['tableDATA']));

	$filename = md5($_POST['chairID']);
	$filename = 'pdf/'.$filename.'.pdf';

	$doc->setField("CHAIRNAME", $_POST['chairName']);
	$doc->setField("FAKNAME",$_POST['fakNAME']);
	$doc->setField("PDFDATE",date("Дата: d.m.Y года."));
	$doc->setTable("CHAIRDATA", $tableArr);
		
	file_put_contents($filename,$doc->getPDFContent());

	sleep(20);
	header('Location: '.$_POST['srcAddr']); 
}

