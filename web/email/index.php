<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    mb_internal_encoding("UTF-8");

    session_start();

    require_once(__DIR__ . "/../../vendor/autoload.php");
    use pagecontrol\PageController;
    $user = User::getFromSession($_SESSION);
    $view = isset($_GET["view"]) ? $_GET["view"] : "monitor";
    switch ($view) {
        case 'monitor':
        default:
            $formCtrl = new PageController\Herzen\ExternalMonitoring\Email\Monitor();
            break;
    }
    $formCtrl->attachUser($user);
    $formCtrl->render();
