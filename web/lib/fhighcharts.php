<?php
class HighCharts{

public function getBar($arraydata, $title = '', $subtitle = '', $cssId = 'container', $plotcolor = '#555555',$height = '400px',$max=100,$ytext='баллы'){
//	var_dump($height);
	$categories = implode("', '", array_keys($arraydata));
	$categories = "'".$categories."'";
	$data = implode(', ', array_values($arraydata));
	$out['body']='
<div id="'.$cssId.'" style="min-width: 600px; max-width: 1200px; width:100%; margin: 0 auto; background-color:#ffffff; height:'.$height.';">
	</div>
	';
	$out['head'] = "
<script>
$(function () {
$('#".$cssId."').highcharts({
exporting:{
enabled: false
},
chart: {
type: 'bar'
},
colors:['".$plotcolor."'],
title: {
text: '".$title."',
style:{
fontWeight:'bold',
}
},
subtitle: {
text: '".$subtitle."',
style:{
fontSize: '16px',
fontStyle:'italic',           
}
},
xAxis: {
categories: [".$categories."],
title: {
text: 'рейтинг кафедр РГПУ им. А.И. Герцена'
},
labels: {
overflow: 'justify',
style: {
fontSize: '15px',
fontWeight:'bold',
color: '".$plotcolor."'
}
}
},
yAxis: {
max:".$max.",
min: 0,
title: {
text: '".$ytext."',
align: 'high',
style: {
fontSize: '17px',
}
},
labels: {
overflow: 'justify',
style: {
fontSize: '17px',
color: '#000000'
}
}
},
tooltip: {
valueSuffix: ' баллов',
style: {
fontSize: '17px',
color: '#000000'
}
},
plotOptions: {
bar: {
dataLabels: {
enabled: true,
style: {
fontSize: '17px',
color: '#000000'
}
},				
}
},
legend: {
layout: 'vertical',
enable: false,
align: 'right',
verticalAlign: 'bottom',
x: -40,
y: 100,
floating: true,
borderWidth: 1,
backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
shadow: true
},
credits: {
enabled: false
},
series: [{
name: 'набрано',
data: [".$data."],
style: {
fontSize: '17px'
}
}]
});
});
</script>
";
	return $out;
	}
}
