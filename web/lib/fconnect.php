<?php

class DBConnect{
	private static $instance;
	private static $a;
	private static $prefix;
	private static $host;
	private static $port;
	private static $user;
	private static $pass;
	private static $server;
    private $m;
    private $dbSecurity;
    private $dbData;
    private $dbModel;

    public $colRules;
    public $colModel;
    public $colDocs;
    public $colVisit;
    public $colMessages;
    public $colUnits;
    public $colUnitRate;

    private function __construct(){

        $a                  = require(__DIR__ .'/../../conf/conf.php');
        $prefix             = $a['db_prefix'];
        $host				= $a['mongo_host'];
        $port				= $a['mongo_port'];
        $user				= $a['db_user'];
        $pass               = $a['db_pass'];
        $auth               = $a['db_auth'];
        $db                 = $a['db_db'];
        if ($auth) {
            $server         = 'mongodb://'.$user.':'.$pass.'@'.$host.':'.$port.'/'.$db;
        } else {
            $server         = 'mongodb://'.$host.':'.$port;
        }

        try {
            $this->m            = new \MongoClient($server);

        } catch (\Exception $e) {
            if (isset($_GET["debug"]) && $_SERVER['REMOTE_ADDR'] == "10.2.24.225") {
                throw $e;
            }
            require(__DIR__ . '/under_construction.html');
            exit;
        }
        $this->dbSecurity   = $this->m->selectDB($prefix . 'security');
        $this->dbData       = $this->m->selectDB($prefix . 'data');
        $this->dbModel      = $this->m->selectDB($prefix . 'model');

        $this->colRules     = new MongoCollection($this->dbSecurity, 'rules');
        $this->colModel     = new MongoCollection($this->dbModel, 'types');
        $this->colDocs      = new MongoCollection($this->dbData, 'documents');
        $this->colVisit     = new MongoCollection($this->dbData, 'visit');
        $this->colMessages  = new MongoCollection($this->dbData, 'messages');
        $this->colUnits		= new MongoCollection($this->dbData, 'units');
        $this->colUnitRate	= new MongoCollection($this->dbData, 'unit_rating');
	}

	public static function getInstance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
