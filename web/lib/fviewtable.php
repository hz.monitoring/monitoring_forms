<?php
class ViewTable{
	
	public function __construct(){
		require_once("lib/fconnect.php");
		$this->db = DBConnect::getInstance(); 
	}
	
	public function confirmAllPeriods($uuid){
		$chairID = array('uuid'=>$uuid);
		$dataset = array('$set'=>array('confirmed'=>1));
		$this->db->colDocs->update($chairID, $dataset);
		
		$a = $this->db->colDocs->findOne(array('uuid'=>$uuid));
		foreach($a['versions'] as $key => $value){
			$dataset = array('$set'=>array('versions.'.$key.'.content.confirmed'=>1));
			$this->db->colDocs->update($chairID, $dataset);
		}
	}
}
