<?php
/*
 *
 * Roles class for ComplexRating scripts.
 *
 * @author Deviatkov Aleksandr <web@melior.biz>
 *
 */
class Role{

public function __construct(){
	require_once('fconnect.php');
	$this->db = DBConnect::getInstance();
}

public function isPageEnable($uuid){
	return true;
	$page = $_SERVER['SCRIPT_NAME'];// PHP_SELF?
    $page = basename($page, '.php');
	$list = $this->enablePageList($uuid);
    // var_dump($list, $page, $_SERVER);die;
	foreach($list as $value){
		if($value === $page){
			return true;
		}
	}
	$index = $this->indexPage($uuid);

	//header("Location: $index");
}

public function getUserStatus(){

	}

public function indexPage($uuid){
	$index = false;
	//return true;
	$weight = 9999999;
	$my_roles = $this->getRoles($uuid);
	//var_dump($my_roles);
	foreach($my_roles as $role){
		if($role=='guest') {
			$user = User::findByUuid($uuid);
			require(__DIR__ . '/guest.html');
			exit;
		}
// 			die('
// <html>
// <head>
// <style>
// html, body {
// 	margin: 0;
// 	overflow: hidden;
// }
// </style>
// </head>
// <body>
// <div class="fullscreen-bg">
//     <div class="overlay">
// 		<h1>Информационно-аналитическая система</h1>
// <ul>
// <li>Для получения доступа уровня "проректор" обратитесь к Пучкову М.Ю. , телефон приемной 570-03-88 (помощник проректора Черниченко Анна Николаевна)</li>
// <li>Для получения доступа уровня "<!--начальник управления-->", "<!--начальник отдела-->" обратитесь к Шестаковой Р.В. телефон 571-29-52 </li>
// <li>Для получения доступа уровня "директор института", "директор филиала", "декан факультета", "заведующий кафедрой" обратитесь к Шестаковой Р.В. телефон 571-29-52</li>
// <li>Для получения доступа уровня "ответственный за ввод данных по кафедре" обратитесь к Тихомирову Александру Владимировичу <a href="mailto:complexrating@herzen.spb.ru">complexrating@herzen.spb.ru</a> или по телефону +7 (904) 6 - 444 - 932 </li>
// <li>Для получения доступа уровня "обучающийся", обратитесь к </li>
// </ul>
// В случае возникновения вопросов или предложений вы можете воспользоваться мобильной поддержкой пользователей:
// <ul>
// <li>добавьте контакт +7 (904) 6 - 444 - 932</li>
// <li>сфотографируйте Экран монитора с сообщением об ошибке</li>
// <li>напишите ваш вопрос в Whatsapp, Viber, Telegram или на <a href="mailto:complexrating@herzen.spb.ru">электронную почту complexrating@herzen.spb.ru</a></li>
// </ul>

//     </div>
// </div>
// </body>
// </html>
// 		');
		if($role == 'amenable'){
			return false;
		}	
		$allRoles = $this->db->colModel->findOne(array('type'=>'roles'));
		if(isset($allRoles['roles'][$role]['weight']) && $weight > $allRoles['roles'][$role]['weight']){
			$weight = $allRoles['roles'][$role]['weight'];
			$index = $allRoles['roles'][$role]['index'];
		}
	}
	if ($index == "index") {
		$index = "reglament";
	}
	if(strlen($index)>0){
		$index = 'http://'.$_SERVER['HTTP_HOST'].'/'.$index.'.php';
	}
return $index;
}

public function userMenu($uuid){
	//return '';
	$out = '<ul class="pages pages--level1">';
	$allMenus = $this->db->colModel->findOne(array('type'=>'menus'));
	$allMenus = $allMenus['menus'];
	$my_roles = $this->getRoles($uuid);
	$outs = [];
	foreach($my_roles as $role){
		foreach($allMenus as $fileName => $menu){
			foreach($menu['roles'] as $menuRole){
				if($menuRole === $role){
					$outs[$fileName] = '<li class="pages__page' . (stripos($_SERVER["REQUEST_URI"], $fileName . ".php") === 1 ? ' pages__page--active active ' : '') . '"><a href="'
					.$fileName.
					'.php"><button class="btn btn-link">'
					.$menu['title_ru'].
					'</button></a>
					</li>';
				}
			}
		}
	}
	$out.= implode("", $outs);
	$out.= '</ul>';
	return $out;
}
public function menusByRoles(){
	//echo"<pre>";
	$allMenus = $this->db->colModel->findOne(array('type'=>'menus'));
	$roles = $this->getAllRoles('');
	$out = '<table>';
	foreach($roles as $role => $arr){
		//var_dump($arr);
		$out.='<tr><th>'.$arr['title_ru'].' ('.$role.'), index='.$arr['index'].'.php</th></tr>
		<tr><td><ul>';
		foreach($allMenus['menus'] as $file => $link){
			foreach($link['roles'] as $one){
				if($one==$role){
						$out .= '<li>'.$link['title_ru'].' ('.$file.'.php)</li>';
				}
			}
		}
		$out.='</ul></td></tr>';
	}
	$out .= '</table>';
	return $out;
}
public function getAllRoles($uuid){
	$allRolesList = array();
	$allRoles = $this->db->colModel->findOne(array('type'=>'roles'));
	foreach($allRoles['roles'] as $key => $value){
		$allRolesList[$key] = $value;
	}
	return $allRolesList;
}

public function getRoles($uuid){
	$myRoles = array();
	$entries = $this->db->colRules->find(array('actors'=>$uuid));
	foreach($entries as $entry){
		if(isset($entry['roles'])){
			foreach($entry['roles'] as $role){
				$myRoles[] = $role;
			}
		}
	}
    $user = User::findByUuid($uuid);
	if(count($myRoles) == 0){
		// if ($uuid == "7006c81d-b869-4110-a00d-08d7ed69c70c" || $uuid == "009466f6-e180-4bab-b761-b360073cf281" || $uuid == "bc853442-191f-469d-9dd2-cfaa3063f0ea") {
			// echo "[$user->login]";

			$userEntry = $user->getEntry();
			if ($userEntry && isset($userEntry["employeetype"][0]) && $userEntry["employeetype"][0] == "student") {
				$myRoles[] = "student";
			}
			if (isset($user->login) && is_numeric($user->login) && in_array(strlen($user->login), [6, 7])) {
				$myRoles[] = "student";
			}
			// var_dump($myRoles);
		// }
	}

	if(count($myRoles) == 0){
		$myRoles[] = 'guest';
	}
	$myRoles = array_unique($myRoles);
return $myRoles;
}

protected function enablePageList($uuid){
	$myRoles = $this->getRoles($uuid);
	$allMenus = $this->db->colModel->findOne(array('type'=>'menus'));
	$pages = array();
	foreach($myRoles as $myRole){
		foreach($allMenus['menus'] as $key => $value){
			foreach($value['roles'] as $role){
				if($myRole === $role){
					$pages[] = $key;
				}
			}
		}
	}
	return $pages;
}

}
