<?php
/*
 * Service file for ComplexRating scripts. 
 * 
 * @author Deviatkov Aleksandr <web@melior.biz>
 * @author Tagunov Tikhon <niptuckker@gmail.com>
 * 
 */
require_once("lib/fconnect.php");
$db = DBConnect::getInstance();

if(isset($currentPage) and $currentPage->uuid != NULL){
	$collectionMessages = $db->colMessages;
	$doc = $db->colDocs->findOne(array('uuid'=>$currentPage->uuid));
	$chairlist = '<ul>';
	$chairrate = 0;
	if(isset($doc['teachers']) && is_array($doc['teachers'])){
		foreach ($doc['teachers'] as $teacher) {
		$chairlist .= '<li>'.$teacher['name'].'</li>';
		$chairrate += $teacher['rate'];
		}
	}
	$chairlist .= '</ul>';
	if($chairrate == 0){
		$chairlist = '';
		$chairrate = 0;
		}
}
if(isset($this->user->uuid)){ 
$collectionMessages = $db->colMessages;
}
