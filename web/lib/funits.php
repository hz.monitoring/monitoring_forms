<?php
class Unit{

public function __construct(){
	require_once("lib/fconnect.php");
	$this->db = DBConnect::getInstance();
}

public function statusMenu(){
	$out = '<ul>';
	$active['student']='';
	$active['dean']='';
	if(isset($_GET['st'])){
		if($_GET['st']=='student'){
			$active['student']=' pages__page--active';
		}
		if($_GET['st']=='dean'){
			$active['dean']=' pages__page--active';
		}
	}
	$out .= '<li class="pages__page'.$active['student'].'">
	<a href="/unitsrating.php?st=student">
	<button class="btn btn-link">Оценка студентами и членами Совета обучающихся</button>
	</a>
	</li>
	';
	$out .= '<li class="pages__page'.$active['dean'].'">
	<a href="/unitsrating.php?st=dean">
	<button class="btn  btn-link">Оценка директорами институтов и деканами факультетов</button>
	</a>
	</li>
	';
	$out .= '</ul>';
	return $out;
}

public function unitsMenu($status){
	$i=1;
	switch($status){
		case 'student':
			$out = '<h5>Подразделения, которые оценивали студенты</h5>';
		break;
		case 'dean':
			$out = '<h5>Подразделения, которые оценивали деканы</h5>';
		break;
	}
	$out .= '<table class="allstat">
	<tr><th>№</th><th>Название</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th></tr>';
	$allUnits = $this->db->colUnits->find(array('get.'.$status=>true));
	foreach($allUnits as $oneUnit){
		$unitMarks = $this->getOneUnitMarks($oneUnit['id'],$status);
		$out.='
		<tr>
		<td>'.$i++.'</td>
		<td><a href="/unitsrating.php?id='.$oneUnit['id'].'&st='.$status.'">'.$oneUnit['name'].'</a></td>
		<td>'.$unitMarks['par-1']['avg'].'</td>
		<td>'.$unitMarks['par-2']['avg'].'</td>
		<td>'.$unitMarks['par-3']['avg'].'</td>
		<td>'.$unitMarks['par-4']['avg'].'</td>
		<td>'.$unitMarks['par-5']['avg'].'</td>
		<td>'.$unitMarks['par-6']['avg'].'</td>
		<td>'.$unitMarks['par-7']['avg'].'</td>
		</tr>';
	}
	$out .= '</table>';
	return $out;
}


public function allMarks($unitID=0,$status){
	$out = '';
	$params['par-1']['name'] = 'Частота взаимодействия';
	$params['par-2']['name'] = 'Выполнение правил распорядка';
	$params['par-3']['name'] = 'Компетентность сотрудников' ;
	$params['par-4']['name'] = 'Оперативность рассмотрения обращений';
	$params['par-5']['name'] = 'Культура общения сотрудников';
	$params['par-6']['name'] = 'Оценка материалов';
	$params['par-7']['name'] = 'Использование электронных ресурсов';
	for($i = 1; $i<=7; $i++){
		$par = 'par-'.$i;
		for($j = 0; $j<=5; $j++){
			$params[$par][$j] = 0;
			$unitParams[$par][$j] = 0;
		}
	}
	$allStatusMarks = $this->db->colUnitRate->find(array('status'=>$status));
	$allMarksArray = [];
	foreach($allStatusMarks as $oneStatusMarks){

		$allMarksArray[$oneStatusMarks['uid']]=$oneStatusMarks['data'];
	}
	foreach($allMarksArray as $studentMarks){
		if($unitID>0 && isset($studentMarks[$unitID])){
			for($i = 1; $i<=7; $i++){
				$par = 'par-'.$i;
				switch($studentMarks[$unitID][$par]){
					case 0:
						$unitParams[$par][0]++;
					break;
					case 1:
						$unitParams[$par][1]++;
					break;
					case 2:
						$unitParams[$par][2]++;
					break;
					case 3:
						$unitParams[$par][3]++;
					break;
					case 4:
						$unitParams[$par][4]++;
					break;
					case 5:
						$unitParams[$par][5]++;
					break;
				}
			}
		}
		foreach($studentMarks as $studentUnitMarks){
			for($i = 1; $i<=7; $i++){
				$par = 'par-'.$i;
				switch($studentUnitMarks[$par]){
					case 0:
						$params[$par][0]++;
					break;
					case 1:
						$params[$par][1]++;
					break;
					case 2:
						$params[$par][2]++;
					break;
					case 3:
						$params[$par][3]++;
					break;
					case 4:
						$params[$par][4]++;
					break;
					case 5:
						$params[$par][5]++;
					break;
				}
			}
		}
	}
	if($unitID>0 && isset($studentMarks[$unitID])){
		$allUnits = $this->db->colUnits->findOne(array('id' => $unitID));
		$plotTitle = $allUnits['name'];
		$out = '<h4>Таблица значений для выбранного подразделения: "'.$plotTitle.'"</h4>';
			$out .= '<table class="allstat">
			<tr>
			<th>Показатель</th>
			<th>0</th>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>Всего</th>
			<th>Среднее</th></tr>';
			for($i = 1; $i<=7; $i++){
				$par = 'par-'.$i;
				$sumP1[$par] = $unitParams[$par][0] + $unitParams[$par][1] + $unitParams[$par][2] + $unitParams[$par][3] + $unitParams[$par][4] + $unitParams[$par][5];
				$sumNo0P1[$par] = $unitParams[$par][1] + $unitParams[$par][2] + $unitParams[$par][3] + $unitParams[$par][4] + $unitParams[$par][5];
				if($sumNo0P1[$par] !== 0){
					$avgP1[$par] = ($unitParams[$par][1]*1 + $unitParams[$par][2]*2 + $unitParams[$par][3]*3 + $unitParams[$par][4]*4 + $unitParams[$par][5]*5)/$sumNo0P1[$par];
				}else{
					$avgP1[$par] = 0;
				}
				$out .= '
				<tr>
					<td>'.$params[$par]['name'].'</td>
					<td>'.$unitParams[$par][0].'</td>
					<td>'.$unitParams[$par][1].'</td>
					<td>'.$unitParams[$par][2].'</td>
					<td>'.$unitParams[$par][3].'</td>
					<td>'.$unitParams[$par][4].'</td>
					<td>'.$unitParams[$par][5].'</td>
					<td>'.$sumP1[$par].'</td>
					<td class="redtext">'.$avgP1[$par].'</td>
				</tr>';
			}
			$out .='</table>';
	}


	$out .= '<h4>Статистика</h4>';
	$markCount = 46*count($allMarksArray);
	$out .= '<p><b>Общая таблица, характеризует то, какие оценки выставлялись в целом, делает прозрачным расчет средних  для каждого показателя</b></p>';
	$out .= '<table class="allstat">
	<tr>
		<th>Показатель</th>
		<th>0</th>
		<th>1</th>
		<th>2</th>
		<th>3</th>
		<th>4</th>
		<th>5</th>
		<th>Всего</th>
		<th>Среднее по вузу</th>
	</tr>';
	for($i = 1; $i<=7; $i++){
		$par = 'par-'.$i;
		$sum[$par] = $params[$par][0] + $params[$par][1] + $params[$par][2] + $params[$par][3] + $params[$par][4] + $params[$par][5];
		$sumNo0[$par] = $params[$par][1] + $params[$par][2] + $params[$par][3] + $params[$par][4] + $params[$par][5];
		if($sumNo0[$par]!==0){
			$avg[$par] = ($params[$par][1]*1 + $params[$par][2]*2 + $params[$par][3]*3 + $params[$par][4]*4 + $params[$par][5]*5)/$sumNo0[$par];
		}else{
			$avg[$par] =0;
		}

		$out .= '
		<tr>
			<td>'.$params[$par]['name'].'</td>
			<td>'.$params[$par][0].'</td>
			<td>'.$params[$par][1].'</td>
			<td>'.$params[$par][2].'</td>
			<td>'.$params[$par][3].'</td>
			<td>'.$params[$par][4].'</td>
			<td>'.$params[$par][5].'</td>
			<td>'.$sum[$par].'</td>
			<td class="redtext">'.$avg[$par].'</td>
		</tr>';
	}
	$out .= '</table>';
	$out .= '<p class="redtext">Нули при расчете среднего не считаем. 0 обозначает, что оценка для подразделения+показателя не выставлялась!</p>';
	$minMaxUnits = $this->getMinMaxMarks($status);

	$out .= '<table class="allstat">
	<tr><th>Показатель</th><th>Худшая оценка</th><th>Медианная оценка</th><th>Лучшая оценка</th></tr>';
	for($i = 1; $i<=7; $i++){
	$par = 'par-'.$i;
	$out .= '
	<tr>
		<td>'.$params[$par]['name'].'</td>
		<td>'.$minMaxUnits[$par]['min'].'</td>
		<td>'.$minMaxUnits[$par]['med'].'</td>
		<td>'.$minMaxUnits[$par]['max'].'</td>
	</tr>';
	}
	$out .= '</table>';

if($unitID>0 && isset($studentMarks[$unitID])){
	$param[0][0]='Подразделение';
	$param[0][1]='Частота взаимодействия . ';
	$param[0][2]='Выполнение правил распорядка';
    $param[0][3]='Компетентность сотрудников' ;
	$param[0][4]='Оперативность рассмотрения обращений';
	$param[0][5]='Культура общения сотрудников';
	$param[0][6]='Оценка материалов';
	$param[0][7]='Использование электронных ресурсов';

	$param[1][0]="Среднее значение";
	$param[2][0]="Подразделение";

	for($i=1; $i<=7;$i++){
		$par = 'par-'.$i;
		$param[2][$i]=$this->privedenie($minMaxUnits[$par]['min'], $minMaxUnits[$par]['med'], $minMaxUnits[$par]['max'], $avgP1[$par]);
		$param[1][$i]=50;
	}
	$subtitle='';
	if(isset($_GET['st'])){
		if($_GET['st']=='student'){
			$subtitle='Оценка студентами и членами Совета обучающихся';
		}
		if($_GET['st']=='dean'){
			$subtitle='Оценка директорами институтов и деканами факультетов';
		}
	}
	$out .= "
<script>
$(function () {
$('#container').highcharts({
exporting:{
enabled: false
},
credits: {
enabled: false
},
        chart: {
            polar: true,
            type: 'line',
        },
        title: {
            text: '$plotTitle',
            x: -20
        },
        subtitle: {
            text: '$subtitle',
            x: -20
        },
        pane: {
            size: '80%'
        },
        xAxis: {
            categories: [
							'{$param[0][2]}',
							'{$param[0][3]}',
							'{$param[0][4]}',
							'{$param[0][5]}',
							'{$param[0][6]}',
							'{$param[0][7]}'
							],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },
        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0,
            max: 100
        },

        tooltip: {
            shared: true,
            pointFormat: '<span>{series.name} <b>{point.y:,.0f} баллов</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },
        series: [{
            name: '{$param[1][0]}',
            data: [
            {$param[1][2]},
            {$param[1][3]},
            {$param[1][4]},
            {$param[1][5]},
            {$param[1][6]},
            {$param[1][7]}
            ],
            pointPlacement: 'on'
        },{
            name: '{$param[2][0]}',
            data: [
            {$param[2][2]},
            {$param[2][3]},
            {$param[2][4]},
            {$param[2][5]},
            {$param[2][6]},
            {$param[2][7]}
            ],
            pointPlacement: 'on'
        }
        ]

    });
});
</script>

	";
}
	return $out;
}


public function allMarksSource($unitID=0,$status){
	$out = '';

	$allMarksArray = [];

	$allDepartments = [];
	$allStatusMarks = $this->db->colUnitRate->find(array('status'=>$status));
	foreach($allStatusMarks as $oneStatusMarks){
		$user_uuid = $oneStatusMarks['uid'];
		$user = User::findByUuid($user_uuid);
		// $ou_uuid = isset($user->department_uuid) ? $user->department_uuid : null;
		// var_dump($user, $ou_uuid);

		$userParent = $user->getDepartment();
		$userParentUuid = $userParent ? $userParent->uuid : null;

		$allMarksArray[$userParentUuid][] = $oneStatusMarks;
		$allDepartments[$userParentUuid] = $userParent;
	}
	$out .= '<h4>Распределение оценок по подразделениям оценивающих пользователей</h4>';
	$out .= '<table>';
	$out .= ''
		. '<tr>'
		. '<td>'
		. '<b>Подразделение</b>'
		. '</td>'
		. '<td>'
		. '<b>Количество оценок</b>'
		. '</td>'
		. '</tr>'
		. '';

	foreach ($allMarksArray as $departmentUuid => $departmentMarks) {
		$department = $allDepartments[$departmentUuid];
		$out .= ''
			. '<tr>'
			. '<td style="border: 1px solid black; padding: 2px 5px;">'
			. ($departmentUuid ? $department->description : "Неизвестно")
			. '</td>'
			. '<td style="border: 1px solid black; padding: 2px 5px;">'
			. count($departmentMarks)
			. '</td>'
			. '</tr>'
			. '';
	}
	$out .= '</table>';
	return $out;
}

protected function getOneUnitMarks($id,$status){
	for($i=1;$i<=7;$i++){
		for($j=0;$j<=5;$j++){ $marks['par-'.$i][$j]=0;}
		$marks['par-'.$i]['test']=0;
		$marks['par-'.$i]['sum']=0;
		$marks['par-'.$i]['avg']=0;
		$marks['par-'.$i]['med']=0;
	}
	foreach($this->db->colUnitRate->find(array('status'=>$status)) as $rating){

			for($j=1;$j<=7;$j++){
				for($i=1;$i<=5;$i++){
					if(isset($rating['data'][$id]['par-'.$j]) && $rating['data'][$id]['par-'.$j] == $i){
						$marks['par-'.$j][$i]++;
						if($i!=0){$marks['par-'.$j]['test']++;}
						$marks['par-'.$j]['sum'] +=$rating['data'][$id]['par-'.$j];
					}
				}
				if($marks['par-'.$j]['test']>0){$marks['par-'.$j]['avg']=round($marks['par-'.$j]['sum']/$marks['par-'.$j]['test'],3);}
			}

	}
	for($k=1;$k<=7;$k++){
		$nomer = round($marks['par-'.$k]['test']/2);
		for($l=1;$l<=5;$l++){if($marks['par-'.$k][$l]<$nomer){$nomer=$nomer-$marks['par-'.$k][$l];}else{$marks['par-'.$k]['med']=$l;break;}}
	}
return $marks;
}

protected function getMinMaxMarks($status){
	$allunits = $this->db->colUnits->find(array('get.'.$status=>true));
	for($i=1; $i<=7; $i++){
	$minmax['par-'.$i]['min']=5;
	$minmax['par-'.$i]['max']=0;
	$minmax['par-'.$i]['med']=0;
	}
	foreach($allunits as $oneunit){
		$unitmarks = $this->getOneUnitMarks($oneunit['id'],$status);
		for($i=1; $i<=7; $i++){
			$for_median_calculating['par-'.$i][$oneunit['id']]=$unitmarks['par-'.$i]['avg'];
			if($unitmarks['par-'.$i]['avg'] > $minmax['par-'.$i]['max']){
				$minmax['par-'.$i]['max'] = $unitmarks['par-'.$i]['avg'];
				$minmax['par-'.$i]['max-id']=$oneunit['id'];
			}
			if($unitmarks['par-'.$i]['avg'] < $minmax['par-'.$i]['min']){
				$minmax['par-'.$i]['min'] = $unitmarks['par-'.$i]['avg'];
				$minmax['par-'.$i]['min-id']=$oneunit['id'];
			}
		}
	}
	for($i=1; $i<=7; $i++){
		sort($for_median_calculating['par-'.$i]);
		$countArray = count($for_median_calculating['par-'.$i]);
		if(is_int($countArray/2)){
			$key1 = $countArray/2;
			$key2 = $countArray/2+1;
			$minmax['par-'.$i]['med'] = ($for_median_calculating['par-'.$i][$key1] + $for_median_calculating['par-'.$i][$key2])/2;
		}else{
			$key1 = floor($countArray/2);
			$minmax['par-'.$i]['med'] = $for_median_calculating['par-'.$i][$key1];
		}
	}
	return $minmax;
}

protected function privedenie($min, $med, $max, $value){
	$out = 0;
	if($value < $min || $value > $max){
		die('Ошибка приведения: неправильные параметры функции приведения');
	}
	if($value == $min){return 0;}
	if($value == $max){return 100;}
	if($value == $med){return 50;}
	if($value > $min && $value < $med){
		return 50*($value-$min)/($med-$min);
	}
	if($value > $med && $value < $max){
		return 50+50*($value-$med)/($max-$med);
	}
}




//------------------------------------------------------------------------------------








public function changeURLs($arr){
	foreach($arr as $unitID => $unitURL){
			$object = array('id' => $unitID);
			$newdata = array('$set' => array('url' => $unitURL));
			$this->db->colUnits->update($object, $newdata);
	}
}

public function adminList(){
	$out = '<form method="post" action="units_admin.php">';
	$out .= '<input name="change_urls" type="submit" value="Сохранить изменения"><p><ul>';
	$allUnits = $this->getUnitList();
	foreach($allUnits as $key => $value){
		if($value['url']!==''){
		$value['name'] = '<a href="'.$value['url'].'" target="_blank">'.$value['name'].'</a>';
		}
		$out .= '<li><input name="'.$key.'" type="text" value="'.$value['url'].'"> '.$value['name'].' </li>';
	}
	$out .= '</ul></p><input name="change_urls" type="submit" value="Сохранить изменения">';
	$out .= '</form>';
	return $out;
}


protected function getUnitList(){
	$unitList = array();
	$units = $this->db->colUnits->find();
	foreach($units as $unit){
		$unitList[$unit['id']]['name'] = $unit['name'];
		if(!isset($unit['url'])){
			$unit['url']='';
		}
		$unitList[$unit['id']]['url'] = $unit['url'];
	}
	return $unitList;
}

public function testRating($user){
	$doc = $this->getUserDoc($user);
	if($doc['confirm']){
		return false;
	}else{
		return true;
	}
}
public function unConfirm($user){
	$object = array("uid"=>$user->uuid);
	$newdata = array('$set'=> array('confirm' => false));
	$this->updateUserDoc($object, $newdata);
}

public function saveMarks($post, $user){
	$object = array("uid"=>$user->uuid);
	if(isset($post['save'])){
		unset($post['save']);
	};
	if(isset($post['confirm'])){
		unset($post['confirm']);
		$newdata = array('$set'=> array('confirm' => true));
		$this->updateUserDoc($object, $newdata);
	};
	foreach($post as $key => $value){
		$pos = strpos($key,'_');
		$currentObjectId=substr($key,$pos+1);
		$currentParam=substr($key,0, $pos);
		$newdata = array('$set'=> array('data.'.$currentObjectId.'.'.$currentParam => (float)$value));
		$this->updateUserDoc($object, $newdata);
	}
}

public function allAupList($user){
	$units = $this->getUserUnitList($user);
	$list = '<ul>';
	foreach($units as $unit){
		if(isset($unit['4']) && $unit['4'] !== ''){
			$list .= '<li><a href="'.$unit['4'].'" target="_blank">'.$unit['2'].'</a></li>';
		}else{
			$list .= '<li>'.$unit['2'].' (нет URL)</li>';
		}
	}
	$list .= '</ul>';
	return $list;
}

public function buildUnitsTree($user, $root = 'no', $trunk = ''){
	$branches = $this->getUserUnitList($user);
	$oldvalues = $this->getUserMarks($user);
	$j=0;
	$table ='';
	if($root==='yes') {
		$tree = '<ul id="nav_menu_content">';
	}else {
		$tree = '<ul>';
	}
	foreach ($branches as $branch) {
	if($branch['1']===$trunk){
		$tree .= '<li class="'.$this->whichStyle($branch['0'], $user).'">';
			$tree .= '
<div class="popup" id="window-popup-'.$branch['0'].'" style="display:none">
<div class="popup-content">
<h2>'.$branch['2'].'</h2>
<table class="vote">
<tr><td class="text">
<b>Пожалуйста, оцените административное подразделение</b>
<i> <span style="color:red">(1-самая низкая оценка, 5-наивысшая):</i></td>
<td class="mark">1</td><td class="mark">2</td><td class="mark">3</td><td class="mark">4</td><td class="mark">5</td></tr>';

$markfields = array(
2 => 'Выполнение правил внутреннего трудового распорядка',
3 => 'Компетентность сотрудников',
4 => 'Оперативность рассмотрения обращений',
5 => 'Доброжелательность и вежливость сотрудников',
6 => 'Оценка качества методических материалов',
7 => 'Использование информационных электронных ресурсов');

			foreach ($markfields as $key => $value) {
				$str = '<tr><td class="text">'.$value.'</td>';
				for($i=1;$i<=5;$i++){
					$str.='<td class="mark"><input type="radio" name="par-'.$key.'_'.$branch['0'].'" title = "'.$i.'" value="'.$i.'" '.$oldvalues[$branch['0']]['par-'.$key][$i].'></td>';
				}
				$str.='</tr>';
				$table .= $str;
			}
			$tree .= $table.'</table>
<a id="'.$branch['0'].'" onclick="changeclass(id)" href="javascript:PopUpHide('.$branch['0'].')" class="close" style="color:red;">Закрыть окно</a>
</div>
</div>
<div class="hv">
<input type="radio" name="par-1_'.$branch['0'].'" value="1" title = "1. Никогда не взаимодействую" id="'.$branch['0'].'" onclick="changeclass(id)"   '.$oldvalues[$branch['0']]['par-1'][1].'>
<input type="radio" name="par-1_'.$branch['0'].'" value="2" title = "2. Крайне редкие" onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][2].'>
<input type="radio" name="par-1_'.$branch['0'].'" value="3" title = "3. По мере необходимости" onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][3].'>
<input type="radio" name="par-1_'.$branch['0'].'" value="4" title = "4. Еженедельные контакты" onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][4].'>
<span title = "5. Ежедневные контакты">
<input type="radio" name="par-1_'.$branch['0'].'" value="5"
 onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][5].'>
</span>
</div>';
			$table = '';
		$childtree = $this->buildUnitsTree($user, 'no', $branch['0']);
		$open ='';
		if($childtree!=null){
			$open = '- <span style="color:red;text-decoration:underline;">развернуть/свернуть</span>';
		}
		if(isset($branch['4']) && $branch['4'] !== ''){
			$url = '<a href="#" onclick="window.open(\''.$branch['4'].'\')" style="color:green;margin-left:-10px;">WWW</a>';
		}else{
			$url = '';
		}
		$tree .= $url.'<a href="#">'.$branch['2'].' '.$open.'</a>';
		$tree .= $childtree;
		$tree .= '</li>';
		$j++;
	}
}
$tree .= '</ul>';
if($j>0){
	return $tree;
}else{
	return null;
}
}

protected function updateUserDoc($object,$newdata){
	$this->db->colUnitRate->update($object, $newdata);
}

protected function whichStyle($id, $user){
	$a = $this->hasChild($id,$user);
	if(count($a)>=1){
		foreach($a as $docid){
			if(!$this->isUnitFilled($docid, $user)){
				return 'mistake_style';
			}
		}
	}
	if(!$this->isUnitFilled($id, $user)){
		return 'mistake_style';
	}else{
		return 'ok_style';
	}
}

protected function isUnitFilled($id, $user){
	$doc = $this->getUserDoc($user);
	if(isset($doc['data'][$id]['par-1'])){
		if((int)$doc['data'][$id]['par-1']===1){
			return true;
		}
		if(	$doc['data'][$id]['par-1']>1 and
			$doc['data'][$id]['par-2']>0 and
			$doc['data'][$id]['par-3']>0 and
			$doc['data'][$id]['par-4']>0 and
			$doc['data'][$id]['par-5']>0 and
			$doc['data'][$id]['par-6']>0 and
			$doc['data'][$id]['par-7']>0){
			return true;
		}
	}
	return false;
}

protected function hasChild($pid,$user){
$cursor = $this->getUnitsByUserType($user);
$childId = array();
foreach ($cursor as $doc) {
    if($doc['pid']===$pid){
        $childId[] = $doc['id'];
        $grandChildIds = $this->hasChild($doc['id'],$user);
        $childId = array_merge($childId, $grandChildIds);
    }
}
return $childId;
}

protected function getParams(){
	$params = $this->db->colModel->findOne(array('type' => 'aup'));
	unset($params['_id'],$params['type']);
	return $params;
}

protected function getUserMarks($user){
	$array = array();
	$cursor = $this->getUnitsByUserType($user);
	$docA = $this->getUserDoc($user);
	if(is_array($docA)){
		foreach($cursor as $unit){
			for($i=1;$i<=7;$i++){
				for($j=1;$j<=5;$j++){
//					$array[$unit['id']]['par-'.$i][$j]='';
					if(isset($docA['data'][$unit['id']]['par-'.$i]) && $docA['data'][$unit['id']]['par-'.$i]==$j){
						$array[$unit['id']]['par-'.$i][$j] = 'checked';
					}else{
						$array[$unit['id']]['par-'.$i][$j] = '';
					}
				}
			}
		}
	}
return $array;
}

protected function getUserDoc($user){
	$form = $this->db->colUnitRate->findOne(array("uid" => $user->uuid));
	if(is_null($form)){
		$this->zeroArray($user);
		$form = $this->db->colUnitRate->findOne(array("uid" => $user->uuid));
	}
return $form;
}

protected function getUnitsByUserType($user){
	$status = $this->getUserType($user);
	$cursor = $this->db->colUnits->find( array('get.'.$status => true));
return $cursor;
}

protected function getUserUnitList($user){
	$i = 0;
	$cursor = $this->getUnitsByUserType($user);
	foreach($cursor as $doc){
		if(!isset($doc["url"])){$doc["url"]='';}
		$numList[$i]= array($doc["id"],$doc["pid"],$doc["name"],$doc["get"],$doc["url"]);
		$i++;
	}
return $numList;
}

protected function getUserType($user){
	require_once("lib/frole.php");
	$role = new Role();
	$my_roles = $role->getRoles($user->uuid);
	$status = '';
	foreach($my_roles as $value){
		if($value === 'dean'){
			$status = 'dean';
		} elseif($value === 'student'){
			$status = 'student';
		}
	}

	if (!$status){
		die('Внимание! Произошла ошибка получения статуса пользователя! ');
	}
	return $status;
}

protected function zeroArray($user){
	$cursor = $this->getUnitsByUserType($user);

	$status = $this->getUserType($user);

	$a = array( 'uid' => $user->uuid,
				'status' => $status,
				'data' => '',
				'confirm'=> false);
	foreach ($cursor as $doc){
		$a['data'][$doc['id']] =  array('par-1'=>0.0,
										'par-2'=>0.0,
										'par-3'=>0.0,
										'par-4'=>0.0,
										'par-5'=>0.0,
										'par-6'=>0.0,
										'par-7'=>0.0);
	}
	$this->db->colUnitRate->insert($a);
}

function getPercent($value, $paramName){
	$units = $this->db->colUnits->find();
	$cDatas = $this->db->colUnitRate->find();
	foreach($units as $unit){
		foreach($cDatas as $cData){
			$array[$unit['id']][] = $cData['data'][$unit['id']][$paramName];
		}
		$array[$unit['id']] = array_diff($array[$unit['id']], array(0));
		$avg[$unit['id']] = (float)array_sum($array[$unit['id']])/count($array[$unit['id']]);
	}

	sort($avg);

	$count = count($avg);
	if($count % 2 != 0){
		$medianIndex = floor($count/2);
		$medianValue = $avg[$medianIndex];
	}else{
		$medianIndex = $count/2;
		$one = $avg[$medianIndex-1];
		$two = $avg[$medianIndex];
		$medianValue = ($one + $two) / 2;
	}
	if($value == $medianValue){ return 50;}

	if($value < $medianValue){
		$min = $avg[0];
		$max = $medianValue;
		$delta = $max - $min;
		if($delta != 0){ return 50 * ($value - $min) / $delta; }else{ return 0; }
    }

	if($value > $medianValue){
		$min = $medianValue;
		$max = $avg[$count - 1];
		$delta = $max - $min;
		if($delta != 0){ return 50 + 50 * ($value - $min) / $delta;	}else{ return 0; }
	}
}

function makePercent($avg,$min,$max){
		$delta = $max-$min;
		if($delta!=0){$percent = 100*($avg-$min)/$delta;}
		return $percent;
}

function getMinMax(){
	$units = $this->db->colUnits->find();
	for($i=1; $i<=7; $i++){
	$minmax['par-'.$i]['min']=5;
	$minmax['par-'.$i]['min-name']=0;
	$minmax['par-'.$i]['max']=0;
	$minmax['par-'.$i]['max-name']=0;
	}
	foreach($units as $unit){
		$unitmarks = $this->getUnitMarks($unit['id']);
		for($i=1; $i<=7; $i++){
			if($unitmarks['par-'.$i]['avg'] > $minmax['par-'.$i]['max']){
				$minmax['par-'.$i]['max'] = $unitmarks['par-'.$i]['avg'];
				$minmax['par-'.$i]['max-name'] = $unit['name'];
				$minmax['par-'.$i]['max-id']=$unit['id'];
			}
			if($unitmarks['par-'.$i]['avg'] < $minmax['par-'.$i]['min']){
				$minmax['par-'.$i]['min'] = $unitmarks['par-'.$i]['avg'];
				$minmax['par-'.$i]['min-name'] = $unit['name'];
				$minmax['par-'.$i]['min-id']=$unit['id'];
			}
		}
	}
	return $minmax;
}

function getAllMarks(){
	$units = $this->db->colUnits->find();
	for($i=1;$i<=7;$i++){$allMarks['par-'.$i] = array(0 => 0, 1 => 0, 2 => 0, 3 => 0, 4=>0, 5=>0,'med'=>0,'avg'=>0,'test'=>0,'sum'=>0);}
	$j=0;
	foreach($units as $unit){
		$unitmarks = $this->getUnitMarks($unit['id']);
		++$j;
		foreach($unitmarks as $key => $value){
			foreach($value as $key2 => $value2){
				$allMarks[$key][$key2] +=$value2;
			}
		}
	}
for($i=1;$i<=7;$i++){
	$allMarks['par-'.$i]['avg']=round($allMarks['par-'.$i]['sum']/$allMarks['par-'.$i]['test'],3);
	$nomer = round($allMarks['par-'.$i]['test']/2);
for($l=1;$l<=5;$l++){if($allMarks['par-'.$i][$l]<$nomer){$nomer=$nomer-$allMarks['par-'.$i][$l];}else{$allMarks['par-'.$i]['med']=$l;break;}}
	}
return($allMarks);
}

function getUnitMarks($id){
	for($i=1;$i<=7;$i++){
		for($j=0;$j<=5;$j++){ $marks['par-'.$i][$j]=0;}
		$marks['par-'.$i]['test']=0;
		$marks['par-'.$i]['sum']=0;
		$marks['par-'.$i]['avg']=0;
		$marks['par-'.$i]['med']=0;
	}
	foreach($this->db->colUnitRate->find() as $rating){
		if(isset($rating['objects']) and !is_array($rating['objects'])){
			$rating['objects'] = array($rating['objects']);
		}
		if(isset($rating['objects'][0]) and strlen($rating['objects'][0])==36){
			for($j=1;$j<=7;$j++){
				for($i=0;$i<=5;$i++){
					if($rating['data'][$id]['par-'.$j] == $i){
						$marks['par-'.$j][$i]++;
						if($i!=0){$marks['par-'.$j]['test']++;}
						$marks['par-'.$j]['sum'] +=$rating['data'][$id]['par-'.$j];
					}
				}
				if($marks['par-'.$j]['test']>0){$marks['par-'.$j]['avg']=round($marks['par-'.$j]['sum']/$marks['par-'.$j]['test'],3);}
			}
		}
	}
	for($k=1;$k<=7;$k++){
		$nomer = round($marks['par-'.$k]['test']/2);
		for($l=1;$l<=5;$l++){if($marks['par-'.$k][$l]<$nomer){$nomer=$nomer-$marks['par-'.$k][$l];}else{$marks['par-'.$k]['med']=$l;break;}}
	}
return $marks;
}

function checkForm($chairId){
	$rating = $this->db->colUnitRate->findOne(array('objects'=>$chairId));
	$units = $this->db->colUnits->find();
	foreach($units as $unit){
		$check = false;
		if($rating['data'][$unit['id']]['par-1']>1){
			if(	$rating['data'][$unit['id']]['par-2']>0 and
				$rating['data'][$unit['id']]['par-3']>0 and
				$rating['data'][$unit['id']]['par-4']>0 and
				$rating['data'][$unit['id']]['par-5']>0 and
				$rating['data'][$unit['id']]['par-6']>0 and
				$rating['data'][$unit['id']]['par-7']>0){
				$check = true;
			}
		}
		if($rating['data'][$unit['id']]['par-1']>1){
			$check = true;
		}
	}
		return $check;	// true => все ОК
}

function getChairsId($uid){        //возвращает список uuid кафедр для текущего пользователя
$cursor = $this->collectionRules->find(array("actors"=>$uid));
$values=array();
foreach ($cursor as $doc) {
  if(is_array($doc['objects'])){
    foreach ($doc['objects'] as $value) {
      if(strlen($value)>30){
       $values[] = $value;
     }
    }
  }
}
  if(count($values)>0){
 // $newdata = array('$set'=> array('objects'=>$values));
//  $this->my_dump($values);
//  $this->db->colUnitRate->update(array('uid'=> $uid),$newdata);
  }
 return $values;

}

function buildTree($objects,$oldvalues,$root='no',$branches, $trunk=''){
$j=0;
$table ='';
if($root=='yes') {$tree = '<ul id="nav_menu_content">';}else {$tree = '<ul>';}
foreach ($branches as $branch) {
	if($branch['1']==$trunk){
		$tree .= '<li class="'.$this->whichstyle($objects,$branch['0'],$branch['3']).'" id="'.$branch['0'].'">';
		if($branch['3']=='yes'){
			$tree .= '
<div class="popup" id="window-popup-'.$branch['0'].'" style="display:none">
<div class="popup-content">
<h2>'.$branch['2'].'</h2>
<table class="vote">
<tr><td class="text"><b>Пожалуйста, оцените административное подразделение</b><i> <span style="color:red">(1-самая низкая оценка, 5-наивысшая):</i></td><td class="mark">1</td><td class="mark">2</td><td class="mark">3</td><td class="mark">4</td><td class="mark">5</td></tr>';

$markfields = array(
2 => 'Выполнение правил внутреннего трудового распорядка',
3 => 'Компетентность и квалификация сотрудников',
4 => 'Оперативность рассмотрения обращений',
5 => 'Культура общения сотрудников',
6 => 'Оценка качества методических материалов на странице подразделения официального сайта университета (herzen.spb.ru)',
7 => 'Использование информационных электронных ресурсов (корпоративная электронная почта, массовые рассылки, вебинары (meet.herzen.spb.ru))');



			foreach ($markfields as $key => $value) {
				$str = '<tr><td class="text">'.$value.'</td>';
				for($i=1;$i<=5;$i++){
					$str.='<td class="mark"><input type="radio" name="par-'.$key.'_'.$branch['0'].'" title = "'.$i.'" value="'.$i.'" '.$oldvalues[$branch['0']]['par-'.$key][$i].'></td>';
				}
				$str.='</tr>';
				$table .= $str;
			}
			$tree .= $table.'</table>
<a id="'.$branch['0'].'" onclick="changeclass(id)" href="javascript:PopUpHide('.$branch['0'].')" class="close" style="color:red;">Закрыть окно</a>
</div>
</div>
<div class="hv">
<input id="'.$branch['0'].'" onclick="changeclass(id)" type="radio" name="par-1_'.$branch['0'].'" value="1" title = "1. Никогда не взаимодействую"  '.$oldvalues[$branch['0']]['par-1'][1].'>
<input type="radio" name="par-1_'.$branch['0'].'" value="2" title = "2. Крайне редкие" onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][2].'>
<input type="radio" name="par-1_'.$branch['0'].'" value="3" title = "3. По мере необходимости" onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][3].'>
<input type="radio" name="par-1_'.$branch['0'].'" value="4" title = "4. Еженедельные контакты" onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][4].'>
<span title = "5. Ежедневные контакты">
<input type="radio" name="par-1_'.$branch['0'].'" value="5"
 onclick="javascript:PopUpShow('.$branch['0'].')" '.$oldvalues[$branch['0']]['par-1'][5].'>
</span>
</div>';
			$table = '';
		}
		$childtree = $this->buildTree($objects,$oldvalues,'no',$branches, $branch['0']);
		$open ='';
		if($childtree!=null){
			$open = '- <span style="color:red;text-decoration:underline;">развернуть/свернуть</span>';
		}
		$tree .= '<a href="#">'.$branch['2'].' '.$open.'</a>';
		$tree .= $childtree;
		$tree .= '</li>';
		$j++;
	}
}
$tree .= '</ul>';
if($j>0){
	return $tree;
}else{
	return null;
}
}



function confirmMessage($chairId){
  $objects = $this->db->colUnitRate->findOne(array('objects'=>$chairId));
            $newdata = array('$set'=> array('confirm'=>'yes'));
            $this->db->colUnitRate->update(array('objects'=>$chairId),$newdata);

 echo "<h2>Спасибо за участие в анкетировании.</h2>";
}

function showrating(){
  $i=0;
  $votes = $this->db->colUnitRate->count(array('confirm'=>'yes'));
$table = '
<table class="rating">
<tr><th colspan="43">Всего учтено '.$votes.' проголосовавших</th></tr>
<tr>
<td></td>
<td style="width:110px;" colspan="7" title="Частота взаимодействия с отделом">Частота</td>
<td style="width:110px;" colspan="7" title="Приветливость и вежливость персонала">Вежливость</td>
<td style="width:110px;" colspan="7" title="Качество консультирования в сфере деятельности подразделения">Качество</td>
<td style="width:110px;" colspan="7" title="Исполнение правил внутреннего распорядка">Исполение</td>
<td style="width:110px;" colspan="7" title="Проработанность и доступность рабочих материалов (бланков, форм, инструкций)">Мателиалы</td>
<td style="width:110px;" colspan="7" title="Оперативность обработки запросов и выдачи справочной информации">Оперативность</td>
</tr>
<tr class="helpstr">
<td><b>Название отдела</b></td>
<td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>сум.</td><td>ср.</td>
<td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>сум.</td><td>ср.</td>
<td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>сум.</td><td>ср.</td>
<td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>сум.</td><td>ср.</td>
<td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>сум.</td><td>ср.</td>
<td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>сум.</td><td>ср.</td>
</tr>';

$cursor=$this->db->colUnits->find();

$allParams = array();
foreach ($cursor as $doc) {

  $params = $this->db->colUnitRate->aggregate(
    array('$match'   => array(
        'confirm'      => 'yes',
      )),
    array('$project'  => array(
        '_id'           => 0,
        'param-1'       => '$data.'.$doc['id'].'.par-1',
        'param-3'       => '$data.'.$doc['id'].'.par-3',
        'param-4'       => '$data.'.$doc['id'].'.par-4',
        'param-5'       => '$data.'.$doc['id'].'.par-5',
        'param-6'       => '$data.'.$doc['id'].'.par-6',
        'param-7'       => '$data.'.$doc['id'].'.par-7'
      )),
    array('$group'      => array(
        '_id'           => $doc['id'],

        'chastotaS'     => array('$sum'=>'$param-1'),
        'privetlivostS' => array('$sum'=>'$param-3'),
        'kachestvoS'    => array('$sum'=>'$param-4'),
        'ispolnenieS'   => array('$sum'=>'$param-5'),
        'dostupnostS'   => array('$sum'=>'$param-6'),
        'operativnostS' => array('$sum'=>'$param-7'),

        'chastotaM'     => array('$avg'=>'$param-1'),
        'privetlivostM' => array('$avg'=>'$param-3'),
        'kachestvoM'    => array('$avg'=>'$param-4'),
        'ispolnenieM'   => array('$avg'=>'$param-5'),
        'dostupnostM'   => array('$avg'=>'$param-6'),
        'operativnostM' => array('$avg'=>'$param-7')
      ))
    );
$children = $this->testchild($doc['id']);
  $dopParams = array(
      'name'  =>$doc['name'],
      'get'   =>$doc['get'],
      'pid'   =>$doc['pid'],
      'children'=>$children,
'chastota1'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-1' => 1)),
'chastota2'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-1' => 2)),
'chastota3'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-1' => 3)),
'chastota4'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-1' => 4)),
'chastota5'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-1' => 5)),

'privetlivost1'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-3' => 1)),
'privetlivost2'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-3' => 2)),
'privetlivost3'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-3' => 3)),
'privetlivost4'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-3' => 4)),
'privetlivost5'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-3' => 5)),

'kachestvo1'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-4' => 1)),
'kachestvo2'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-4' => 2)),
'kachestvo3'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-4' => 3)),
'kachestvo4'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-4' => 4)),
'kachestvo5'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-4' => 5)),

'ispolnenie1'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-5' => 1)),
'ispolnenie2'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-5' => 2)),
'ispolnenie3'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-5' => 3)),
'ispolnenie4'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-5' => 4)),
'ispolnenie5'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-5' => 5)),

'dostupnost1'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-6' => 1)),
'dostupnost2'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-6' => 2)),
'dostupnost3'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-6' => 3)),
'dostupnost4'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-6' => 4)),
'dostupnost5'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-6' => 5)),

'operativnost1'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-7' => 1)),
'operativnost2'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-7' => 2)),
'operativnost3'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-7' => 3)),
'operativnost4'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-7' => 4)),
'operativnost5'=> $this->db->colUnitRate->count(array('confirm'=>'yes','data.'.$doc['id'].'.par-7' => 5)),
      );
  $allParams[$doc['id']] = array_merge($params['result'][0],$dopParams);

}

foreach ($allParams as $value) {
  $sqr=" ";
  if($value['get']=='yes') $textcolor='green';
  if(count($value['children'])>0) $textcolor = 'red';
  if(count($value['children'])>0 and $value['get']=='yes') {
            $textcolor = 'orange';
            $sqr=" ^ ";
        }
      if($value['pid']==''){
      $table .= '
        <tr>
        <td class="unitname" style="color:'.$textcolor.'">'.$value['name'].'</td>

        <td style="color:'.$textcolor.';">'.$value['chastota1'].'</td>
        <td style="color:'.$textcolor.';">'.$value['chastota2'].'</td>
        <td style="color:'.$textcolor.';">'.$value['chastota3'].'</td>
        <td style="color:'.$textcolor.';">'.$value['chastota4'].'</td>
        <td style="color:'.$textcolor.';">'.$value['chastota5'].'</td>
        <td style="color:'.$textcolor.';">'.$value['chastotaS'].'</td>
        <td style="color:'.$textcolor.';">'.round($value['chastotaM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$value['privetlivost1'].'</td>
        <td style="color:'.$textcolor.';">'.$value['privetlivost2'].'</td>
        <td style="color:'.$textcolor.';">'.$value['privetlivost3'].'</td>
        <td style="color:'.$textcolor.';">'.$value['privetlivost4'].'</td>
        <td style="color:'.$textcolor.';">'.$value['privetlivost5'].'</td>
        <td style="color:'.$textcolor.';">'.$value['privetlivostS'].'</td>
        <td style="color:'.$textcolor.';">'.round($value['privetlivostM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$value['kachestvo1'].'</td>
        <td style="color:'.$textcolor.';">'.$value['kachestvo2'].'</td>
        <td style="color:'.$textcolor.';">'.$value['kachestvo3'].'</td>
        <td style="color:'.$textcolor.';">'.$value['kachestvo4'].'</td>
        <td style="color:'.$textcolor.';">'.$value['kachestvo5'].'</td>
        <td style="color:'.$textcolor.';">'.$value['kachestvoS'].'</td>
        <td style="color:'.$textcolor.';">'.round($value['kachestvoM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$value['ispolnenie1'].'</td>
        <td style="color:'.$textcolor.';">'.$value['ispolnenie2'].'</td>
        <td style="color:'.$textcolor.';">'.$value['ispolnenie3'].'</td>
        <td style="color:'.$textcolor.';">'.$value['ispolnenie4'].'</td>
        <td style="color:'.$textcolor.';">'.$value['ispolnenie5'].'</td>
        <td style="color:'.$textcolor.';">'.$value['ispolnenieS'].'</td>
        <td style="color:'.$textcolor.';">'.round($value['ispolnenieM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$value['dostupnost1'].'</td>
        <td style="color:'.$textcolor.';">'.$value['dostupnost2'].'</td>
        <td style="color:'.$textcolor.';">'.$value['dostupnost3'].'</td>
        <td style="color:'.$textcolor.';">'.$value['dostupnost4'].'</td>
        <td style="color:'.$textcolor.';">'.$value['dostupnost5'].'</td>
        <td style="color:'.$textcolor.';">'.$value['dostupnostS'].'</td>
        <td style="color:'.$textcolor.';">'.round($value['dostupnostM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$value['operativnost1'].'</td>
        <td style="color:'.$textcolor.';">'.$value['operativnost2'].'</td>
        <td style="color:'.$textcolor.';">'.$value['operativnost3'].'</td>
        <td style="color:'.$textcolor.';">'.$value['operativnost4'].'</td>
        <td style="color:'.$textcolor.';">'.$value['operativnost5'].'</td>
        <td style="color:'.$textcolor.';">'.$value['operativnostS'].'</td>
        <td style="color:'.$textcolor.';">'.round($value['operativnostM'],2).'</td>
        </tr>';
              if(isset($value['children']) and count($value['children'])>0){
           //     $this->my_dump($value['children']);
                      foreach ($value['children'] as $id) {
                         if($allParams[$id]['get']=='yes') $textcolor='green';
                        if(count($allParams[$id]['children'])>0) $textcolor = 'red';
                        if(count($allParams[$id]['children'])>0 and $allParams[$id]['get']=='yes') $textcolor = 'orange';
                  $table .= '
        <tr>
        <td class="unitname" style="color:'.$textcolor.'"> &nbsp; ^ '.$sqr. $allParams[$id]['name'].'</td>

        <td style="color:'.$textcolor.';">'.$allParams[$id]['chastota1'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['chastota2'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['chastota3'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['chastota4'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['chastota5'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['chastotaS'].'</td>
        <td style="color:'.$textcolor.';">'.round($allParams[$id]['chastotaM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$allParams[$id]['privetlivost1'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['privetlivost2'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['privetlivost3'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['privetlivost4'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['privetlivost5'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['privetlivostS'].'</td>
        <td style="color:'.$textcolor.';">'.round($allParams[$id]['privetlivostM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$allParams[$id]['kachestvo1'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['kachestvo2'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['kachestvo3'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['kachestvo4'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['kachestvo5'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['kachestvoS'].'</td>
        <td style="color:'.$textcolor.';">'.round($allParams[$id]['kachestvoM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$allParams[$id]['ispolnenie1'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['ispolnenie2'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['ispolnenie3'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['ispolnenie4'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['ispolnenie5'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['ispolnenieS'].'</td>
        <td style="color:'.$textcolor.';">'.round($allParams[$id]['ispolnenieM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$allParams[$id]['dostupnost1'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['dostupnost2'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['dostupnost3'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['dostupnost4'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['dostupnost5'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['dostupnostS'].'</td>
        <td style="color:'.$textcolor.';">'.round($allParams[$id]['dostupnostM'],2).'</td>

        <td style="color:'.$textcolor.';">'.$allParams[$id]['operativnost1'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['operativnost2'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['operativnost3'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['operativnost4'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['operativnost5'].'</td>
        <td style="color:'.$textcolor.';">'.$allParams[$id]['operativnostS'].'</td>
        <td style="color:'.$textcolor.';">'.round($allParams[$id]['operativnostM'],2).'</td>
        </tr>';
                      }
              }
      }


}
$table .= '</table>';
return $table;
}

} // end class
