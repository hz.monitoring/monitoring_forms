<?php
/*
 * 
 * Messages class for ComplexRating scripts.
 * 
 * @author Deviatkov Aleksandr <web@melior.biz>
 * @author Tagunov Tikhon <niptuckker@gmail.com>
 * 
 */
class Messages{
function __construct(){
  	require_once('fconnect.php');
	$this->db = DBConnect::getInstance();
  
}

function testConfirm($userID){
	$confirmed = true;
	$messages = $this->db->colMessages->find(array('user' => $userID, 'closed' => '','enable'=>array('$ne'=>'')));
	foreach($messages as $message){
		$periods = array();
		foreach($message as $key => $value){
			if(is_int($key) && $value == 'on'){
				$periods[] = $key;
			}
		}
		if(count($periods)>0){
			$chair = $this->db->colDocs->findOne(array('uuid'=>$message['chair']));
			foreach($periods as $period){
				if($chair['versions'][$period]['content']['confirmed']<>1){
					$confirmed = false;
				}
			}
			if($confirmed == true){
				$dataset = array('$set'=>array('closed'=>Date("Ymd")));
				$docID = array('_id' =>  new MongoId($message['_id']));
				$this->db->colMessages->update($docID, $dataset);
			}
		}
	}
	
}
	
function moderateMessages($array){
 foreach($array as $key => $value){
  $messageID = array('_id' =>  new MongoId($key));
  if($value=='delete'){
   $this->db->colMessages->remove($messageID);
  }
  if($value=='approve'){
	$dataset = array('$set'=>array('enable'=>Date("Ymd")));
    $this->db->colMessages->update($messageID,$dataset);
    $message = $this->db->colMessages->findOne($messageID);
    $doc = $this->db->colDocs->findOne(array('uuid' => $message['chair']));
    $docID = array('_id' =>  new MongoId($doc['_id']));
    foreach($message as $keyM => $valueM){
	  if(is_int($keyM) && $valueM == 'on'){
		$dataset = array('$set' => array(
		'versions.'.$keyM.'.content.visited'=>false, 
		'versions.'.$keyM.'.content.confirmed'=>false));
		$this->db->colDocs->update($docID,$dataset);
	  }
	}
  }
 }	
}

function getAdminMessages(){
 $messages = $this->db->colMessages->find();
 $out = '<form method="POST" name="admin"><table class="messagelist"><tr>
 <th>Создано:</th>
 <th>Одобрено:</th>
 <th>Закрыто:</th>
 <th>Кафедра:</th>
 <th>Периоды:</th>
 <th>Одобрить/удалить</th>
 <th>Комментарий:</th>
 </tr>';
 foreach($messages as $message){
  $createDate = $this->getDate($message['create']);
   $class = 'iscreate';
  if((int)$message['enable']>0){$class = 'isenabled';}
  if((int)$message['closed']>0){$class = 'isclosed';}
  if($message['enable']!=''){$enableDate = $this->getDate($message['enable']);}else{$enableDate = ' нет ';}
  if($message['closed']!=''){$closedDate = $this->getDate($message['closed']);}else{$closedDate = ' нет ';}
  $buttons = '<input type="radio" name="'.$message['_id'].'" value="approve" class="green"> или <input type="radio" name="'.$message['_id'].'" value="delete">';
  $out .= '<tr class="'.$class.'">
  <td>'.$createDate.'</td>
  <td>'.$enableDate.'</td>
  <td>'.$closedDate.'</td>
  <td>'.$this->getChairName($message['chair']).'</td>
  <td><table><tr>';
  foreach($message as $key => $value){ //просмотр ключей для поиска выбранных этапов
   if(is_int($key) && $value=='on'){
    $out .= '<td><div>Этап&nbsp;'.++$key.'</div></td>';
   }
  } 
  $out .= '</tr></table></td>
  <td>'.$buttons.'</td>
  <td>'.$message['reason'].'</td>
  </tr>';
 }
 $out .= '
 <tr><th colspan="5"></th><th><input type="submit" name="confirm" value="Применить"></th><th></th>
 </tr>
 </table>
 </form>';
 return $out;
}

function getUserMessages($uid){
$amount = $this->db->colMessages->count(array('user' => $uid));
if($amount>0){
 $cursor = $this->db->colMessages->find(array('user' => $uid));
 $out = '<h4>История ваших заявок</h4>
 <form method="POST" name="delete">
 <table class="messagelist">
 <tr>
 <th>Создано:</th>
 <th>Одобрено:</th>
 <th>Закрыто:</th>
 <th>Кафедра:</th>
 <th>Выбранные периоды:</th>
 <th>Статус:</th>
 <th>Удалить?</th>
 </tr>';
 foreach($cursor as $doc){ //по 1 заявке
  $createDate = $this->getDate($doc['create']);
  if($doc['enable']!=''){$enableDate = $this->getDate($doc['enable']);}else{$enableDate = ' нет ';}
  if($doc['closed']!=''){$closedDate = $this->getDate($doc['closed']);}else{$closedDate = ' нет ';}
  $chairname = $this->getChairName($doc['chair']);
  $deletecheck = '<input type="checkbox" name="'.$doc['_id'].'">';
  $status = 'Ожидание'; 
  $class = 'iscreate';
  if((int)$doc['enable']>0){$status = 'Одобрено';$class = 'isenabled';}
  if((int)$doc['closed']>0){$status = 'Закрыто';$class = 'isclosed';}
  $out .= '<tr class="'.$class.'">
  <td>'.$createDate.'</td>
  <td>'.$enableDate.'</td>
  <td>'.$closedDate.'</td>
  <td>'.$chairname.'</td><td><table><tr>';
  foreach($doc as $key => $value){ //просмотр ключей для поиска выбранных этапов
   if(is_int($key) && $value=='on'){
    $out .= '<td><div>Этап&nbsp;'.++$key.'</div></td>';
   }
  } 
  $out .= '</tr></table></td><td>'.$status.'</td>
  <td class="delete">'.$deletecheck.'</td>
  </tr>';
 }
 $out .= '<tr><th colspan="6"></th><th><input type="submit" name="delete" value="Удалить"></th>
 </tr>
 </table>
 </form>';
}else{
 $out = '<h4>История заявок пуста</h4>'; 
}
 return $out;
}

function getChairName($uuid){
	$out = $this->db->colDocs->findOne(array('uuid'=>$uuid));
	$out = $out['title_ru'];
return $out;	
}

function getDate($string){
	$date = DateTime::createFromFormat('Ymd', $string);
	$goodDate = $date->format('d').'/'.$date->format('m').'/'.$date->format('y').'г.';
	return $goodDate;
}

function deleteMessages($array){
 foreach($array as $key => $value){
  $forDelete = array('_id' =>  new MongoId($key));
  $this->db->colMessages->remove($forDelete);
 }	
 return 'Удаление выделенных заявок выполнено!';
}

function getAvailableChairs($uuid){
 $cursor = $this->db->colRules->find(array("actors"=>$uuid, "actions"=>"+update", "actions"=>"+read", "actions"=>"+display"));	
 $out = "";
 foreach ($cursor as $doc) {
  foreach($doc['objects'] as $chairid){ 
   if (strlen($chairid) == 36){
	$chair = $this->db->colDocs->findOne(array ("uuid" => $chairid));			
	$out .= '<p><input type="radio" name="chair" value="'.$chair["uuid"].'"> - '.$chair["title_ru"].'</p>';
   }  
  }
 }	
 if ($out == "") { $out = "Нет доступных кафедр!"; }
 return $out;
}

function sendRequest($p){
 $out = "";
 $isError = false;	
 $chairSelected = false;
 if(strlen($p['reason']) < 4) {
  $out .= "Ошибка! Пожалуйста, укажите причину создания заявки<br>";
  $isError = true;	
 }
 if(!isset($p['0']) and 
	!isset($p['1']) and 
	!isset($p['2']) and 
	!isset($p['3']) and 
	!isset($p['4']) and 
	!isset($p['5']) and 
	!isset($p['6']) and 
	!isset($p['7'])){
  $out .= "Ошибка! Вы должны выбрать не менее одного периода<br>";
  $isError = true;		
 }
 foreach($p as $key => $value){ 
  if($key=="chair" and strlen($value)==36){	$chairSelected = true; }
 }
 if(!$chairSelected){$out .= "Ошибка! Вы должны выбрать не менее одной кафедры<br>";}
 if($chairSelected and !$isError){
  $p['create'] = Date("Ymd");
  $p['enable'] = '';
  $p['closed'] = '';
  $this->db->colMessages->insert($p);
  $headers = 'From: ROBOT@COMPLEXRATING.HERZEN.SPB.RU' . "\r\n" .
    'Reply-To: complexrating@herzen.spb.ru' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
  mail('adevyatkov@herzen.spb.ru', ' FORMS ... NEW REQUEST ADDED', '<a href="http://forms.herzen.spb.ru/messages_admin.php"> МОДЕРИРОВАТЬ!!!</a>',$headers);
  $out.='<span style="color:red">Заявка успешно отправлена. Она будет рассмотрена в ближайшее время.</span>';	
 }else{
  $out.='	<a href="/messages.php"><button class="btn btn-link">Повторить ввод данных</button></a>';
 }
 return $out;
}

} // end class
