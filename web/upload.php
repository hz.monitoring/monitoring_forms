<?php

$folder =  '/web/forms/www/pdf/';//директория в которую будет загружен файл
$data = array();
$error = true;

$target = isset($_POST["target"]) ? $_POST["target"] : null;

if(isset($_POST['upload'])){
    //Список разрешенных файлов
    $whitelist = array(".pdf", ".png", ".jpg", ".jpeg");

	//Проверяем разрешение файла
    foreach  ($whitelist as  $item) {
		if(preg_match("/$item\$/i",$_FILES['userfile']['name'])) $error = false;
    }

    //если нет ошибок, грузим файл
    if(!$error) {


		//$uploadedFile =  $folder.basename($_FILES['userfile']['name']);
		$uploadedFile =  $folder.basename($_POST['filename1']);

		if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
			if(move_uploaded_file($_FILES['userfile']['tmp_name'],$uploadedFile)){
			//if(true){
		        $data = $_FILES['userfile'];
			}
			else {
				$data['errors'] = "Во время загрузки файла произошла ошибка";
			}
		}
		else {
			$data['errors'] = "Файл не загружен";
		}
    }
    else{

		$data['errors'] = 'Вы загружаете запрещенный тип файла';
    }
   // $data['errors'] = $_POST['filename1'];
}
elseif (isset($_POST["delete-upload"])) {
    $uploadedFile =  $folder.basename($_POST['filename1']);
    if (!file_exists($uploadedFile)) {
        $data['errors'] = 'Вы пытаетесь удалить несуществующий файл. Перестаньте.';
    } else {
        $deleted = true;unlink($uploadedFile);

        if (!$deleted) {
            $data['errors'] = 'При удалении файла произошла ошибка. Попробуйте снова или обратитесь к администратору.';
        } else {
            $data['errors'] = 'Файл удален. Подразделение будет скрыто.';
        }
    }

}
else{
	// die("ERROR");
    $data['errors'] = "Неправильный запрос.";
}

//Формируем js-файл
$res = '<script type="text/javascript">';
$res .= "var data = new Object;";
foreach($data as $key => $value){
    $res .= 'data.'.$key.' = "'.$value.'";';
}
$res .= 'window.parent.handleResponse(data, "' . $target . '");';
$res .= "</script>";

echo $res;
