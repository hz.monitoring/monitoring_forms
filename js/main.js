    $(function(){
        if (window.location.host == "foo.herzen.spb.ru") {
            $("body")
            .addClass("body_site_test")
            .append(function(){
                return $("<div>")
                .addClass("body__test-notification")
                .text("Development site")
            })
        }
    });

    var controls_states = ["controls_state_mistake", "controls_state_process", "controls_state_ok", "controls_state_warning"];
    var controls_states_string = controls_states.join(" ");

    var parseSaveResponse = function(data, status, $xhr, $form) {
        if ($form) {
            $form.find('[type=submit], .field-save-handler').prop("disabled", false);
        }


        if (data) {
            var $output = $form.find(".form__output");
            if ($output.is(":visible")) {
                $output
                .hide()
                .empty();
            }



            $output
            .append(function(){
                return $("<div>")
                .addClass(data.success ? "form__result" : "form__error")
                .text(data.success ? data.resultMessage : data.errorMessage)
                // .delay(3000)
                // .animate({"opacity": "0"}, 5000, function(){
                //     $(this).remove();
                // })
            })
            .fadeIn("slow")



            if (data.success) {
                if (data.id) {
                    pageId = data.id.toString();
                    $form.find("[name=id]").val(pageId);

                    $(".if-exists").css({"visibility": "visible"});
                }
            } else {
            }

            if (data.updatedFields) {
                $.each(data.updatedFields, function(i, v) {
                    $("[name=" + v + "]")
                    .closest(".field-wrapper")
                    .removeClass(controls_states_string)
                    .addClass("controls_state_updated");
                });
            }
            if (data.successFields) {
                $.each(data.successFields, function(i, v) {
                    $("[name=" + v + "]")
                    .closest(".field-wrapper")
                    .removeClass(controls_states_string)
                    .addClass("controls_state_ok");
                });
            }
            if (data.mistakeFields) {
                $.each(data.mistakeFields, function(i, v) {
                    $("[name=" + v + "]")
                    .closest(".field-wrapper")
                    .removeClass(controls_states_string)
                    .addClass("controls_state_mistake");
                });
            }
            if (data.warningFields) {
                $.each(data.warningFields, function(i, v) {
                    $("[name=" + v + "]")
                    .closest(".field-wrapper")
                    .removeClass(controls_states_string)
                    .addClass("controls_state_warning");
                });
            }
            // if (data.validResults) {
            //     $.each(data.validResults, function(i, v) {
            //         $("[name=" + v + "]")
            //         .closest(".field-wrapper")
            //         .removeClass(controls_states_string)
            //         .addClass("controls_state_warning");
            //     });
            // }
            // $validResults
        }
    };

    var deleteAttachment = function(fieldName, elem) {
        if (window.confirm('Удалить файл окончательно?')) {
            var args = {};
            $.post("/service.php?lang=" + language + '&page_type=' + pageType + '&subject=attachment&action=delete&type=' + fieldName + '&id=' + pageId, args, function(data) {
                if (data && data.success) {
                    alert("removed");
                    $(elem).remove();
                }
            });
        }
    };

    var deletePage = function() {
        if (pageId) {
            if (window.confirm('Удалить страницу окончательно (#'+pageId+')?')) {
                var args = {};
                args['id'] = pageId;
                $.post("/service.php?lang=" + language + '&page_type=' + pageType + '&subject=page&action=delete', args, function(data) {


                    if (data && data.success) {
                        alert("removed");
                        window.location = "/";
                    }
                });
            }
        }
    };

    var viewPage = function(fieldName) {
        if (pageId) {
            var win=window.open("/view.php?id=" + pageId, '_blank');
            win.focus();
        }
    }

    var fieldSaveHandler = function(fieldName) {
        var $field = $("[name=" + fieldName + "]");

        var $form = $field.closest("form");

        var $controls = $field.closest(".field-wrapper");
        $controls
        .removeClass(controls_states_string)
        .addClass("controls_state_process")

        var $elements = $form.find("[type=hidden], [name=" + fieldName + "]")

        var args = $.param($elements);
        var url = $form.attr("action");
        url = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'save_field=' + fieldName;
        $.ajax({
            url: url,
            dataType: 'json',
            type: $form.attr("method"),
            data: args,
            success: function(data, textStatus, xhr) {
                parseSaveResponse(data, textStatus, xhr, $form);

                $controls
                .removeClass("controls_state_process")
            }
        });
    }

    var fieldMultipleRearrangeCallback = function($fieldWrappers) {

        $fieldWrappers.find(".control-label-content-wrapper").each(function(i, e) {
            if (i == 0) {
                $(e).removeClass("control-label-content-wrapper--hidden");
            } else {
                $(e).addClass("control-label-content-wrapper--hidden");
            }
        });

    }

    var confirmForm = function() {
        if (pageId) {

            if (window.confirm("Вы действительно хотите отправить данные?")) {
                var args = {};
                args['id'] = pageId;
                args["confirmed"] = 1;

                $.post("/service.php?lang=" + language + '&page_type=' + pageType + '&subject=page&action=update&save_field=confirmed', args, function(data) {
                    if (data && data.success) {
                        // alert("Confirmed");
                        $(".confirm-alert")
                        .hide()
                        .removeClass("confirm-alert-hidden")
                        .fadeIn("fast")
                    }
                });
            }
        }
    };

    //_______________________________________________________________________________________________________
        var confirmVerForm = function(period) {
        if (pageId) {

            if (window.confirm("Вы действительно хотите отправить данные?")) {
                var args = {};
                args['id'] = pageId;
                args["confirmed"] = 1;

                $.post("/service.php?lang=" + language + '&page_type=' + pageType + '&subject=page&action=edithistory&period=' + period + '&save_field=confirmed', args, function(data) {
                    if (data && data.success) {
                        // alert("Confirmed");
                        $(".confirm-alert")
                        .hide()
                        .removeClass("confirm-alert-hidden")
                        .fadeIn("fast")
                    }
                });
            }
        }
    };

    $(function(){

        $(".form--" + pageType).ajaxForm({
            target: "#" + pageType + "_output"
            , success: parseSaveResponse
            , beforeSubmit: function(arr, $form, options) {
                // The array of form data takes the following form:
                // [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
                 // console.log(arr, $form, options);


                // return false to cancel submit
            }
        });




        $(".field-save-handler").click(function(e){
            var $this = $(this);

            var $field = $this.closest(".field-wrapper").find(".form__field");
            var fieldName = $field.prop("name");
            // console.log(fieldName);

            fieldSaveHandler(fieldName);
        });

        $(".form__field").change(function(e) {
            var $field = $(this);
            var fieldName = $field.prop("name");
            // console.log(fieldName);

            fieldSaveHandler(fieldName);
        });

        $( ".sections" )
        .tabs()
        .removeClass( "ui-widget ui-widget-content ui-corner-all" )

        $( ".sections .steps" )
        .removeClass( "ui-widget-header ui-corner-all" );

        $( ".sections .steps__step" )
        .removeClass( "ui-corner-top" )

        $( ".sections .section" )
        .removeClass( "ui-widget-content ui-corner-bottom" )

        // $( ".sections .steps__step a" )
        // .click(function(){
        //     window.location.hash = $(this).attr("href")
        // })

        //.addClass( "ui-corner-left" );
        // $( ".sections li a" )
        // .removeClass( "ui-corner-top" );
        //.addClass( "ui-corner-left" );

        $(window).scroll(function(e) {
            if (false) {
                var $steps = $(".steps");
                var $sections = $(".sections");
                var topLimit = $sections.offset()["top"];
                var scrollTop = $(window).scrollTop();
                // console.log(scrollTop, $steps.offset(), topLimit, scrollTop - topLimit);
                if (scrollTop > topLimit-20 && $(window).height() >= $steps.height()) {
                    $steps.addClass("steps-fixed");
                } else {
                    $steps.data("topPrev", $steps.css("top"));
                    $steps.removeClass("steps-fixed");
                }
            }
        });

        $(".field-add-element-handler")
        .click(function(){
            var $this = $(this);

            var $fieldGroupWrapper = $this.closest(".form-actions").siblings(".control-group-wrapper");
            var $fieldWrappers = $fieldGroupWrapper.find(".control-group");
            var $fieldWrapper = $fieldWrappers.first();

            var $fieldWrapperCopy = $fieldWrapper.clone(true);
            $fieldWrapperCopy.find(".form__field").val("");

            $fieldGroupWrapper.append($fieldWrapperCopy);

            fieldMultipleRearrangeCallback($fieldGroupWrapper.find(".control-group"));
        })

        $(".field-remove-element-handler")
        .click(function(){
            var $this = $(this);

            var $fieldWrapper = $this.closest(".control-group");

            var $fieldGroupWrapper = $fieldWrapper.parent();
            var $fieldWrappers = $fieldGroupWrapper.find(".control-group");

            if ($fieldWrappers.length > 1) {
                $fieldWrapper.remove();
            }

            fieldMultipleRearrangeCallback($fieldGroupWrapper.find(".control-group"));
        })

        $(".control-group-sortable").sortable({
            placeholder: "ui-state-highlight"
            , start: function(e, ui) {
                ui.placeholder.height(ui.item.height());
            }
            , stop: function(e, ui) {
            }
            , update: function(e, ui) {
                fieldMultipleRearrangeCallback(ui.item.siblings(".control-group").andSelf());
            }
        });

        // var toggleNavigation = function(){
        //     $(this)
        //     .closest(".pages")
        //     .toggleClass("pages_view_line")
        // };

        // $(".pages")
        // .hover(toggleNavigation, toggleNavigation);
    });
