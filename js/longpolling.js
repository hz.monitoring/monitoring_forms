function LongPolling(subId, callback, dataType) {
  arguments.callee.prototype.dataTypes = {
      "xml": "application/xml"
      , "json": "application/json"
      , "jsonp": "application/javascript"
      , "html": "text/html"
      , "text": "text/plain"
  };

  this.etag = 0;
  this.time = null;
  this.callback = callback;
  this.subId = subId;

  this.dataType = this.dataTypes[dataType] ? this.dataTypes[dataType] : (dataType ? dataType : "text/plain");

  // console.log("Long polling");

  arguments.callee.prototype.init = function () {
    var $this = this, xhr;
    if ($this.time === null) {
      $this.time = (new Date()).toUTCString();
      // $this.time = $this.dateToUTCString(new Date());
    }

    if (window.XDomainRequest) {
      // Если IE, запускаем работу чуть позже (из-за бага IE8)
      setTimeout(function () {
        $this.poll_IE($this);
      }, 2000);

    } else {
      // Создает XMLHttpRequest объект
      mcXHR = xhr = new XMLHttpRequest();
      xhr.onreadystatechange = xhr.onload = function () {
        if (4 === xhr.readyState) {

          // Если пришло сообщение
          if (200 === xhr.status && xhr.responseText.length > 0) {

            // Берем Etag и Last-Modified из Header ответа
            $this.etag = xhr.getResponseHeader('Etag');
            $this.time = xhr.getResponseHeader('Last-Modified');

            // Вызываем обработчик сообщения
            $this.callback(xhr.responseText);
          }

          if (xhr.status > 0) {
            // Если ничего не пришло повторяем операцию
            $this.poll($this, xhr);
          }
        }
      };
      xhr.onprogress = function(event) {
          var percentComplete = (event.loaded / event.total)*100;
          // console.log("Progress is " + percentComplete, event);
      }

      // Начинаем long polling
      $this.poll($this, xhr);
    }
  };

  arguments.callee.prototype.poll = function ($this, xhr) {
    var timestamp = (new Date()).getTime(),
      url = '/sub/' + $this.subId + '?v=' + timestamp; // callback=?&
      // timestamp помогает защитить от кеширования в браузерах

    xhr.open('GET', url, true);
    xhr.setRequestHeader("If-None-Match", $this.etag);
    xhr.setRequestHeader("If-Modified-Since", $this.time);

    if ($this.dataType) {
      xhr.setRequestHeader('Content-Type', $this.dataType);
    }

    xhr.send();
  };

  // То же самое что и poll(), только для IE
  arguments.callee.prototype.poll_IE = function ($this) {
    var xhr = new window.XDomainRequest();
    var timestamp = (new Date()).getTime(),
      url = '/sub/' + $this.subId + '?v=' + timestamp; // callback=?&

    xhr.onprogress = function () {};
    xhr.onload = function () {
      $this.callback(xhr.responseText);
      $this.poll_IE($this);
    };
    xhr.onerror = function () {
      $this.poll_IE($this);
    };
    xhr.open('GET', url, true);
    xhr.send();
  };

  arguments.callee.prototype.valueToTwoDigits = function (value) {
    return ((value < 10) ? '0' : '') + value;
  };

  // представление даты в виде UTC
  arguments.callee.prototype.dateToUTCString = function (date) {
    var time = this.valueToTwoDigits(date.getUTCHours())
        + ':' + this.valueToTwoDigits(date.getUTCMinutes())
        + ':' + this.valueToTwoDigits(date.getUTCSeconds());
    // console.log(date);
    return this.days[date.getUTCDay()] + ', '
           + this.valueToTwoDigits(date.getUTCDate()) + ' '
           + this.months[date.getUTCMonth()] + ' '
           + date.getUTCFullYear() + ' ' + time + ' GMT';
  };
}
