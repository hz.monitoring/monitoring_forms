
function hideBtn(uploadid){
    $('#upload-' + uploadid)
    .hide()

    $('#status-' + uploadid)
    .html("Идет загрузка файла")
    .show()
}

function handleResponse(mes, uploadid) {
    $('#upload-' + uploadid)
    .show()

    if (mes.errors != null) {
        $('#status-' + uploadid)
        .html("Возникли ошибки во время загрузки файла: " + mes.errors)
        .show()
    }
    else {
        $('#status-' + uploadid)
        .html("Файл " + mes.name + " загружен")
        .show()
    }
}
