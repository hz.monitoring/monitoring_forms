
    function editItem(obj) {
        $("#dialog-form").dialog("open");

        $(obj).addClass("palettePlaced_selected_true");

        // var propDescriptionText = $(obj).attr('data-description');
        // var propDescription = $.parseJSON(propDescriptionText);

        var propDescriptionText = $(obj).attr('data-properties-list');
        var propDescription = propDescriptionText.split(" ");
        // console.log(propDescription);

        for (var propI in propDescription) {
            // var propValue = propDescription[propKey];
            var propKey = propDescription[propI];
            var propValue = $(obj).attr("data-property-" + propKey);

            var query = "#dialog-form [name='" + propKey + "']";
            var $field = $(query);
            // console.log("field ", propKey, ', value: ', propValue, ', element: ', $field, ', query: ', query);
            if ($field.length > 0) {
                $field.val(propValue);

                // console.log("field: ", $field, propKey, propValue);
            } else {
                $field = $("#dialog-form [name='" + propKey + "[]']"); //checkboxes

                if ($field.length > 0) {
                    $field.attr("checked", !!propValue);
                }
            }
        }

        $("#dialog-form [name=index]").val($(obj).index());
    }


    function updateItem(item, itemDescription) {
        // console.log(item, itemDescription);
        if (itemDescription == null) {
            $(item).remove();
        } else {
            var itemKeys = [];
            for (var itemPropKey in itemDescription) {
                itemKeys.push(itemPropKey);
                var itemPropValue = itemDescription[itemPropKey];
                $(item).attr("data-property-" + itemPropKey, itemPropValue);
            }

            var itemType = itemDescription["type"];
            var itemTypeLabel = itemType ? metatypes[itemType] : "?";
            var itemLabel = itemDescription["label_" + language] ? itemDescription["label_" + language] : "(без названия)";

            // console.log(itemType, itemTypeLabel, itemLabel);

            $(item)
            .addClass('palettePlaced')
            .click(function() {
                return editItem(this);
            })
            .each(function() {
                $(this).find(".palettePlaced__name").text(itemLabel)
                $(this).find(".palettePlaced__type").text(itemTypeLabel)
            })
            .attr('data-properties-list', itemKeys.join(' '))
        }
    }

    $(document).ready(function() {
        $(".form-preview__toggle").click(function(){
            $(this).siblings(".form-preview__content").toggle();
        });

        $("#schema__sortable").sortable({
            placeholder: "ui-state-highlight"
            , start: function(e, ui) {
                ui.item.attr("data-previous-index", ui.item.index());
                // ui.item.attr("data-previous-index", null);
        }
        , stop: function(e, ui) {
            // ui.item.attr("data-previous-index", ui.item.attr("data-start-index"));
            // ui.item.attr("data-start-index", null);
        }
        , update: function(e, ui) {
            var prev_index = ui.item.attr("data-previous-index");
            // console.log(ui.item.index(), prev_index);

            if (ui.item.hasClass("palettePlaced")) { // sort
                var content = {};
                content["index"] = prev_index;
                content["new_index"] = ui.item.index();
                $.post('/admin/admin-service.php?subject=item&action=sort&lang=' + language + '&page_type=' + pageType + '', content, function(response) {
                    if (response) {
                        if (response["success"]) {
                            if (typeof response["item"] != "undefined") {
                                var itemDescription = response["item"];
                                updateItem(ui.item, itemDescription);
                            }
                        } else {
                        }
                    } else {
                    }
                });

            } else { // add new

                var content = {};
                content["index"] = ui.item.index();
                content["type"] = ui.item.attr("data-item-type");
                $.post('/admin/admin-service.php?subject=item&action=add&lang=' + language + '&page_type=' + pageType + '', content, function(response) {
                    if (response) {
                        if (response["success"]) {
                            if (typeof response["item"] != "undefined") {
                                var itemDescription = response["item"];
                                updateItem(ui.item, itemDescription);
                            }
                        } else {
                            ui.item.remove();
                        }
                    } else {
                        ui.item.remove();
                    }
                });
            }
        }
    });

    $("#schema__sortable").disableSelection();
        $(".palette__item").draggable({
            connectToSortable: "#schema__sortable",
            helper: "clone",
            revert: "invalid",
            stop: function(e, ui) {

            }
        });

        $("#dialog-form").dialog({
            autoOpen: false,
            height: 600,
            width: 870,
            modal: true,
            buttons: [{
                "text": "Сохранить"
                , "click": function(e) {
                        $("#dialog-form form [type=submit]").click();

                        // $( "#dialog-form" ).dialog( "close" )
                    }
                , "class": "btn btn-primary"
            }, {
                "text": "Удалить"
                , "click": function(e) {
                        removeItem(e);
                    }
                , "class": "btn"
            }]
            , close: function (event, ui) {
                var currentItemIndex = $("#dialog-form form [name=index]").val();
                $("#schema__sortable .palettePlaced_selected_true").removeClass("palettePlaced_selected_true");
            }
        })
    });


    var saveResponseCallback = function(response) {

        // console.log("HELLO: ", response);
        if (response) {
            if (response["success"]) {
                if (typeof response["item"] != "undefined") {
                    var itemDescription = response["item"];
                    var currentItemIndex = $("#dialog-form form [name=index]").val();
                    updateItem(".palettePlaced:nth(" + currentItemIndex + ")", itemDescription);
                }
            } else {
                alert("Ошибка");
            }
        } else {
            alert("Ответ не получен");
        }
    };

    function removeItem(e){
        var currentItemIndex = $("#dialog-form form [name=index]").val();
        var formContent = $("#dialog-form").find("form").serialize();
        $.post('/admin/admin-service.php?subject=item&action=delete&lang=' + language + '&page_type=' + pageType + '', formContent, function(response) {
            if (response) {
                if (response["success"]) {
                    if (typeof response["item"] != "undefined") {
                        var itemDescription = response["item"];
                        updateItem(".palettePlaced:nth(" + currentItemIndex + ")", itemDescription);

                        $( "#dialog-form" ).dialog( "close" )
                    }
                } else {
                    alert("Ошибка");
                }
            } else {
                alert("Ответ не получен");
            }
        });
    }
