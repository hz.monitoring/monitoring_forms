
    $("#overview-table").ready(function(){
        // return;
        $("#overview-table-top")
        .append(function(){
            var $thead = $("#overview-table thead").clone();

            $thead.find("td").each(function(i,e){
                var cellWidth = $("#overview-table thead td[data-item=" + $(e).attr("data-item") + "]").width();
                $(e).css("width", cellWidth + "px");
            })

            return $thead;
        })
        .css("width", $("#overview-table").width() + "px");

        $("#overview-table-left")
        .append(function(){
            var $thead = $("#overview-table thead").clone();

            $thead
            .find("td:not(.overview-table-cell-fixed-left)")
            .remove()

            $thead
            .find("tr")
            .each(function(i,e){
                var rowI = (i*1+1);
                var rowHeight = $("#overview-table thead tr:nth-child(" + rowI + ")").height();
                $(e).css({
                  "height": rowHeight + "px"
                });
                console.log("Row [" + "#overview-table tbody tr:nth-child(" + rowI + ")" + "] thead height: " + rowHeight + "px");
            })

            var $tbody = $("#overview-table tbody").clone()

            $tbody
            .find("td:not(.overview-table-cell-fixed-left)")
            .remove()

            $tbody
            .find("tr")
            .each(function(i,e){
                var rowI = (i*1+1);
                var rowHeight = $("#overview-table tbody tr:nth-child(" + rowI + ")").height();
                $(e).css({
                  "height": rowHeight + "px"
                });
                console.log("Row [" + "#overview-table tbody tr:nth-child(" + rowI + ")" + "] tbody height: " + rowHeight + "px");
            })

            $thead
            .find("td")
            .each(function(i,e){
                var cellWidth = $("#overview-table thead td[data-item=" + $(e).attr("data-item") + "]").width();
                $(e).css("width", cellWidth + "px");
            })

            $tbody
            .find("td")
            .each(function(i,e){
                var cellWidth = $("#overview-table thead td[data-item=" + $(e).attr("data-item") + "]").width();
                $(e).css("width", cellWidth + "px");
            })

            return [$thead, $tbody];
        })
        .css("height", $("#overview-table").height() + "px");

        $("#overview-table-top-left")
        .append(function(){
            var $thead = $("#overview-table thead").clone();

            $thead
            .find("td:not(.overview-table-cell-fixed-left)")
            .remove()

            $thead
            .find("tr")
            .each(function(i,e){
                var rowI = (i*1+1);
                var rowHeight = $("#overview-table thead tr:nth-child(" + rowI + ")").height();
                $(e).css({
                  "height": rowHeight + "px"
                });
                console.log("Row [" + "#overview-table tbody tr:nth-child(" + rowI + ")" + "] thead height: " + rowHeight + "px");
            })

            $thead
            .find("td")
            .each(function(i,e){
                var cellWidth = $("#overview-table thead td[data-item=" + $(e).attr("data-item") + "]").width();
                $(e).css("width", cellWidth + "px");
            })

            return $thead;
        })
        .css({
          "width": $("#overview-table-left").width() + "px"
          , "height": $("#overview-table-top").width() + "px"
          , "top": $("#overview-table-top").scrollTop() + "px"
          , "left": $("#overview-table-left").scrollLeft() + "px"
        });

        $(window).scroll(function(e) {
            $("#overview-table-top-wrapper")
            .css({
              'top': $(this).scrollTop()
            });
            var topLimit = $("#overview-table").offset()["top"];
            var scrollTop = $(window).scrollTop();
            // console.log(scrollTop, topLimit, scrollTop - topLimit);
            if (scrollTop > topLimit) {
                $("#overview-table-top-wrapper").show();
            } else {
                $("#overview-table-top-wrapper").hide();
            }

            $("#overview-table-left-wrapper")
            .css({
              'left': $(this).scrollLeft()
            });
            var leftLimit = $("#overview-table").offset()["left"];
            var scrollLeft = $(window).scrollLeft();
            console.log(scrollLeft, leftLimit, scrollLeft - leftLimit);
            if (scrollLeft > leftLimit) {
                $("#overview-table-left-wrapper").show();
            } else {
                $("#overview-table-left-wrapper").hide();
            }

            if (scrollLeft > leftLimit && scrollTop > topLimit) {
                $("#overview-table-top-left-wrapper")
                .css({
                  "width": $("#overview-table-left").width() + "px"
                  , "height": $("#overview-table-top").width() + "px"
                  , "top": $("#overview-table-top").scrollTop() + "px"
                  , "left": $("#overview-table-left").scrollLeft() + "px"
                });

                $("#overview-table-top-left-wrapper").show();

            } else {
                $("#overview-table-top-left-wrapper").hide();
            }

        });

    })