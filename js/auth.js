
    var parseAuthResponse = function(data) {
        if (data) {
            if ($("#authbox__output").is(":empty")) {
                $("#authbox__output")
                .hide()
            } else {
                $("#authbox__output")
                .fadeOut("fast")
            }
            $("#authbox__output")
            .empty()
            .append(function(){
                return $("<div>")
                .addClass(data.success ? "authbox__result alert alert-success" : "authbox__error alert alert-error")
                .text(data.success ? data.resultMessage : data.errorMessage)
            })
            .fadeIn("fast")

            if (data.success) {
                window.location.reload(true);
            }
        }
    }


    $(function(){

        if (false && userUuid) {
            var args = {};

            var personClass = "employee";

            $.post("/bus/frontend.php?action=get&class=" + personClass + "&uuid=" + userUuid, args, function(response) {
                // console.log(response);

                if (response && response.subId) {
                    personXml = new LongPolling(response.subId, function(subResponse) {
                        // alert(subResponse);
                        if (subResponse) {
                            xmlPerson = $.parseXML(subResponse);
                            $person = $(xmlPerson);

                            $personRoot = $person.find(":first-child");
                            if ($personRoot.length > 0 && $personRoot.children().length > 0) {
                                $(".userbox__username__displayname")
                                .text($personRoot.find("personal firstname").text() + ' ' + $personRoot.find("lastname").text());
                                $(".userbox__photo")
                                .fadeOut("fast", function() {
                                    $(this)
                                    .addClass("userbox__photo_source_loaded")
                                    .removeClass("userbox__photo_source_loading")
                                    .css({"background-image": "url(data:image/jpeg;base64," + $personRoot.find("photo").text() + ")"})
                                    .fadeIn("fast")
                                })
                            }

                        } else {
                            // console.log("person xml is empty");
                        }
                        // console.log(subResponse);


                      }, "xml");

                    // console.log(personXml);

                    personXml.init();
                } else {
                    // console.log("get person fail");
                }
            });
        }
        // logout
        $(".userbox__controls__logout").click(function(){
            var args = {};
            args["form"] = "userbox";
            args["act"] = "logout";

            $.post("/auth.php", args, parseAuthResponse);
        });
    });


    function logout() {
        var args = {};
        args["form"] = "userbox";
        args["act"] = "logout";
        $.post("/auth.php", args, parseAuthResponse);
    }


