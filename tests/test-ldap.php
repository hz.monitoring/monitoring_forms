<?php

    $ldap_host = "ldap.service.herzen";
    $ldap_rootdn = "dc=herzen,dc=spb,dc=ru";

    $ttUuid = "009466f6-e180-4bab-b761-b360073cf281";


    $ldap_handle = ldap_connect($ldap_host);
    if ($ldap_handle) {
        ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);

        // try anonymous login to test connection
        // $binded = ldap_bind($ldap_handle, $user_dn, $password);
        $anon = ldap_bind($ldap_handle);
        if (!$anon) {
            // test failed
            $ldap_handle = null;
        } else {
            // test passed, unbind anonymous and reconnect to primary
            ldap_unbind($ldap_handle);
            $ldap_handle = ldap_connect($ldap_host);
            ldap_set_option($ldap_handle, LDAP_OPT_PROTOCOL_VERSION, 3);
        }
    }

    $entries_search = ldap_search($ldap_handle, $ldap_rootdn, "(cn=$ttUuid)");
    $entries = ldap_get_entries($ldap_handle, $entries_search);

    var_dump($entries);