#!/bin/bash

# root directory for web available filed
WWW_ROOT=${1%/}

if [ -z $WWW_ROOT ]; then
    echo "WWW_ROOT not defined!"
    echo
    echo "How to use build.sh:"
    echo
    echo -ne "\t"
    echo "bash build.sh ./www"
    echo
    exit
fi

# source web available scripts directory
WWW_SOURCE=web
# target web available scripts directory
WWW_TARGET=$WWW_ROOT

# source conf files directory
CONF_SOURCE=conf
# target conf files directory
CONF_TARGET=vendor/tikhontagunov/conf

# source css files directory
CSS_SOURCE=css
# target css files directory
CSS_TARGET=$WWW_ROOT/css

# source js scripts directory
JS_SOURCE=js
# target js scripts directory
JS_TARGET=$WWW_ROOT/js

# source images directory
IMAGES_SOURCE=images
# target images directory
IMAGES_TARGET=$WWW_ROOT/images

# source images directory
IMG_SOURCE=img
# target images directory
IMG_TARGET=$WWW_ROOT/img


if [ ! -e $WWW_ROOT ]; then
    mkdir $WWW_ROOT
fi

if [ ! -e $WWW_ROOT ]; then
    echo "WWW_ROOT directory ($WWW_ROOT) does not exist and cannot be created! Stopping..."
    return
fi

if [ ! -e $WWW_TARGET ]; then
    mkdir $WWW_TARGET
    echo "Created WWW_ROOT directory ($WWW_ROOT)"
fi

if [ ! -e $WWW_TARGET ]; then
    echo "WWW_TARGET directory ($WWW_TARGET) does not exist and cannot be created! Stopping..."
    return
fi

if [ -e $WWW_SOURCE ]; then
    ln -r -s $WWW_SOURCE/* $WWW_TARGET

    echo "Created symlinks for www ($WWW_TARGET)"
fi

if [ -e $CONF_SOURCE ]; then

    if [ ! -e $CONF_TARGET ]; then
        ln -r -s -T $CONF_SOURCE $CONF_TARGET

        echo "Created symlink for conf ($CONF_TARGET)"
    else
        echo "Symlink for conf ($CONF_TARGET) already exists"
    fi

fi

if [ -e $CSS_SOURCE ]; then

    if [ ! -e $CSS_TARGET ]; then
        mkdir $CSS_TARGET
    fi

    if [ ! -e $CSS_TARGET ]; then
        echo "CSS_TARGET directory ($CSS_TARGET) does not exist and cannot be created! Stopping..."
        return
    fi

    ln -r -s $CSS_SOURCE/* $CSS_TARGET

    echo "Created symlinks for css ($CSS_TARGET)"
fi

if [ -e $JS_SOURCE ]; then

    if [ ! -e $JS_TARGET ]; then
        mkdir $JS_TARGET
    fi

    if [ ! -e $JS_TARGET ]; then
        echo "JS_TARGET directory ($JS_TARGET) does not exist and cannot be created! Stopping..."
        return
    fi

    ln -r -s $JS_SOURCE/* $JS_TARGET

    echo "Created symlinks for js ($JS_TARGET)"
fi

if [ -e $IMAGES_SOURCE ]; then

    if [ ! -e $IMAGES_TARGET ]; then
        mkdir $IMAGES_TARGET
    fi

    if [ ! -e $IMAGES_TARGET ]; then
        echo "IMAGES_TARGET directory ($IMAGES_TARGET) does not exist and cannot be created! Stopping..."
        return
    fi

    ln -r -s $IMAGES_SOURCE/* $IMAGES_TARGET

    echo "Created symlinks for images ($IMAGES_TARGET)"
fi


if [ -e $IMG_SOURCE ]; then

    if [ ! -e $IMG_TARGET ]; then
        mkdir $IMG_TARGET
    fi

    if [ ! -e $IMG_TARGET ]; then
        echo "IMG_TARGET directory ($IMG_TARGET) does not exist and cannot be created! Stopping..."
        return
    fi

    ln -r -s $IMG_SOURCE/* $IMG_TARGET

    echo "Created symlinks for img ($IMG_TARGET)"
fi
